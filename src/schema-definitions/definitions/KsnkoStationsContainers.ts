import { MODULE_NAME } from "../../constants";

export interface IKsnkoStationContainer {
    id: number;
    code: string;
    volume_ratio: number;
    trash_type: string;
    container_volume: number;
    container_brand: string;
    container_dump: string;
    cleaning_frequency_code: string;
    station_id: number;
    station_number: string;
    active: boolean;
}

export const ksnkoStationContainers = {
    name: MODULE_NAME + "KsnkoContainers",
    pgTableName: "ksnko_containers",
};
