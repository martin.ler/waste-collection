import { Point } from "geojson";
import { MODULE_NAME } from "../../constants";

export interface IKsnkoStation {
    id: number;
    number: string;
    name: string;
    access: string;
    location: string;
    has_sensor: boolean;
    changed_at: string;
    city_district_name: string;
    coordinate_lat: string;
    coordinate_lon: string;
    geom: Point;
    active: boolean;
    city_district_slug?: string;
}

export const ksnkoStations = {
    name: MODULE_NAME + "KsnkoStations",
    pgTableName: "ksnko_stations",
};
