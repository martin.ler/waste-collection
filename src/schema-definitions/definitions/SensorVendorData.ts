import { MODULE_NAME } from "../../constants";

export interface ISensorVendorData {
    vendor_id: string;
    ksnko_container_id: number;
    sensor_id: string | null;
    vendor: string;
    prediction: string | null;
    installed_at: string | null;
    installed_by: string | null;
    network: string | null;
    bin_depth: number | null;
    has_anti_noise: boolean;
    anti_noise_depth: number | null;
    algorithm: string;
    schedule: string | null;
    sensor_version: string;
    is_sensitive_to_pickups: boolean;
    pickup_sensor_type: string;
    decrease_threshold: number | null;
    pick_min_fill_level: number | null;
    active: boolean;
    bin_brand: string;
}

export const sensorVendorData = {
    name: MODULE_NAME + "SensorVendorData",
    pgTableName: "container_vendor_data",
};
