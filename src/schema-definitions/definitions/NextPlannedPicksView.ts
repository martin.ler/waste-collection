import { MODULE_NAME } from "../../constants";

export interface INextPlannedPick {
    ksnko_container_id: number;
    next_planned_pick_at: string;
}

export const nextPlannedPicksView = {
    name: MODULE_NAME + "NextPlannedPicksView",
    pgTableName: "v_next_planned_picks",
};
