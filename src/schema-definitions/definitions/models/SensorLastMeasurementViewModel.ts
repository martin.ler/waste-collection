import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ISensorMeasurement } from "#sch/definitions/SensorMeasurements";

export class SensorLastMeasurementViewModel extends Model<SensorLastMeasurementViewModel> implements ISensorMeasurement {
    declare vendor_id: string;
    declare ksnko_container_id: number;
    declare percent_smoothed: number;
    declare percent_raw: number;
    declare is_blocked: boolean;
    declare temperature: number;
    declare battery_status: number;
    declare measured_at: string;
    declare saved_at: string;
    declare vendor_updated_at: string;
    declare prediction_bin_full: string | null;
    declare firealarm: boolean;

    public static attributeModel: ModelAttributes<SensorLastMeasurementViewModel, ISensorMeasurement> = {
        vendor_id: {
            type: DataTypes.STRING(255),
            primaryKey: true,
        },
        ksnko_container_id: DataTypes.INTEGER,
        percent_smoothed: DataTypes.INTEGER,
        percent_raw: DataTypes.INTEGER,
        is_blocked: DataTypes.BOOLEAN,
        temperature: DataTypes.INTEGER,
        battery_status: DataTypes.FLOAT,
        measured_at: {
            type: DataTypes.DATE,
            primaryKey: true,
        },
        saved_at: DataTypes.DATE,
        vendor_updated_at: DataTypes.DATE,
        prediction_bin_full: DataTypes.DATE,
        firealarm: DataTypes.BOOLEAN,
    };
}
