import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import ITrashTypeMapping from "./interfaces/ITrashTypeMapping";

export default class TrashTypeMappingModel extends Model<ITrashTypeMapping> implements ITrashTypeMapping {
    declare name: string;
    declare code_ksnko: string;
    declare code_psas: string;
    declare legacy_id: number;

    public static attributeModel: ModelAttributes<TrashTypeMappingModel, ITrashTypeMapping> = {
        name: DataTypes.STRING,
        code_ksnko: { type: DataTypes.STRING(3), primaryKey: true },
        code_psas: DataTypes.STRING(3),
        legacy_id: DataTypes.INTEGER,
    };
}
