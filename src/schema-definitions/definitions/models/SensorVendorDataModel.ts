import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ISensorVendorData } from "#sch/definitions/SensorVendorData";

export class SensorVendorDataModel extends Model<SensorVendorDataModel> implements ISensorVendorData {
    declare vendor_id: string;
    declare ksnko_container_id: number;
    declare sensor_id: string | null;
    declare vendor: string;
    declare prediction: string | null;
    declare installed_at: string | null;
    declare installed_by: string | null;
    declare network: string | null;
    declare bin_depth: number | null;
    declare has_anti_noise: boolean;
    declare anti_noise_depth: number | null;
    declare algorithm: string;
    declare schedule: string | null;
    declare sensor_version: string;
    declare is_sensitive_to_pickups: boolean;
    declare pickup_sensor_type: string;
    declare decrease_threshold: number | null;
    declare pick_min_fill_level: number | null;
    declare active: boolean;
    declare bin_brand: string;

    public static attributeModel: ModelAttributes<SensorVendorDataModel, ISensorVendorData> = {
        vendor_id: {
            type: DataTypes.STRING(255),
            primaryKey: true,
        },
        ksnko_container_id: DataTypes.INTEGER,
        sensor_id: DataTypes.STRING(255),
        vendor: DataTypes.STRING(255),
        prediction: DataTypes.DATE,
        installed_at: DataTypes.DATE,
        installed_by: DataTypes.STRING(255),
        network: DataTypes.STRING(255),
        bin_depth: DataTypes.INTEGER,
        has_anti_noise: DataTypes.BOOLEAN,
        anti_noise_depth: DataTypes.INTEGER,
        algorithm: DataTypes.STRING(255),
        schedule: DataTypes.STRING(255),
        sensor_version: DataTypes.STRING(255),
        is_sensitive_to_pickups: DataTypes.BOOLEAN,
        pickup_sensor_type: DataTypes.STRING(255),
        decrease_threshold: DataTypes.INTEGER,
        pick_min_fill_level: DataTypes.INTEGER,
        active: DataTypes.BOOLEAN,
        bin_brand: DataTypes.STRING(255),
    };

    public static jsonSchema: JSONSchemaType<ISensorVendorData[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                vendor_id: { type: "string" },
                ksnko_container_id: { type: "integer" },
                sensor_id: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                vendor: { type: "string" },
                prediction: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                installed_at: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                installed_by: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                network: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                bin_depth: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                has_anti_noise: { type: "boolean" },
                anti_noise_depth: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                algorithm: { type: "string" },
                schedule: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                sensor_version: { type: "string" },
                is_sensitive_to_pickups: { type: "boolean" },
                pickup_sensor_type: { type: "string" },
                decrease_threshold: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                pick_min_fill_level: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                active: { type: "boolean" },
                bin_brand: { type: "string" },
            },
            required: ["vendor_id"],
        },
    };
}
