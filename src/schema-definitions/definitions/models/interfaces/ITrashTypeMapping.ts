export default interface ITrashTypeMapping {
    name: string;
    code_ksnko: string;
    code_psas: string;
    legacy_id: number;
}
