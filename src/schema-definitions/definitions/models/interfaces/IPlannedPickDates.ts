export default interface IPlannedPickDates {
    ksnko_container_id: number;
    pick_date: string | Date;
}
