import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ISensorPick } from "#sch/definitions/SensorPicks";

export class SensorPickModel extends Model<ISensorPick> implements ISensorPick {
    declare vendor_id: string;
    declare ksnko_container_id: number;
    declare pick_at: string;
    declare percent_before: number;
    declare percent_now: number;
    declare event_driven: boolean;

    public static attributeModel: ModelAttributes<SensorPickModel, ISensorPick> = {
        vendor_id: {
            type: DataTypes.STRING(255),
            primaryKey: true,
        },
        ksnko_container_id: DataTypes.INTEGER,
        pick_at: {
            type: DataTypes.DATE,
            primaryKey: true,
        },
        percent_before: DataTypes.INTEGER,
        percent_now: DataTypes.INTEGER,
        event_driven: DataTypes.BOOLEAN,
    };

    public static updateAttributes = Object.keys(SensorPickModel.attributeModel)
        .filter((att) => !["vendor_id", "pick_at"].includes(att))
        .concat("updated_at") as Array<keyof ISensorPick>;

    public static jsonSchema: JSONSchemaType<ISensorPick[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                vendor_id: { type: "string" },
                ksnko_container_id: { type: "integer" },
                pick_at: { type: "string" },
                percent_before: { type: "integer" },
                percent_now: { type: "integer" },
                event_driven: { type: "boolean" },
            },
            required: ["vendor_id"],
        },
    };
}
