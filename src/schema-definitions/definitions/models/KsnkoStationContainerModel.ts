import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IKsnkoStationContainer } from "#sch/definitions/KsnkoStationsContainers";

export class KsnkoStationContainerModel extends Model<KsnkoStationContainerModel> implements IKsnkoStationContainer {
    declare id: number;
    declare code: string;
    declare volume_ratio: number;
    declare trash_type: string;
    declare container_volume: number;
    declare container_brand: string;
    declare container_dump: string;
    declare cleaning_frequency_code: string;
    declare station_id: number;
    declare station_number: string;
    declare active: boolean;

    public static attributeModel: ModelAttributes<KsnkoStationContainerModel> = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        code: DataTypes.STRING(50),
        volume_ratio: DataTypes.FLOAT,
        trash_type: DataTypes.STRING(50),
        container_volume: DataTypes.INTEGER,
        container_brand: DataTypes.STRING(150),
        container_dump: DataTypes.STRING(50),
        cleaning_frequency_code: DataTypes.STRING(50),
        station_id: DataTypes.INTEGER,
        station_number: DataTypes.STRING(50),
        active: DataTypes.BOOLEAN,
    };

    public static jsonSchema: JSONSchemaType<IKsnkoStationContainer[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "integer" },
                code: { type: "string" },
                volume_ratio: { type: "number" },
                trash_type: { type: "string" },
                container_volume: { type: "integer" },
                container_brand: { type: "string" },
                container_dump: { type: "string" },
                cleaning_frequency_code: { type: "string" },
                station_id: { type: "integer" },
                station_number: { type: "string" },
                active: { type: "boolean" },
            },
            required: ["id"],
        },
    };
}
