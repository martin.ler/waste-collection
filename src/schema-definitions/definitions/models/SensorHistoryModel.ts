import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ISensorHistory } from "#sch/definitions/SensorHistory";

export class SensorHistoryModel extends Model<SensorHistoryModel> implements ISensorHistory {
    declare vendor_id: string;
    declare sensor_id: string | null;
    declare recorded_at: string;

    public static attributeModel: ModelAttributes<SensorHistoryModel, ISensorHistory> = {
        vendor_id: {
            type: DataTypes.STRING(255),
            primaryKey: true,
        },
        sensor_id: DataTypes.STRING(255),
        recorded_at: {
            type: DataTypes.DATE,
            primaryKey: true,
        },
    };

    public static jsonSchema: JSONSchemaType<ISensorHistory[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                vendor_id: { type: "string" },
                sensor_id: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                recorded_at: { type: "string" },
            },
            required: ["vendor_id"],
        },
    };
}
