import IDbRfidPick from "#ie/repositories/rfid/interfaces/IDbRfidPicks";
import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions/SharedSchemaProvider";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { Point } from "geojson";

export class RfidPickModel extends Model<IDbRfidPick> implements IDbRfidPick {
    declare vendor_id: string;
    declare ksnko_container_id: number;
    declare pick_at: string;
    declare event_driven: boolean;
    declare cu_id: string;
    declare pick_lat: number;
    declare pick_lon: number;
    declare geometry: Point;

    public static attributeModel: ModelAttributes<RfidPickModel, IDbRfidPick> = {
        vendor_id: {
            type: DataTypes.STRING(255),
            primaryKey: true,
        },
        ksnko_container_id: DataTypes.INTEGER,
        pick_at: {
            type: DataTypes.DATE,
            primaryKey: true,
        },
        event_driven: DataTypes.BOOLEAN,
        cu_id: DataTypes.STRING(255),
        pick_lat: DataTypes.NUMBER,
        pick_lon: DataTypes.NUMBER,
        geometry: DataTypes.GEOMETRY,
    };

    public static jsonSchema: JSONSchemaType<IDbRfidPick[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                vendor_id: { type: "string" },
                ksnko_container_id: { type: "integer" },
                pick_at: { type: "string" },
                event_driven: { type: "boolean" },
                cu_id: { type: "string" },
                pick_lat: { type: "number" },
                pick_lon: { type: "number" },
                geometry: { $ref: "#/definitions/geometry" },
            },
            required: ["vendor_id"],
        },
        definitions: {
            // @ts-expect-error referenced definition from other file
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}
