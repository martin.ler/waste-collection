import IRfidContainerVendorData from "#sch/datasources/rfid/IRfidContainerVendorData";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";

export class RfidContainerVendorDataModel
    extends Model<RfidContainerVendorDataModel, IRfidContainerVendorData>
    implements IRfidContainerVendorData
{
    declare vendor_id: string;
    declare ksnko_container_id: number;
    declare vendor: string | null;
    declare sensor_id: string; // | null;
    declare network: string; // | null;
    declare installed_at: string | null;
    declare installed_by: string; // | null;
    declare bin_brand: string;
    declare active: boolean;
    declare sensor_version: string;

    public static attributeModel: ModelAttributes<RfidContainerVendorDataModel, IRfidContainerVendorData> = {
        vendor_id: {
            type: DataTypes.STRING(255),
            primaryKey: true,
        },
        ksnko_container_id: DataTypes.INTEGER,
        vendor: { type: DataTypes.STRING(255), allowNull: true },
        sensor_id: { type: DataTypes.STRING(255), allowNull: true },
        network: { type: DataTypes.STRING(255), allowNull: true },
        installed_at: { type: DataTypes.DATE, allowNull: true },
        installed_by: { type: DataTypes.STRING(255), allowNull: true },
        bin_brand: { type: DataTypes.STRING(255), allowNull: true },
        active: DataTypes.BOOLEAN,
        sensor_version: { type: DataTypes.STRING(255), allowNull: true },
    };

    public static jsonSchema: JSONSchemaType<IRfidContainerVendorData[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                vendor_id: { type: "string" },
                ksnko_container_id: { type: "integer" },
                vendor: { type: "string" },
                sensor_id: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                network: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                installed_at: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                installed_by: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                bin_brand: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                active: { type: "boolean" },
                sensor_version: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            },
            required: ["vendor_id", "ksnko_container_id", "active"],
        },
    };
}
