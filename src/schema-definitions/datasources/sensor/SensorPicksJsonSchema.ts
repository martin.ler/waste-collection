import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export interface ISensorPicksInput {
    vendor_id: string;
    ksnko_container_id: number;
    pick_at: string;
    percent_before: number;
    percent_now: number;
    event_driven: boolean;
}

const sensorPicksJsonSchema: JSONSchemaType<ISensorPicksInput[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            ksnko_container_id: { type: "integer" },
            vendor_id: { type: "string" },
            pick_at: { type: "string" },
            percent_before: { type: "integer" },
            percent_now: { type: "integer" },
            event_driven: { type: "boolean" },
        },
        required: ["vendor_id", "ksnko_container_id", "pick_at", "percent_before", "percent_now", "event_driven"],
        additionalProperties: false,
    },
};

export const sensorPicksDatasource: any = {
    name: "SensorPicksDataSource",
    jsonSchema: sensorPicksJsonSchema,
};
