import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import IRfidPick from "./IRfidPick";

export default class RfidContainerVendorDataSchema {
    public static jsonSchema: JSONSchemaType<IRfidPick[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                vendor_id: { type: "string" },
                ksnko_container_id: { type: "integer" },
                pick_at: { type: "string" },
                event_driven: { type: "boolean" },
                cu_id: { type: "string" },
                pick_lat: { type: "number" },
                pick_lon: { type: "number" },
            },
            required: ["vendor_id", "ksnko_container_id", "pick_at", "event_driven", "cu_id", "pick_lat", "pick_lon"],
        },
    };
}
