export default interface IRfidContainerVendorData {
    vendor_id: string;
    ksnko_container_id: number;
    vendor: string | null;
    sensor_id: string | null;
    network: string | null;
    installed_at: string | null;
    installed_by: string | null;
    bin_brand: string | null;
    active: boolean;
    sensor_version: string | null;
}
