import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import IRfidContainerVendorData from "./IRfidContainerVendorData";

export default class RfidContainerVendorDataSchema {
    public static itemJsonSchema: JSONSchemaType<IRfidContainerVendorData[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                vendor_id: { type: "string" },
                ksnko_container_id: { type: "integer" },
                vendor: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                sensor_id: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                network: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                installed_at: {
                    oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                },
                installed_by: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                bin_brand: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                active: { type: "boolean" },
                sensor_version: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            },
            required: ["vendor_id", "ksnko_container_id", "active"],
        },
    };

    public static jsonSchema: any = {
        type: "object",
        properties: {
            vendor: { type: "string" },
            container_vendor_data: this.itemJsonSchema,
        },
    };
}
