export default interface IRfidPick {
    vendor_id: string;
    ksnko_container_id: number;
    pick_at: string;
    event_driven: boolean;
    cu_id: string;
    pick_lat: number;
    pick_lon: number;
}
