import { ksnkoStationsDatasource } from "#sch/datasources/ksnko/KsnkoStationJsonSchema";
import { sensorMeasurementsDatasource } from "#sch/datasources/sensor/SensorMeasurementsJsonSchema";
import { sensorPicksDatasource } from "#sch/datasources/sensor/SensorPicksJsonSchema";
import { sensorVendorDataDatasource } from "#sch/datasources/sensor/SensorVendorDataJsonSchema";
import { ksnkoStations } from "#sch/definitions/KsnkoStations";
import { ksnkoStationContainers } from "#sch/definitions/KsnkoStationsContainers";
import { ksnkoStationItems } from "#sch/definitions/KsnkoStationsItems";
import { nextPlannedPicksView } from "#sch/definitions/NextPlannedPicksView";
import { sensorHistory } from "#sch/definitions/SensorHistory";
import { sensorLastMeasurementsView } from "#sch/definitions/SensorLastMeasurementsView";
import { sensorLastPicksView } from "#sch/definitions/SensorLastPicksView";
import { sensorMeasurements } from "#sch/definitions/SensorMeasurements";
import { sensorPicks } from "#sch/definitions/SensorPicks";
import { sensorVendorData } from "#sch/definitions/SensorVendorData";
import { MODULE_NAME, SCHEMA_NAME } from "../constants";
import InputRawPickDatesSchema from "./datasources/psas/InputRawPickDatesSchema";
import RfidContainerVendorDataSchema from "./datasources/rfid/RfidContainerVendorDataSchema";
import RfidPickSchema from "./datasources/rfid/RfidPickSchema";

const forExport: any = {
    name: MODULE_NAME,
    pgSchema: SCHEMA_NAME,
    datasources: {
        ksnkoStationsDatasource,
        sensorVendorDataDatasource,
        sensorMeasurementsDatasource,
        sensorPicksDatasource,
        ksnkoStations: {
            name: "KsnkoStationsDataSource",
            jsonSchema: ksnkoStationsDatasource.ksnkoStationsJsonSchema,
        },
        rawPickDates: {
            name: "RawPickDatesDataSource",
            jsonSchema: InputRawPickDatesSchema.jsonSchema,
        },
        rfid: {
            containerVendorData: {
                name: "RfidContainerVendorDataSource",
                jsonSchema: RfidContainerVendorDataSchema.jsonSchema,
            },
            picks: {
                name: "RfidPickDataSource",
                jsonSchema: RfidPickSchema.jsonSchema,
            },
        },
    },
    definitions: {
        ksnkoStations,
        ksnkoStationItems,
        ksnkoStationContainers,
        sensorMeasurements,
        sensorHistory,
        sensorPicks,
        sensorVendorData,
        sensorLastMeasurementsView,
        sensorLastPicksView,
        nextPlannedPicksView,
        plannedPickDates: {
            name: MODULE_NAME + "PlannedPickDates",
            pgTableName: "planned_pick_dates",
        },
        rawPickDates: {
            name: MODULE_NAME + "RawPickDates",
            pgTableName: "raw_pick_dates",
        },
        trashTypeMapper: {
            name: MODULE_NAME + "TrashTypeMapping",
            pgTableName: "trash_type_mapping",
        },
        rfid: {
            containerVendorData: {
                name: MODULE_NAME + "ContainerVendorRfid",
                pgTableName: "container_vendor_data_rfid",
            },
            picks: {
                name: MODULE_NAME + "PicksRfid",
                pgTableName: "picks_rfid",
            },
            sensorHistory: {
                name: MODULE_NAME + "SensorHistoryRfid",
                pgTableName: "sensor_history_rfid",
            },
        },
    },
};

export { forExport as WasteCollection };
