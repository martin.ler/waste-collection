import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { query, oneOf } from "@golemio/core/dist/shared/express-validator";
import { parseCoordinates } from "@golemio/core/dist/output-gateway/Geo";
import { useCacheMiddleware } from "@golemio/core/dist/output-gateway/redis";
import { BaseRouter } from "@golemio/core/dist/output-gateway/routes/BaseRouter";
import { checkErrors, paginationLimitMiddleware, pagination } from "@golemio/core/dist/output-gateway/Validation";
import { StationsRepository } from "#og/repositories/StationsRepository";
import { SensorMeasurementsRepository } from "#og/repositories/SensorMeasurementsRepository";
import { SensorPicksRepository } from "#og/repositories/SensorPicksRepository";
import { PlannedPickDatesRepository } from "#og/repositories/PlannedPickDatesRepository";
import { repositories } from "#og/repositories/RepositoryManager";
import { LegacyOutputMapper } from "#og/helpers";

class WasteCollectionLegacyRouter extends BaseRouter {
    protected stationsRepository: StationsRepository;
    protected sensorMeasurementsRepository: SensorMeasurementsRepository;
    protected sensorPicksRepository: SensorPicksRepository;
    protected plannedPickDatesRepository: PlannedPickDatesRepository;

    constructor() {
        super();
        this.stationsRepository = repositories.stationsRepository;
        this.sensorMeasurementsRepository = repositories.sensorMeasurementsRepository;
        this.sensorPicksRepository = repositories.sensorPicksRepository;
        this.plannedPickDatesRepository = repositories.plannedPickDatesRepository;

        this.router.get(
            "/measurements",
            [
                oneOf([query("containerId").exists().isInt(), query("ksnkoId").exists().isInt()]),
                query("from").optional().isISO8601(),
                query("to").optional().isISO8601(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("SortedWasteRouter"),
            useCacheMiddleware(),
            this.GetMeasurements
        );
        this.router.get(
            "/picks",
            [
                oneOf([query("containerId").exists().isInt(), query("ksnkoId").exists().isInt()]),
                query("from").optional().isISO8601(),
                query("to").optional().isISO8601(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("SortedWasteRouter"),
            useCacheMiddleware(),
            this.GetPicks
        );

        this.router.get(
            "/pickdays",
            [query("sensoneoCode").optional().isInt(), query("ksnkoId").optional().isInt()],
            pagination,
            checkErrors,
            paginationLimitMiddleware("LegacySortedWasteRouter"),
            useCacheMiddleware(),
            this.GetPickDays
        );

        this.router.get(
            "/",
            [
                query("accessibility").optional().isInt(),
                query("onlyMonitored").optional().isBoolean(),
                query("districts").optional(),
                query("districts.*").isString(),
                query("latlng").optional().isString(),
                query("range").optional().isInt(),
                query("id").optional().isInt(),
                query("ksnkoId").optional().isInt(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("SortedWasteRouter"),
            useCacheMiddleware(),
            this.GetAll
        );
    }

    public GetAll = async (req: Request, res: Response, next: NextFunction) => {
        let districts: any = req.query.districts;

        if (districts && !(districts instanceof Array)) {
            districts = districts.split(",");
        }

        try {
            const coords = await parseCoordinates(req.query.latlng as string, req.query.range as string);

            const data = await this.stationsRepository.GetAll({
                accessibility: req.query.accessibility ? Number(req.query.accessibility) : undefined,
                districts,
                id: req.query.id as string,
                ksnkoId: req.query.ksnkoId as string,
                lat: coords.lat,
                lng: coords.lng,
                onlyMonitored: req.query.onlyMonitored === "true",
                range: coords.range,
                limit: req.query.limit ? Number(req.query.limit) : undefined,
                offset: req.query.offset ? Number(req.query.offset) : undefined,
            });

            res.status(200).send(await LegacyOutputMapper.getAllMapper(data));
        } catch (err) {
            next(err);
        }
    };

    public GetMeasurements = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.sensorMeasurementsRepository.GetAll({
                containerId: req.query.containerId as string,
                ksnkoId: req.query.ksnkoId as string,
                from: req.query.from as string,
                limit: req.query.limit ? Number(req.query.limit) : undefined,
                offset: req.query.offset ? Number(req.query.offset) : undefined,
                to: req.query.to as string,
            });

            res.status(200).send(LegacyOutputMapper.getAllMeasurementsMapper(data));
        } catch (err) {
            next(err);
        }
    };

    public GetPicks = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.sensorPicksRepository.GetAll({
                containerId: req.query.containerId as string,
                ksnkoId: req.query.ksnkoId as string,
                from: req.query.from as string,
                limit: req.query.limit ? Number(req.query.limit) : undefined,
                offset: req.query.offset ? Number(req.query.offset) : undefined,
                to: req.query.to as string,
            });
            res.status(200).send(LegacyOutputMapper.getAllPicksMapper(data));
        } catch (err) {
            next(err);
        }
    };

    public GetPickDays = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.plannedPickDatesRepository.GetAll({
                sensoneoCode: req.query.sensoneoCode as string,
                ksnkoId: req.query.ksnkoId as string,
            });
            res.status(200).send(LegacyOutputMapper.getAllPlannedPickDatesMapper(data));
        } catch (err) {
            next(err);
        }
    };
}

const wasteCollectionLegacyRouter: Router = new WasteCollectionLegacyRouter().router;

export { wasteCollectionLegacyRouter };
