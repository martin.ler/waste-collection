import Sequelize from "@golemio/core/dist/shared/sequelize";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { WasteCollection } from "#sch";
import { KsnkoStationModel } from "#sch/definitions/models/KsnkoStationModel";
import { KsnkoStationContainerModel } from "#sch/definitions/models/KsnkoStationContainerModel";
import { IWasteCollectionRepositories, repositories } from "#og/repositories/RepositoryManager";
import { SensorMeasurementModel } from "#sch/definitions/models/SensorMeasurementModel";
import { SensorVendorDataModel } from "#sch/definitions/models/SensorVendorDataModel";
import { LegacyAccessibilityMapper } from "#og/helpers";
import RawPickDates from "#sch/definitions/models/RawPickDatesModel";

export type TContainersExtendedModel = KsnkoStationContainerModel & {
    next_planned_pick: { next_planned_pick_at_date: string };
    raw_picks: Array<Pick<RawPickDates, "days">>;
    vendor_data?: Pick<SensorVendorDataModel, "vendor" | "vendor_id"> & {
        last_picks?: { last_pick_at: string };
        last_measurements?: Pick<SensorMeasurementModel, "measured_at" | "percent_smoothed" | "prediction_bin_full">;
    };
};

export type TStationWithContainersModel = Omit<
    KsnkoStationModel,
    "location" | "has_sensor" | "changed_at" | "coordinate_lat" | "coordinate_lon" | "active"
> & { containers: TContainersExtendedModel[]; updated_at: string };

export class StationsRepository extends SequelizeModel {
    constructor() {
        super(
            WasteCollection.definitions.ksnkoStations.name + "Repository",
            WasteCollection.definitions.ksnkoStations.pgTableName,
            KsnkoStationModel.attributeModel,
            { schema: WasteCollection.pgSchema }
        );
    }

    public Associate = (repositories: IWasteCollectionRepositories) => {
        this.sequelizeModel.hasMany(repositories.stationContainersRepository["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "station_id",
            as: "containers",
        });
    };

    public async GetAll(
        options: {
            accessibility?: number;
            districts?: string[];
            id?: string;
            ksnkoId?: string;
            onlyMonitored?: boolean;
            lat?: number;
            lng?: number;
            range?: number;
            limit?: number;
            offset?: number;
        } = {}
    ): Promise<TStationWithContainersModel[]> {
        const whereAttributes: Sequelize.WhereOptions = {};
        const order: Sequelize.Order = [[Sequelize.col("id"), "asc"]];

        if (options.districts && options.districts.length > 0) {
            whereAttributes.city_district_slug = {
                [Sequelize.Op.in]: options.districts,
            };
        }

        if (options.id) {
            whereAttributes.id = {
                [Sequelize.Op.eq]: options.id,
            };
        }

        if (options.accessibility) {
            whereAttributes.access = {
                [Sequelize.Op.eq]: LegacyAccessibilityMapper.getAccessibilityById(options.accessibility),
            };
        }

        if (options.lat && options.lng) {
            if (options.range) {
                whereAttributes.range = Sequelize.where(
                    Sequelize.fn(
                        "ST_DWithin",
                        Sequelize.col("geom"),
                        Sequelize.cast(
                            Sequelize.fn("ST_SetSRID", Sequelize.fn("ST_MakePoint", options.lng, options.lat), 4326),
                            "geography"
                        ),
                        options.range,
                        true
                    ),
                    "true"
                );
            }

            order.push(
                Sequelize.fn(
                    "ST_Distance",
                    Sequelize.col("geom"),
                    Sequelize.cast(
                        Sequelize.fn("ST_SetSRID", Sequelize.fn("ST_MakePoint", options.lng, options.lat), 4326),
                        "geography"
                    )
                )
            );
        }

        order.push([{ model: repositories.stationContainersRepository["sequelizeModel"], as: "containers" }, "id", "asc"]);

        return this.sequelizeModel.findAll({
            attributes: {
                include: ["updated_at"],
                exclude: ["location", "has_sensor", "changed_at", "coordinate_lat", "coordinate_lon", "active"],
            },
            include: [
                {
                    model: repositories.stationContainersRepository["sequelizeModel"],
                    as: "containers",
                    include: [
                        {
                            model: repositories.nextPlannedPicksRepository["sequelizeModel"],
                            as: "next_planned_pick",
                            attributes: [
                                [
                                    Sequelize.fn("DATE", Sequelize.literal("next_planned_pick_at AT TIME ZONE 'Europe/Prague'")),
                                    "next_planned_pick_at_date",
                                ],
                            ],
                            required: false,
                        },
                        {
                            model: repositories.rawPickDatesRepository["sequelizeModel"],
                            as: "raw_picks",
                            attributes: ["days"],
                            required: false,
                            on: {
                                station_number: { [Sequelize.Op.eq]: Sequelize.col("containers.station_number") },
                                trash_type_code: { [Sequelize.Op.eq]: Sequelize.col("containers.trash_type") },
                            },
                        },
                        {
                            model: repositories.sensorVendorDataRepository["sequelizeModel"],
                            as: "vendor_data",
                            attributes: ["vendor", "vendor_id", "active"],
                            required: options.onlyMonitored ?? false,
                            subQuery: false,
                            where: {
                                active: {
                                    [Sequelize.Op.eq]: true,
                                },
                            },
                            include: [
                                {
                                    model: repositories.sensorLastMeasurementsRepository["sequelizeModel"],
                                    as: "last_measurements",
                                    attributes: ["measured_at", "percent_smoothed", "prediction_bin_full"],
                                    required: false,
                                    subQuery: false,
                                },
                                {
                                    model: repositories.sensorLastPicksRepository["sequelizeModel"],
                                    as: "last_picks",
                                    attributes: ["last_pick_at"],
                                    required: false,
                                    subQuery: false,
                                },
                            ],
                        },
                    ],
                },
            ],
            where: whereAttributes,
            limit: options?.limit,
            offset: Number.isInteger(options?.offset) ? options?.offset : undefined,
            order,
        });
    }

    public GetOne(id: string): never {
        throw new Error("Not implemented");
    }
}
