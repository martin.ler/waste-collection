import Sequelize from "@golemio/core/dist/shared/sequelize";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { WasteCollection } from "#sch/index";
import PlannedPickDatesModel from "#sch/definitions/models/PlannedPickDatesModel";
import { IWasteCollectionRepositories, repositories } from "#og";

export class PlannedPickDatesRepository extends SequelizeModel {
    constructor() {
        super(
            WasteCollection.definitions.plannedPickDates.name + "Repository",
            WasteCollection.definitions.plannedPickDates.pgTableName,
            PlannedPickDatesModel.attributeModel,
            { schema: WasteCollection.pgSchema }
        );
    }

    public Associate = (repositories: IWasteCollectionRepositories) => {
        this.sequelizeModel.belongsTo(repositories.sensorVendorDataRepository["sequelizeModel"], {
            targetKey: "ksnko_container_id",
            foreignKey: "ksnko_container_id",
            as: "vendor_data",
        });
    };

    public async GetAll(options?: { sensoneoCode?: string; ksnkoId?: string }): Promise<PlannedPickDatesModel[]> {
        const whereAttributes: Sequelize.WhereOptions = {};
        const order: Sequelize.Order = [[Sequelize.col("ksnko_container_id"), "asc"]];

        if (options?.sensoneoCode && options?.ksnkoId && options.sensoneoCode !== options.ksnkoId) {
            return [];
        }

        if (options?.sensoneoCode) {
            whereAttributes.ksnko_container_id = {
                [Sequelize.Op.eq]: options.sensoneoCode,
            };
        }

        if (options?.ksnkoId) {
            whereAttributes.ksnko_container_id = {
                [Sequelize.Op.eq]: options?.ksnkoId,
            };
        }

        return this.sequelizeModel.findAll<PlannedPickDatesModel>({
            attributes: {
                include: [[Sequelize.fn("date", Sequelize.col("pick_date")), "pick_date"]],
            },
            include: [
                {
                    model: repositories.sensorVendorDataRepository["sequelizeModel"],
                    as: "vendor_data",
                    attributes: [],
                    where: { active: true },
                },
            ],
            where: whereAttributes,
            order,
        });
    }

    public GetOne(id: string): never {
        throw new Error("Not implemented");
    }
}
