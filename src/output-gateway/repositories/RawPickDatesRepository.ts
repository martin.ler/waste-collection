import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { WasteCollection } from "#sch/index";
import RawPickDates from "#sch/definitions/models/RawPickDatesModel";
import { IWasteCollectionRepositories } from "#og";

export class RawPickDatesRepository extends SequelizeModel {
    constructor() {
        super(
            WasteCollection.definitions.rawPickDates.name + "Repository",
            WasteCollection.definitions.rawPickDates.pgTableName,
            RawPickDates.attributeModel,
            { schema: WasteCollection.pgSchema }
        );
    }

    public Associate = (repositories: IWasteCollectionRepositories) => {
        this.sequelizeModel.belongsTo(repositories.stationContainersRepository["sequelizeModel"], {
            targetKey: "station_number",
            foreignKey: "station_number",
            as: "containers",
        });
    };

    public GetAll(id: string): never {
        throw new Error("Not implemented");
    }

    public GetOne(id: string): never {
        throw new Error("Not implemented");
    }
}
