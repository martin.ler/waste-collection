import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { WasteCollection } from "#sch";
import { KsnkoStationContainerModel } from "#sch/definitions/models/KsnkoStationContainerModel";
import { IWasteCollectionRepositories } from "#og/repositories/RepositoryManager";

export class StationContainersRepository extends SequelizeModel {
    constructor() {
        super(
            WasteCollection.definitions.ksnkoStationContainers.name + "Repository",
            WasteCollection.definitions.ksnkoStationContainers.pgTableName,
            KsnkoStationContainerModel.attributeModel,
            { schema: WasteCollection.pgSchema }
        );
    }

    public Associate = (repositories: IWasteCollectionRepositories) => {
        this.sequelizeModel.belongsTo(repositories.stationsRepository["sequelizeModel"], {
            targetKey: "id",
            foreignKey: "station_id",
        });

        this.sequelizeModel.hasOne(repositories.sensorVendorDataRepository["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "ksnko_container_id",
            as: "vendor_data",
        });

        this.sequelizeModel.hasMany(repositories.sensorPicksRepository["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "ksnko_container_id",
            as: "picks",
        });

        this.sequelizeModel.hasMany(repositories.sensorMeasurementsRepository["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "ksnko_container_id",
            as: "measurements",
        });

        this.sequelizeModel.hasOne(repositories.nextPlannedPicksRepository["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "ksnko_container_id",
            as: "next_planned_pick",
        });

        this.sequelizeModel.hasMany(repositories.rawPickDatesRepository["sequelizeModel"], {
            sourceKey: "station_number",
            foreignKey: "station_number",
            as: "raw_picks",
        });
    };

    public GetAll = (): never => {
        throw new Error("Method not implemented.");
    };

    public GetOne = (): never => {
        throw new Error("Method not implemented.");
    };
}
