import Sequelize from "@golemio/core/dist/shared/sequelize";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { WasteCollection } from "#sch/index";
import { SensorMeasurementModel } from "#sch/definitions/models/SensorMeasurementModel";
import { IWasteCollectionRepositories } from "#og";

export type TMeasurementsExtendedDataModel = SensorMeasurementModel & { updated_at: string; created_at: string };

export class SensorMeasurementsRepository extends SequelizeModel {
    constructor() {
        super(
            WasteCollection.definitions.sensorMeasurements.name + "Repository",
            WasteCollection.definitions.sensorMeasurements.pgTableName,
            SensorMeasurementModel.attributeModel,
            { schema: WasteCollection.pgSchema }
        );
    }

    public Associate = (repositories: IWasteCollectionRepositories) => {
        this.sequelizeModel.belongsTo(repositories.stationContainersRepository["sequelizeModel"], {
            targetKey: "id",
            foreignKey: "ksnko_container_id",
            as: "containers",
        });
    };

    public GetAll = async (
        options: {
            containerId?: string;
            ksnkoId?: string;
            from?: string;
            to?: string;
            limit?: number;
            offset?: number;
        } = {}
    ): Promise<TMeasurementsExtendedDataModel[]> => {
        const whereAttributes: Sequelize.WhereOptions = {
            [Sequelize.Op.and]: [
                ...(options.containerId ? [{ ksnko_container_id: { [Sequelize.Op.eq]: options.containerId } }] : []),
                ...(options.ksnkoId ? [{ ksnko_container_id: { [Sequelize.Op.eq]: options.ksnkoId } }] : []),
                ...(options.from ? [{ measured_at: { [Sequelize.Op.gte]: options.from } }] : []),
                ...(options.to ? [{ measured_at: { [Sequelize.Op.lte]: options.to } }] : []),
            ],
        };

        return this.sequelizeModel.findAll<TMeasurementsExtendedDataModel>({
            attributes: {
                include: ["updated_at", "created_at"],
            },
            raw: true,
            where: whereAttributes,
            limit: options.limit,
            offset: options.offset,
            order: [[Sequelize.col("measured_at"), "desc"]],
        });
    };

    public GetOne(id: string): never {
        throw new Error("Not implemented");
    }
}
