import Sequelize from "@golemio/core/dist/shared/sequelize";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { WasteCollection } from "#sch/index";
import { SensorPickModel } from "#sch/definitions/models/SensorPickModel";
import { IWasteCollectionRepositories, repositories } from "#og";
import { SensorVendorDataModel } from "#sch/definitions/models/SensorVendorDataModel";

export type TPicksWithVendorDataModel = SensorPickModel & {
    vendor_data?: Pick<SensorVendorDataModel, "pick_min_fill_level" | "decrease_threshold">;
    updated_at: Date;
    created_at: Date;
};

export class SensorPicksRepository extends SequelizeModel {
    constructor() {
        super(
            WasteCollection.definitions.sensorPicks.name + "Repository",
            WasteCollection.definitions.sensorPicks.pgTableName,
            SensorPickModel.attributeModel,
            { schema: WasteCollection.pgSchema }
        );
    }

    public Associate = (repositories: IWasteCollectionRepositories) => {
        this.sequelizeModel.belongsTo(repositories.stationContainersRepository["sequelizeModel"], {
            targetKey: "id",
            foreignKey: "ksnko_container_id",
            as: "containers",
        });

        this.sequelizeModel.belongsTo(repositories.sensorVendorDataRepository["sequelizeModel"], {
            targetKey: "ksnko_container_id",
            foreignKey: "ksnko_container_id",
            as: "vendor_data",
        });
    };

    public GetAll = async (
        options: {
            containerId?: string;
            ksnkoId?: string;
            from?: string;
            to?: string;
            limit?: number;
            offset?: number;
        } = {}
    ): Promise<TPicksWithVendorDataModel[]> => {
        const whereAttributes: Sequelize.WhereOptions = {
            [Sequelize.Op.and]: [
                ...(options.containerId ? [{ ksnko_container_id: { [Sequelize.Op.eq]: options.containerId } }] : []),
                ...(options.ksnkoId ? [{ ksnko_container_id: { [Sequelize.Op.eq]: options.ksnkoId } }] : []),
                ...(options.from ? [{ pick_at: { [Sequelize.Op.gte]: options.from } }] : []),
                ...(options.to ? [{ pick_at: { [Sequelize.Op.lte]: options.to } }] : []),
            ],
        };

        return this.sequelizeModel.findAll<TPicksWithVendorDataModel>({
            attributes: {
                include: ["updated_at", "created_at"],
            },
            include: [
                {
                    model: repositories.sensorVendorDataRepository["sequelizeModel"],
                    as: "vendor_data",
                    attributes: ["pick_min_fill_level", "decrease_threshold"],
                },
            ],
            where: whereAttributes,
            raw: true,
            nest: true,
            limit: options.limit,
            offset: options.offset,
            order: [[Sequelize.col("pick_at"), "desc"]],
        });
    };

    public GetOne(id: string): never {
        throw new Error("Not implemented");
    }
}
