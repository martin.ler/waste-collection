import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { WasteCollection } from "#sch/index";
import { IWasteCollectionRepositories } from "#og";
import { NextPlannedPicksViewModel } from "#sch/definitions/models/NextPlannedPicksViewModel";

export class NextPlannedPicksRepository extends SequelizeModel {
    constructor() {
        super(
            WasteCollection.definitions.nextPlannedPicksView.name + "Repository",
            WasteCollection.definitions.nextPlannedPicksView.pgTableName,
            NextPlannedPicksViewModel.attributeModel,
            { schema: WasteCollection.pgSchema }
        );
    }

    public Associate = (repositories: IWasteCollectionRepositories) => {
        this.sequelizeModel.belongsTo(repositories.stationContainersRepository["sequelizeModel"], {
            targetKey: "id",
            foreignKey: "ksnko_container_id",
            as: "containers",
        });
    };

    public GetAll(): never {
        throw new Error("Not implemented");
    }

    public GetOne(id: string): never {
        throw new Error("Not implemented");
    }
}
