import { StationsRepository } from "#og/repositories/StationsRepository";
import { SensorVendorDataRepository } from "#og/repositories/SensorVendorDataRepository";
import { SensorMeasurementsRepository } from "#og/repositories/SensorMeasurementsRepository";
import { SensorLastMeasurementsRepository } from "#og/repositories/SensorLastMeasurementsRepository";
import { SensorPicksRepository } from "#og/repositories/SensorPicksRepository";
import { StationContainersRepository } from "#og/repositories/StationContainersRepository";
import { PlannedPickDatesRepository } from "#og/repositories/PlannedPickDatesRepository";
import { RawPickDatesRepository } from "#og/repositories/RawPickDatesRepository";
import { SensorLastPicksRepository } from "#og/repositories/SensorLastPicksRepository";
import { NextPlannedPicksRepository } from "#og/repositories/NextPlannedPicksRepository";

export interface IWasteCollectionRepositories {
    stationsRepository: StationsRepository;
    stationContainersRepository: StationContainersRepository;
    sensorVendorDataRepository: SensorVendorDataRepository;
    sensorMeasurementsRepository: SensorMeasurementsRepository;
    sensorLastMeasurementsRepository: SensorLastMeasurementsRepository;
    sensorPicksRepository: SensorPicksRepository;
    sensorLastPicksRepository: SensorLastPicksRepository;
    nextPlannedPicksRepository: NextPlannedPicksRepository;
    rawPickDatesRepository: RawPickDatesRepository;
    plannedPickDatesRepository: PlannedPickDatesRepository;
}

const repositories: IWasteCollectionRepositories = {
    stationsRepository: new StationsRepository(),
    stationContainersRepository: new StationContainersRepository(),
    sensorVendorDataRepository: new SensorVendorDataRepository(),
    sensorMeasurementsRepository: new SensorMeasurementsRepository(),
    sensorLastMeasurementsRepository: new SensorLastMeasurementsRepository(),
    sensorPicksRepository: new SensorPicksRepository(),
    nextPlannedPicksRepository: new NextPlannedPicksRepository(),
    sensorLastPicksRepository: new SensorLastPicksRepository(),
    rawPickDatesRepository: new RawPickDatesRepository(),
    plannedPickDatesRepository: new PlannedPickDatesRepository(),
};

for (const type of Object.keys(repositories)) {
    const model = (repositories as any)[type];
    if (model.hasOwnProperty("Associate")) {
        model.Associate(repositories);
    }
}

export { repositories };
