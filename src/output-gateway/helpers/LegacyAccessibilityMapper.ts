enum Access {
    FREE = "volně",
    RESIDENT = "obyvatelům domu",
    OTHER = "neznámá dostupnost",
}

export class LegacyAccessibilityMapper {
    private static legacyOptionMap: Map<number, string> = new Map([
        [1, Access.FREE],
        [2, Access.RESIDENT],
        [3, Access.OTHER],
    ]);
    private static defaultId = 3;

    public static getIdByValue = (searchValue: string): number | undefined => {
        for (const [key, value] of this.legacyOptionMap.entries()) {
            if (value === searchValue) {
                return key;
            }
        }
    };

    public static getAccessibilityByString = (searchValue: string): { id: number; description: string } | null => {
        const id = LegacyAccessibilityMapper.getIdByValue(searchValue);
        return {
            description: id ? searchValue : this.legacyOptionMap.get(this.defaultId)!,
            id: id ?? this.defaultId,
        };
    };

    public static getAccessibilityById = (id: number): string => {
        return this.legacyOptionMap.get(id) ?? this.legacyOptionMap.get(this.defaultId)!;
    };
}
