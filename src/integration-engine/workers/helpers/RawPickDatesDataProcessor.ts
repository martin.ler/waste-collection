import IRawPickDatesInput from "#sch/datasources/psas/IRawPickDatesInput";

export default class RawPickDatesDataProcessor {
    private inputData: IRawPickDatesInput[];
    private maximumDate: Date | null; // downloaded data contains date and only the ones with last date should be processed

    constructor() {
        this.inputData = new Array<IRawPickDatesInput>();
        this.maximumDate = null;
    }

    public processData = async (data: IRawPickDatesInput[]) => {
        this.updateMaxDate(data);

        if (this.maximumDate) {
            this.inputData.push(...data.filter((el) => el.datum === this.formatDateToExpectedString(this.maximumDate!)));
        }
    };

    public getData = (): IRawPickDatesInput[] => {
        return this.maximumDate
            ? this.inputData.filter((el) => el.datum === this.formatDateToExpectedString(this.maximumDate!))
            : [];
    };

    private formatDateToExpectedString(date: Date): string {
        return date.toISOString().slice(0, 10) + "T00:00:00";
    }

    private updateMaxDate(data: IRawPickDatesInput[]) {
        for (let index = 0; index < data.length; index++) {
            const elementDate = new Date(data[index].datum);
            if (!this.maximumDate || elementDate > this.maximumDate) this.maximumDate = elementDate;
        }
    }
}
