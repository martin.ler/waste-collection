import { WasteCollectionWorker } from "./WasteCollectionWorker";

export * from "./KsnkoWorker";
export * from "./SensorWorker";

export const workers = [WasteCollectionWorker];
