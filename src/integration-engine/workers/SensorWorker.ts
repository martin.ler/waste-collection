import { BaseWorker, config, PostgresConnector } from "@golemio/core/dist/integration-engine";
import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import {
    SensorVendorDataRepository,
    SensorMeasurementsRepository,
    SensorPicksRepository,
    SensorHistoryRepository,
} from "#ie/repositories";
import { SensorVendorDataTransformation, SensorMeasurementsTransformation, SensorPicksTransformation } from "#ie/transformations";
import {
    sensorityDataSourceFactory,
    sensoneoDataSourceFactory,
    BaseSensorDataSourceFactory,
} from "#ie/datasources/SensorDataSource";
import { ISensorVendorDataInput } from "#sch/datasources/sensor/SensorVendorDataJsonSchema";
import { WasteCollection } from "#sch";
import MessageIntervalHelper from "#ie/helpers/MessageIntervalHelper";
import { ISensorMeasurementsInput } from "#sch/datasources/sensor/SensorMeasurementsJsonSchema";
import { ISensorPicksInput } from "#sch/datasources/sensor/SensorPicksJsonSchema";
import { SensorLastMeasurementsViewRepository } from "#ie/repositories/SensorLastMeasurementsViewRepository";
import { SensorLastPicksViewRepository } from "#ie/repositories/SensorLastPicksViewRepository";

export class SensorWorker extends BaseWorker {
    private sensorVendorDataRepository: SensorVendorDataRepository;
    private sensorMeasurementsRepository: SensorMeasurementsRepository;
    private sensorLastMeasurementsViewRepository: SensorLastMeasurementsViewRepository;
    private sensorHistoryRepository: SensorHistoryRepository;
    private sensorPicksRepository: SensorPicksRepository;
    private sensorLastPicksViewRepository: SensorLastPicksViewRepository;
    private queuePrefix: string = `${config.RABBIT_EXCHANGE_NAME}.${WasteCollection.name.toLowerCase()}`;

    constructor() {
        super();
        this.sensorVendorDataRepository = new SensorVendorDataRepository();
        this.sensorHistoryRepository = new SensorHistoryRepository();
        this.sensorMeasurementsRepository = new SensorMeasurementsRepository();
        this.sensorLastMeasurementsViewRepository = new SensorLastMeasurementsViewRepository();
        this.sensorPicksRepository = new SensorPicksRepository();
        this.sensorLastPicksViewRepository = new SensorLastPicksViewRepository();
    }

    public refreshSensorVendorData = async () => {
        try {
            await this.processSensorVendorData(sensoneoDataSourceFactory);
            await this.processSensorVendorData(sensorityDataSourceFactory);

            const { from, to } = MessageIntervalHelper.getDefaultInterval();

            await Promise.all([
                this.sendMessageToExchange(
                    "workers." + this.queuePrefix + ".refreshSensorMeasurements",
                    JSON.stringify({ from, to })
                ),
                this.sendMessageToExchange("workers." + this.queuePrefix + ".refreshSensorPicks", JSON.stringify({ from, to })),
            ]);
        } catch (err) {
            throw new CustomError(`Error while executing refreshSensorVendorData.`, true, this.constructor.name, 5001, err);
        }
    };

    private processSensorVendorData = async (sensorDataSourceFactory: BaseSensorDataSourceFactory) => {
        try {
            const datasource = await sensorDataSourceFactory.getVendorDataDataSource();
            const dataStream = await datasource.getAll(true);

            const sensorVendorDataTransformation = new SensorVendorDataTransformation(sensorDataSourceFactory.datasourceName);

            await dataStream
                .setDataProcessor(async (data: ISensorVendorDataInput[]) => {
                    const batch = await sensorVendorDataTransformation.transform(data);
                    await this.sensorVendorDataRepository.save(batch);
                    await this.sensorHistoryRepository.updateHistory(batch);
                })
                .proceed();
        } catch (err) {
            throw new CustomError(
                `Error while processing SensorVendorData (${sensorDataSourceFactory.datasourceName}).`,
                true,
                this.constructor.name,
                5001,
                err
            );
        }
    };

    public refreshSensorMeasurements = async (msg: any) => {
        try {
            await this.processSensorMeasurements(msg, sensoneoDataSourceFactory);
            await this.processSensorMeasurements(msg, sensorityDataSourceFactory);
            await this.sensorLastMeasurementsViewRepository.refreshView();
        } catch (err) {
            throw new CustomError(`Error while executing refreshSensorMeasurements.`, true, this.constructor.name, 5001, err);
        }
    };

    private processSensorMeasurements = async (msg: any, sensorDataSourceFactory: BaseSensorDataSourceFactory) => {
        try {
            const { from, to } = MessageIntervalHelper.getMessageInterval(msg);
            const datasource = await sensorDataSourceFactory.getMeasurementsDataSource(from, to);
            const dataStream = await datasource.getAll(true);

            const sensorMeasurementsTransformation = new SensorMeasurementsTransformation(sensorDataSourceFactory.datasourceName);

            await dataStream
                .setDataProcessor(async (data: ISensorMeasurementsInput[]) => {
                    const batch = await sensorMeasurementsTransformation.transform(data);
                    await this.sensorMeasurementsRepository.save(batch);
                })
                .proceed();
        } catch (err) {
            throw new CustomError(
                `Error while processing SensorMeasurements (${sensorDataSourceFactory.datasourceName}).`,
                true,
                this.constructor.name,
                5001,
                err
            );
        }
    };

    public refreshSensorPicks = async (msg: any) => {
        try {
            await this.processSensorPicks(msg, sensoneoDataSourceFactory);
            await this.processSensorPicks(msg, sensorityDataSourceFactory);
            await this.sensorLastPicksViewRepository.refreshView();
        } catch (err) {
            throw new CustomError(`Error while executing refreshSensorPicks.`, true, this.constructor.name, 5001, err);
        }
    };

    private processSensorPicks = async (msg: any, sensorDataSourceFactory: BaseSensorDataSourceFactory) => {
        const { from, to } = MessageIntervalHelper.getMessageInterval(msg);
        const updatedAt = new Date().toISOString();
        const datasource = await sensorDataSourceFactory.getPicksDataSource(from, to);
        const data = await datasource.getAll();

        const sensorPicksTransformation = new SensorPicksTransformation(sensorDataSourceFactory.datasourceName);
        const transformed = await sensorPicksTransformation.transform({ data, updatedAt });

        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();

        try {
            await this.sensorPicksRepository.bulkUpdate(transformed, t);
            await this.sensorPicksRepository.deleteOldData({ from, to }, updatedAt, sensorDataSourceFactory.datasourceName, t);
            await t.commit();
        } catch (err) {
            await t.rollback();
            throw new CustomError(
                `Error while processing SensorPicks (${sensorDataSourceFactory.datasourceName}).`,
                true,
                this.constructor.name,
                5001,
                err
            );
        }
    };

    public updateSensorsFromTo = async (msg: any): Promise<void> => {
        const { from, to } = MessageIntervalHelper.getMessageInterval(msg);
        return this.updateSensors(DateTime.fromJSDate(from), DateTime.fromJSDate(to));
    };

    public updateSensorsForMonth = async (): Promise<void> => {
        return this.updateSensors(DateTime.now().minus({ month: 1 }), DateTime.now());
    };

    public updateSensorsForPreviousTwoMonths = async (): Promise<void> => {
        return this.updateSensors(DateTime.now().minus({ month: 3 }), DateTime.now().minus({ month: 1 }));
    };

    private updateSensors = async (from: DateTime, to: DateTime): Promise<void> => {
        try {
            const intervals = MessageIntervalHelper.generateWeekIntervals(from, to);

            for (const interval of intervals) {
                const { from, to } = interval;
                await this.sendMessageToExchange(
                    "workers." + this.queuePrefix + ".refreshSensorMeasurements",
                    JSON.stringify({ from, to })
                );
                await this.sendMessageToExchange(
                    "workers." + this.queuePrefix + ".refreshSensorPicks",
                    JSON.stringify({ from, to })
                );
            }
        } catch (err) {
            throw new CustomError("Error while processing updateSensors.", true, this.constructor.name, 5001, err);
        }
    };
}
