import RfidPicksDatasource from "#ie/datasources/rfid/RfidPicksDatasource";
import MessageIntervalHelper, { IMessageInterval } from "#ie/helpers/MessageIntervalHelper";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import IDbRfidPick from "#ie/repositories/rfid/interfaces/IDbRfidPicks";
import { RfidPicksRepository } from "#ie/repositories/rfid/RfidPicksRepository";
import { ILogger } from "@golemio/core/dist/helpers";
import { AbstractTask, QueueManager } from "@golemio/core/dist/integration-engine";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { Point } from "geojson";
import { MODULE_NAME } from "src/constants";
import IDateTimeRange from "./interfaces/IDateTimeRange";
import { DateTimeRangeSchema } from "./schema/DateTimeRangeSchema";

@injectable()
export default class RfidDownloadPicksTask extends AbstractTask<IDateTimeRange> {
    public readonly queueName = "rfidDownloadPicks";
    public readonly queueTtl = 23 * 60 * 60 * 1000;
    public readonly schema = DateTimeRangeSchema;

    constructor(
        @inject(ModuleContainerToken.RfidPicksDatasource)
        private rfidPicksDatasource: RfidPicksDatasource,
        @inject(ModuleContainerToken.RfidPicksRepository)
        private rfidPicksRepository: RfidPicksRepository,
        @inject(ContainerToken.Logger) private logger: ILogger
    ) {
        super(MODULE_NAME);
    }

    protected async execute(params: IDateTimeRange): Promise<void> {
        try {
            const { from, to } = params.from
                ? { from: new Date(params.from), to: new Date(params.to) }
                : MessageIntervalHelper.getDefaultInterval();

            const intervals = MessageIntervalHelper.generateWeekIntervals(DateTime.fromJSDate(from), DateTime.fromJSDate(to));

            if (intervals.length > 1) {
                this.generateNewTasks(intervals);
            } else {
                await this.handleTask(from, to);
            }
        } catch (error) {
            if (error instanceof CustomError) {
                throw error;
            }

            this.logger.error(error, `Unable to download rfid picks with message: ${JSON.stringify(params)}`);
        }
    }

    private async handleTask(from: Date, to: Date) {
        const picksResult = await this.rfidPicksDatasource.getData({ from, to });

        const transformedData = picksResult.map((element) => {
            return {
                ...element,
                geometry: {
                    type: "Point",
                    coordinates: [element.pick_lon, element.pick_lat],
                } as Point,
            } as IDbRfidPick;
        });

        await this.rfidPicksRepository.bulkUpdate(transformedData);
    }

    private generateNewTasks(intervals: IMessageInterval[]) {
        for (const interval of intervals) {
            QueueManager.sendMessageToExchange(this.queuePrefix, this.queueName, {
                from: interval.from,
                to: interval.to,
            });
        }
    }
}
