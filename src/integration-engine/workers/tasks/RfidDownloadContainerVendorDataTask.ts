import RfidContainerVendorDatasource from "#ie/datasources/rfid/RfidContainerVendorDatasource";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { RfidContainerVendorDataRepository } from "#ie/repositories/rfid/RfidContainerVendorDataRepository";
import { RfidSensorHistoryRepository } from "#ie/repositories/rfid/RfidSensorHistoryRepository";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { MODULE_NAME } from "src/constants";

@injectable()
export default class RfidDownloadContainerVendorDataTask extends AbstractEmptyTask {
    public readonly queueName = "rfidDownloadContainerVendorData";
    public readonly queueTtl = 23 * 60 * 60 * 1000;

    constructor(
        @inject(ModuleContainerToken.RfidContainerVendorDatasource)
        private rfidContainerVendorDatasource: RfidContainerVendorDatasource,
        @inject(ModuleContainerToken.RfidContainerVendorDataRepository)
        private containerVendorDataRepository: RfidContainerVendorDataRepository,
        @inject(ModuleContainerToken.RfidSensorHistoryRepository)
        private rfidSensorHistoryRepository: RfidSensorHistoryRepository,
        @inject(ContainerToken.Logger) private logger: ILogger
    ) {
        super(MODULE_NAME);
    }

    protected async execute(): Promise<void> {
        try {
            const containerVendorResult = await this.rfidContainerVendorDatasource.getData();
            containerVendorResult.map((element) => {
                if (!element.vendor) {
                    element.vendor = null;
                }
            });

            await this.containerVendorDataRepository.bulkUpdate(containerVendorResult);

            await this.rfidSensorHistoryRepository.updateHistory(containerVendorResult);
        } catch (error) {
            if (error instanceof CustomError) {
                throw error;
            }

            this.logger.error(error, `Unable to download rfid container vendor data.`);
        }
    }
}
