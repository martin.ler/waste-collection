import { IsISO8601, IsOptional, IsString } from "@golemio/core/dist/shared/class-validator";
import IDateTimeRange from "../interfaces/IDateTimeRange";

export class DateTimeRangeSchema implements IDateTimeRange {
    @IsOptional()
    @IsString()
    @IsISO8601()
    from!: string;
    @IsOptional()
    @IsString()
    @IsISO8601()
    to!: string;
}
