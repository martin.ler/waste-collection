export default interface IDateTimeRange {
    from: string;
    to: string;
}
