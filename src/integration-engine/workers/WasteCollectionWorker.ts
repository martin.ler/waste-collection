import { WasteCollectionContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { AbstractTask, AbstractWorker } from "@golemio/core/dist/integration-engine";
import { MODULE_NAME } from "src/constants";
import CollectPickDatesTask from "./tasks/CollectPickDatesTask";
import RfidDownloadContainerVendorDataTask from "./tasks/RfidDownloadContainerVendorDataTask";
import RfidDownloadPicksTask from "./tasks/RfidDownloadPicksTask";

export class WasteCollectionWorker extends AbstractWorker {
    protected name = MODULE_NAME;

    constructor() {
        super();

        // Register tasks
        this.registerTask(new CollectPickDatesTask(this.getQueuePrefix()));
        this.registerTask(
            WasteCollectionContainer.resolve<RfidDownloadContainerVendorDataTask>(
                ModuleContainerToken.RfidDownloadContainerVendorDataTask
            )
        );
        this.registerTask(WasteCollectionContainer.resolve<RfidDownloadPicksTask>(ModuleContainerToken.RfidDownloadPicksTask));
    }

    public registerTask = (task: AbstractTask<any>): void => {
        super.registerTask(task);
        task.queuePrefix = this.getQueuePrefix();
    };
}
