import { KsnkoDataSourceFactory } from "#ie/datasources/KsnkoDataSourceFactory";
import { KsnkoContainersRepository } from "#ie/repositories/KsnkoContainersRepository";
import { KsnkoItemsRepository } from "#ie/repositories/KsnkoItemsRepository";
import { KsnkoStationsRepository } from "#ie/repositories/KsnkoStationsRepository";
import { IKsnkoStationsTransformationResult, KsnkoTransformation } from "#ie/transformations";
import { IKsnkoStationInput } from "#sch/datasources/ksnko/KsnkoStationJsonSchema";
import { IKsnkoStation } from "#sch/definitions/KsnkoStations";
import CityDistrictsModel from "@golemio/city-districts/dist/integration-engine/repositories/CityDistrictPostgresRepository";
import { BaseWorker } from "@golemio/core/dist/integration-engine";

export class KsnkoWorker extends BaseWorker {
    private transformation: KsnkoTransformation;
    private ksnkoStationsRepository: KsnkoStationsRepository;
    private ksnkoItemsRepository: KsnkoItemsRepository;
    private ksnkoContainersRepository: KsnkoContainersRepository;
    private cityDistrictRepository: CityDistrictsModel;

    constructor() {
        super();
        this.transformation = new KsnkoTransformation();
        this.ksnkoStationsRepository = new KsnkoStationsRepository();
        this.ksnkoItemsRepository = new KsnkoItemsRepository();
        this.ksnkoContainersRepository = new KsnkoContainersRepository();
        this.cityDistrictRepository = new CityDistrictsModel();
    }

    public refreshKsnkoData = async () => {
        const datasource = await KsnkoDataSourceFactory.getDataSource();
        const dataStream = await datasource.getAll(true);

        const stationsTransformed: IKsnkoStationsTransformationResult = {
            stations: [],
            containers: [],
            items: [],
        };
        await dataStream
            .setDataProcessor(async (data: IKsnkoStationInput[]) => {
                const batch = await this.transformation.transform(data);
                stationsTransformed.stations.push(...batch.stations);
                stationsTransformed.items.push(...batch.items);
                stationsTransformed.containers.push(...batch.containers);
            })
            .proceed();

        stationsTransformed.stations = await this.addDistrictSlug(stationsTransformed.stations);

        await this.ksnkoStationsRepository.updateActive(stationsTransformed.stations);
        await this.ksnkoContainersRepository.updateActive(stationsTransformed.containers);
        await this.ksnkoItemsRepository.updateActive(stationsTransformed.items);
    };

    private async addDistrictSlug(stations: IKsnkoStation[]) {
        for (let index = 0; index < stations.length; index++) {
            const element = stations[index];

            element.city_district_slug = await this.cityDistrictRepository.getDistrict(
                element.geom.coordinates[0],
                element.geom.coordinates[1]
            );
        }

        return stations;
    }
}
