import { Message } from "amqplib";
import { DateTime } from "@golemio/core/dist/shared/luxon";

export interface IMessageInterval {
    from: Date;
    to: Date;
}

export default class MessageIntervalHelper {
    public static getDefaultInterval(): IMessageInterval {
        const now = new Date();
        return {
            from: new Date(new Date().setHours(now.getHours() - 2)),
            to: now,
        };
    }

    public static getMessageInterval(msg: Message): IMessageInterval {
        try {
            const content = msg.content.toString();
            return MessageIntervalHelper.parseInterval(content);
        } catch (err) {
            throw new Error(`Error while parsing message input data: ${err}`);
        }
    }

    public static generateWeekIntervals(from: DateTime, to: DateTime): IMessageInterval[] {
        const breakpoints = [from];
        let tmpFrom = from.plus({ week: 1 });

        while (tmpFrom.diff(to, "milliseconds").milliseconds < 0) {
            breakpoints.push(tmpFrom);
            tmpFrom = tmpFrom.plus({ week: 1 });
        }

        if (tmpFrom.diff(to, "milliseconds").milliseconds >= 0) {
            breakpoints.push(to);
        }

        const intervalsOutput = [];
        for (let i = 0; i < breakpoints.length - 1; i++) {
            const current = breakpoints[i];
            const next = breakpoints[i + 1];
            intervalsOutput.push({ from: current.toJSDate(), to: next.toJSDate() });
        }

        return intervalsOutput;
    }

    private static parseInterval(content: string) {
        const { from, to } = JSON.parse(content);

        if (isNaN(Date.parse(from)) || isNaN(Date.parse(to))) {
            throw new Error(`Invalid "from" or "to" properties in message: "${content}"`);
        }

        return {
            from: new Date(from),
            to: new Date(to),
        };
    }
}
