import { DateTime } from "@golemio/core/dist/shared/luxon";
import { TIMEZONE } from "src/constants";
import IPickDateCreationDetails from "./interfaces/IPickDateCreationDetails";

export default class PsasParserHelper {
    private static GENERATE_FOR_NEXT_X_MONTHS = 6;
    public static SEPARATOR = ", ";
    private static dayList = ["Po", "Út", "St", "Čt", "Pá", "So", "Ne"];
    private static regexWeekNumbers: RegExp = new RegExp("\\d+", "gm");
    private static regexInputYearIsoWeeks = new RegExp("^\\d{1,4}-.+$");

    public static parseYearIsoWeeks(value: string | undefined | null): IPickDateCreationDetails {
        if (value && this.regexInputYearIsoWeeks.test(value)) {
            const temp = value.split("-");
            const year = temp[0];
            const other = temp.slice(1).join(" ");
            const days = this.dayList.filter((el) => other.includes(el));
            const weekNumber = Array.from(other.matchAll(this.regexWeekNumbers)).map((el) => el[0]);

            return {
                year: Number.parseInt(year),
                days: days.join(this.SEPARATOR),
                weekNumbers: weekNumber.join(this.SEPARATOR),
            };
        }

        return { year: undefined, days: undefined, weekNumbers: undefined };
    }

    public static *generatePickDates(isoWeeks: string[], dayShortcuts: string[], validToIso: string | undefined) {
        const validTo: DateTime | undefined = validToIso ? DateTime.fromISO(validToIso) : undefined;

        if (isoWeeks.length === 0) {
            isoWeeks = this.numberRange(1, DateTime.now().weeksInWeekYear).map((el) => el.toString());
        }
        const years = [new Date().getFullYear(), new Date().getFullYear() + 1];

        for (const year of years) {
            for (const isoWeek of isoWeeks) {
                for (const dayShortcut of dayShortcuts) {
                    const generatedDate = PsasParserHelper.generateDateFromIsoWeek(isoWeek, dayShortcut, year);
                    if (PsasParserHelper.isValidDate(generatedDate, validTo)) {
                        yield generatedDate;
                    }
                }
            }
        }
    }

    private static isValidDate(generatedDate: DateTime, validTo: DateTime | undefined) {
        return (
            generatedDate > DateTime.now() && // only dates in future
            (!validTo || generatedDate <= validTo) && // that are before validTo definition
            generatedDate < DateTime.now().plus({ month: this.GENERATE_FOR_NEXT_X_MONTHS }) // up to X months into the future
        );
    }

    public static generateDateFromIsoWeek(weekNumber: string, dayName: string, year: number): DateTime {
        const weekNumberPad2 = weekNumber.padStart(2, "0");
        const dayNumber = this.dayList.indexOf(dayName) + 1;

        return DateTime.fromISO(`${year}-W${weekNumberPad2}-${dayNumber}`, { zone: TIMEZONE });
    }

    private static numberRange(start: number, end: number): number[] {
        // returns array [start, start+1, start+2, ..., end]
        return new Array(end + 1 - start).fill(1).map((d, i) => i + start);
    }
}
