export default interface IPickDateCreationDetails {
    year: number | undefined;
    days: string | undefined;
    weekNumbers: string | undefined;
}
