import axios from "@golemio/core/dist/shared/axios";
import { config } from "@golemio/core/dist/integration-engine";

export const getKsnkoToken = async (): Promise<string> => {
    try {
        const response = await axios.post(
            `${config.datasources.KSNKOApi.url}/login`,
            {
                password: config.datasources.KSNKOApi.password,
                username: config.datasources.KSNKOApi.user,
            },
            {
                headers: {
                    "Content-Type": "application/json",
                },
            }
        );
        return response.data.token;
    } catch (err) {
        throw new Error(`Can not obtain KSNKO auth token: ${err}`);
    }
};
