import { WasteCollection } from "#sch";
import IRfidPick from "#sch/datasources/rfid/IRfidPick";
import { DataSource, HTTPProtocolStrategy, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { IntegrationEngineConfiguration } from "@golemio/core/dist/integration-engine/config/IntegrationEngineConfiguration";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { MODULE_NAME } from "src/constants";
import AbstractRfidDataSource from "./AbstractRfidDataSource";

@injectable()
export default class RfidPicksDatasource extends AbstractRfidDataSource<IRfidPick> {
    protected url: string;
    protected code: string;

    constructor(@inject(ContainerToken.Config) config: IntegrationEngineConfiguration) {
        super();
        this.url = config.datasources["waste-collection"].rfid.picksUrl;
        this.code = config.datasources["waste-collection"].rfid.picksCode;
    }

    public getData = async (params: { from: Date; to: Date }): Promise<IRfidPick[]> => {
        const strategy = new HTTPProtocolStrategy({
            method: "POST",
            url: this.getUrl(),
            body: this.getRequestBody(params),
            headers: { "Content-Type": "application/json" },
        });

        const dataSource = new DataSource(
            MODULE_NAME + "RfidPicksDataSource",
            strategy,
            new JSONDataTypeStrategy({ resultsPath: "picks" }),
            new JSONSchemaValidator(
                WasteCollection.datasources.rfid.picks.name,
                WasteCollection.datasources.rfid.picks.jsonSchema
            )
        );

        const result = await dataSource.getAll();

        return result;
    };

    protected getRequestBody(params?: { from: Date; to: Date }): string {
        return JSON.stringify({
            vendor_id: [],
            ksnko_container_id: [],
            measured_at: {
                start: params?.from,
                end: params?.to,
            },
        });
    }
}
