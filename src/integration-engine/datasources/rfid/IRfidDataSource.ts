export default interface IRfidDataSource<T> {
    getData(params?: any): Promise<T[]>;
}
