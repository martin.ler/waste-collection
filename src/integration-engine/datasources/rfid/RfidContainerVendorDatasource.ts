import { WasteCollection } from "#sch";
import IRfidContainerVendorData from "#sch/datasources/rfid/IRfidContainerVendorData";
import { DataSource, HTTPProtocolStrategy, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { IntegrationEngineConfiguration } from "@golemio/core/dist/integration-engine/config/IntegrationEngineConfiguration";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { MODULE_NAME } from "src/constants";
import AbstractRfidDataSource from "./AbstractRfidDataSource";

@injectable()
export default class RfidContainerVendorDatasource extends AbstractRfidDataSource<IRfidContainerVendorData> {
    protected url: string;
    protected code: string;

    constructor(@inject(ContainerToken.Config) config: IntegrationEngineConfiguration) {
        super();
        this.url = config.datasources["waste-collection"].rfid.containerVendorUrl;
        this.code = config.datasources["waste-collection"].rfid.containerVendorCode;
    }

    public getData = async (): Promise<IRfidContainerVendorData[]> => {
        const dataSource = new DataSource(
            MODULE_NAME + "RfidContainerVendorDatasource",
            new HTTPProtocolStrategy({
                method: "POST",
                url: this.getUrl(),
                body: this.getRequestBody(),
                headers: { "Content-Type": "application/json" },
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                WasteCollection.datasources.rfid.containerVendorData.name,
                WasteCollection.datasources.rfid.containerVendorData.jsonSchema
            )
        );

        const result = await dataSource.getAll();

        return result.container_vendor_data.map((element: any) => {
            element.vendor = result.vendor;
            return element;
        });
    };
}
