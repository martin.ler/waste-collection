import IRfidDataSource from "./IRfidDataSource";

export default abstract class AbstractRfidDataSource<T> implements IRfidDataSource<T> {
    protected abstract url: string;
    protected abstract code: string;

    abstract getData(params?: any): Promise<T[]>;

    protected getRequestBody(params?: any): string {
        return "{}";
    }

    protected getUrl() {
        const urlWithParams = new URL(this.url);

        urlWithParams.searchParams.set("code", this.code);

        return urlWithParams.href;
    }
}
