import JSONStream from "JSONStream";
import { config, HTTPProtocolStrategy, HTTPProtocolStrategyStreamed } from "@golemio/core/dist/integration-engine";
import { DatasourceName } from "src/constants";
import { BaseSensorDataSourceFactory } from "./BaseSensorDataSourceFactory";

export class SensorityDataSourceFactory extends BaseSensorDataSourceFactory {
    public datasourceName: DatasourceName = config.datasources.WasteCollectionSensorSources.sensorityApi.name;

    protected getVendorDataHTTPProtocolStrategy = (): HTTPProtocolStrategyStreamed => {
        const apiConfig = config.datasources.WasteCollectionSensorSources.sensorityApi;
        return new HTTPProtocolStrategyStreamed({
            method: "POST",
            url: `${apiConfig.url}/golemio-container-vendor-data`,
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${apiConfig.token}`,
            },
        }).setStreamTransformer(JSONStream.parse("container_vendor_data.*"));
    };

    protected getMeasurementsHTTPProtocolStrategy = (from: Date, to: Date): HTTPProtocolStrategyStreamed => {
        const apiConfig = config.datasources.WasteCollectionSensorSources.sensorityApi;
        return new HTTPProtocolStrategyStreamed({
            method: "POST",
            url: `${apiConfig.url}/golemio-container-measurements`,
            body: {
                measured_at: {
                    start: from,
                    end: to,
                },
            },
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${apiConfig.token}`,
            },
        }).setStreamTransformer(JSONStream.parse("measurements.*"));
    };

    protected getPicksHTTPProtocolStrategy = (from: Date, to: Date): HTTPProtocolStrategy => {
        const apiConfig = config.datasources.WasteCollectionSensorSources.sensorityApi;
        return new HTTPProtocolStrategy({
            method: "POST",
            url: `${apiConfig.url}/golemio-container-picks`,
            body: {
                pick_at: {
                    start: from,
                    end: to,
                },
            },
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${apiConfig.token}`,
            },
        });
    };
}
