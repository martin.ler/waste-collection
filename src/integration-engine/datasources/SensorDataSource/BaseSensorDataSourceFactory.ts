import {
    DataSourceStreamed,
    HTTPProtocolStrategyStreamed,
    HTTPProtocolStrategy,
    JSONDataTypeStrategy,
    DataSource,
} from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { WasteCollection } from "#sch";
import { DatasourceName } from "src/constants";

export abstract class BaseSensorDataSourceFactory {
    public abstract datasourceName: DatasourceName;

    public async getVendorDataDataSource(): Promise<DataSourceStreamed> {
        const sourceHTTPProtocolStrategy = this.getVendorDataHTTPProtocolStrategy();

        return new DataSourceStreamed(
            WasteCollection.datasources.sensorVendorDataDatasource.name + ` (${this.datasourceName})`,
            sourceHTTPProtocolStrategy,
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                WasteCollection.datasources.sensorVendorDataDatasource.name + ` (${this.datasourceName})`,
                WasteCollection.datasources.sensorVendorDataDatasource.jsonSchema
            )
        );
    }

    public async getMeasurementsDataSource(from: Date, to: Date): Promise<DataSourceStreamed> {
        const sourceHTTPProtocolStrategy = this.getMeasurementsHTTPProtocolStrategy(from, to);

        return new DataSourceStreamed(
            WasteCollection.datasources.sensorMeasurementsDatasource.name + ` (${this.datasourceName})`,
            sourceHTTPProtocolStrategy,
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                WasteCollection.datasources.sensorMeasurementsDatasource.name + ` (${this.datasourceName})`,
                WasteCollection.datasources.sensorMeasurementsDatasource.jsonSchema
            )
        );
    }

    public async getPicksDataSource(from: Date, to: Date): Promise<DataSource> {
        const sourceHTTPProtocolStrategy = this.getPicksHTTPProtocolStrategy(from, to);

        return new DataSource(
            WasteCollection.datasources.sensorPicksDatasource.name + ` (${this.datasourceName})`,
            sourceHTTPProtocolStrategy,
            new JSONDataTypeStrategy({ resultsPath: "picks" }),
            new JSONSchemaValidator(
                WasteCollection.datasources.sensorPicksDatasource.name + ` (${this.datasourceName})`,
                WasteCollection.datasources.sensorPicksDatasource.jsonSchema
            )
        );
    }

    protected abstract getVendorDataHTTPProtocolStrategy(): HTTPProtocolStrategyStreamed;
    protected abstract getMeasurementsHTTPProtocolStrategy(from: Date, to: Date): HTTPProtocolStrategyStreamed;
    protected abstract getPicksHTTPProtocolStrategy(from: Date, to: Date): HTTPProtocolStrategy;
}
