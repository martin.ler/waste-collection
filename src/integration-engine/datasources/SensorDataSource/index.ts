import { BaseSensorDataSourceFactory } from "./BaseSensorDataSourceFactory";
import { SensoneoDataSourceFactory } from "./SensoneoDataSourceFactory";
import { SensorityDataSourceFactory } from "./SensorityDataSourceFactory";

const sensorityDataSourceFactory = new SensorityDataSourceFactory();
const sensoneoDataSourceFactory = new SensoneoDataSourceFactory();

export { sensorityDataSourceFactory, sensoneoDataSourceFactory, BaseSensorDataSourceFactory };
