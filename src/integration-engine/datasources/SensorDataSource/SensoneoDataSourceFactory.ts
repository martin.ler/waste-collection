import JSONStream from "JSONStream";
import { config, HTTPProtocolStrategy, HTTPProtocolStrategyStreamed } from "@golemio/core/dist/integration-engine";
import { DatasourceName } from "src/constants";
import { BaseSensorDataSourceFactory } from "./BaseSensorDataSourceFactory";

export class SensoneoDataSourceFactory extends BaseSensorDataSourceFactory {
    public datasourceName: DatasourceName = config.datasources.WasteCollectionSensorSources.sensoneoApi.name;

    protected getVendorDataHTTPProtocolStrategy = (): HTTPProtocolStrategyStreamed => {
        const apiConfig = config.datasources.WasteCollectionSensorSources.sensoneoApi;
        return new HTTPProtocolStrategyStreamed({
            method: "POST",
            url: `${apiConfig.url}/container_vendor_data?code=${apiConfig.code}`,
            body: {
                vendor_id: [],
                ksnko_container_id: [],
            },
            headers: { "Content-Type": "application/json" },
        }).setStreamTransformer(JSONStream.parse("container_vendor_data.*"));
    };

    protected getMeasurementsHTTPProtocolStrategy = (from: Date, to: Date): HTTPProtocolStrategyStreamed => {
        const apiConfig = config.datasources.WasteCollectionSensorSources.sensoneoApi;
        return new HTTPProtocolStrategyStreamed({
            method: "POST",
            url: `${apiConfig.url}/measurements?code=${apiConfig.code}`,
            body: {
                vendor_id: [],
                ksnko_container_id: [],
                measured_at: {
                    start: from,
                    end: to,
                },
            },
            headers: { "Content-Type": "application/json" },
        }).setStreamTransformer(JSONStream.parse("measurements.*"));
    };

    protected getPicksHTTPProtocolStrategy = (from: Date, to: Date): HTTPProtocolStrategy => {
        const apiConfig = config.datasources.WasteCollectionSensorSources.sensoneoApi;
        return new HTTPProtocolStrategy({
            method: "POST",
            url: `${apiConfig.url}/picks?code=${apiConfig.code}`,
            body: {
                vendor_id: [],
                ksnko_container_id: [],
                measured_at: {
                    start: from,
                    end: to,
                },
            },
            headers: { "Content-Type": "application/json" },
        });
    };
}
