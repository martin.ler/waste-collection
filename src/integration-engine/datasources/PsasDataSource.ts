import { WasteCollection } from "#sch";
import { DataSourceStreamed, HTTPProtocolStrategyStreamed, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import axios from "@golemio/core/dist/shared/axios";
import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import https from "https";
import JSONStream from "JSONStream";
import { MODULE_NAME } from "src/constants";
export default class PsasDataSource {
    private url: string;
    private loginPath = "/api/User/Login";
    private dataPath = "/api/View/oict_nadoby";
    private user: string;
    private password: string;

    constructor(url: string, user: string, password: string) {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    public getData = async (): Promise<DataSourceStreamed> => {
        const token = await this.getAuthorization();
        const psasStrategy = new HTTPProtocolStrategyStreamed({
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json",
            },
            method: "GET",
            url: new URL(this.dataPath, this.url).href,
            rejectUnauthorized: false,
        }).setStreamTransformer(JSONStream.parse("data.*"));

        return new DataSourceStreamed(
            MODULE_NAME + "RawPickDatesDataSourceStreamed",
            psasStrategy,
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                WasteCollection.datasources.rawPickDates.name,
                WasteCollection.datasources.rawPickDates.jsonSchema
            )
        );
    };

    private getAuthorization = async (): Promise<string> => {
        try {
            const result = await axios.post(
                new URL(this.loginPath, this.url).href,
                { login: this.user, password: this.password },
                {
                    headers: {
                        "Content-Type": "application/json",
                    },
                    httpsAgent: new https.Agent({
                        rejectUnauthorized: false,
                    }),
                }
            );

            return result.data.data.token;
        } catch (err) {
            throw new CustomError("Unable to refresh token", true, PsasDataSource.name, undefined, err);
        }
    };
}
