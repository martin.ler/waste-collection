import JSONStream from "JSONStream";
import {
    config,
    DataSourceStreamed,
    HTTPProtocolStrategyStreamed,
    JSONDataTypeStrategy,
} from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { WasteCollection } from "#sch";
import { getKsnkoToken } from "#ie/helpers/getKsnkoToken";

export class KsnkoDataSourceFactory {
    public static async getDataSource(): Promise<DataSourceStreamed> {
        const token = await getKsnkoToken();

        const ksnkoStrategy = new HTTPProtocolStrategyStreamed({
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json",
            },
            method: "GET",
            url: `${config.datasources.KSNKOApi.url}/stations?full=1&detail=1`,
        }).setStreamTransformer(JSONStream.parse("data.*"));

        return new DataSourceStreamed(
            WasteCollection.datasources.ksnkoStationsDatasource.name,
            ksnkoStrategy,
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                WasteCollection.datasources.ksnkoStationsDatasource.name,
                WasteCollection.datasources.ksnkoStationsDatasource.jsonSchema
            )
        );
    }
}
