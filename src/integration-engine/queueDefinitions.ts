import { KsnkoWorker } from "#ie/workers/KsnkoWorker";
import { SensorWorker } from "#ie/workers/SensorWorker";
import { WasteCollection } from "#sch";
import { config } from "@golemio/core/dist/integration-engine/config";
import { IQueueDefinition } from "@golemio/core/dist/integration-engine/queueprocessors";

const queueDefinitions: IQueueDefinition[] = [
    {
        name: "Ksnko" + WasteCollection.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + WasteCollection.name.toLowerCase(),
        queues: [
            {
                name: "refreshKsnkoData",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 10 * 60 * 1000, // 10 minutes
                },
                worker: KsnkoWorker,
                workerMethod: "refreshKsnkoData",
            },
        ],
    },
    {
        name: "Sensor" + WasteCollection.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + WasteCollection.name.toLowerCase(),
        queues: [
            {
                name: "refreshSensorVendorData",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: SensorWorker,
                workerMethod: "refreshSensorVendorData",
            },
            {
                name: "refreshSensorMeasurements",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: SensorWorker,
                workerMethod: "refreshSensorMeasurements",
            },
            {
                name: "refreshSensorPicks",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: SensorWorker,
                workerMethod: "refreshSensorPicks",
            },
            {
                name: "updateSensorsForMonth",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: SensorWorker,
                workerMethod: "updateSensorsForMonth",
            },
            {
                name: "updateSensorsForPreviousTwoMonths",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: SensorWorker,
                workerMethod: "updateSensorsForPreviousTwoMonths",
            },
            {
                name: "updateSensorsFromTo",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: SensorWorker,
                workerMethod: "updateSensorsFromTo",
            },
        ],
    },
];

export { queueDefinitions };
