import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { WasteCollection } from "#sch";
import { ISensorPicksInput } from "#sch/datasources/sensor/SensorPicksJsonSchema";
import { ISensorPick } from "#sch/definitions/SensorPicks";

export type TSensorPickUpdated = ISensorPick & { updated_at: string };

type TSensorPickInputUpdated = {
    data: ISensorPicksInput[];
    updatedAt: string;
};

export class SensorPicksTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor(vendor: string) {
        super();
        this.name = vendor + WasteCollection.datasources.sensorPicksDatasource.name + "Transformation";
    }

    public transform = async ({ data, updatedAt }: TSensorPickInputUpdated): Promise<TSensorPickUpdated[]> => {
        const res: TSensorPickUpdated[] = [];

        for (const item of data) {
            const transformedData = this.transformElement({ ...item, updatedAt });
            res.push(transformedData);
        }

        return res;
    };

    protected transformElement = (element: ISensorPicksInput & { updatedAt: string }): TSensorPickUpdated => {
        return {
            vendor_id: element.vendor_id,
            ksnko_container_id: element.ksnko_container_id,
            pick_at: element.pick_at,
            percent_before: element.percent_before,
            percent_now: element.percent_now,
            event_driven: element.event_driven,
            updated_at: element.updatedAt,
        };
    };
}
