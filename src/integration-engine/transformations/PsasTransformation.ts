import PsasParserHelper from "#ie/helpers/PsasParserHelper";
import { WasteCollection } from "#sch";
import IRawPickDatesInput from "#sch/datasources/psas/IRawPickDatesInput";
import IRawPickDates from "#sch/definitions/models/interfaces/IRawPickDates";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { TIMEZONE } from "src/constants";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import { TrashTypeMappingRepository } from "#ie/repositories/TrashTypeMappingRepository";
export class PsasTransformation extends BaseTransformation implements ITransformation {
    private trashTypeMapper: TrashTypeMappingRepository;
    name: string;

    constructor() {
        super();
        this.name = WasteCollection.datasources.rawPickDates.name + "Transformation";
        this.trashTypeMapper = TrashTypeMappingRepository.getInstance();
    }

    protected transformElement = async (element: IRawPickDatesInput): Promise<IRawPickDates> => {
        const pickDateDetails = PsasParserHelper.parseYearIsoWeeks(element.tydny_roku);
        const trashTypeDetails = await this.trashTypeMapper.findByPsasCode(element.typ_odpadu);

        return {
            date: this.formatDate(element.datum),
            subject_id: element.cislo_subjektu,
            count: element.pocet,
            container_type: element.typ_nadoby,
            container_volume: element.objem,
            frequency: element.cetnost,
            street_name: element.ulice,
            trash_type: element.typ_odpadu,
            trash_type_code: trashTypeDetails?.code_ksnko,
            trash_type_name: trashTypeDetails?.name,
            orientation_number: element.cor,
            address_char: element.pism?.slice(0, 5),
            conscription_number: element.cpop ? element.cpop : undefined,
            valid_from: this.formatDate(element.platnost_od),
            valid_to: element.platnost_do ? this.formatDate(element.platnost_do) : undefined,
            station_number: element.stanoviste ? element.stanoviste : undefined,
            year_days_isoweeks: element.tydny_roku ? element.tydny_roku : undefined,
            year: pickDateDetails.year,
            days: pickDateDetails.days && pickDateDetails.days.length > 0 ? pickDateDetails.days : undefined,
            isoweeks:
                pickDateDetails.weekNumbers && pickDateDetails.weekNumbers.length > 0 ? pickDateDetails.weekNumbers : undefined,
            company: element.svoz_firma,
        };
    };

    private formatDate(dateWithoutTimezone: string): string {
        return DateTime.fromISO(dateWithoutTimezone, { zone: TIMEZONE }).toISO();
    }
}
