import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { WasteCollection } from "#sch";
import { SensorLastMeasurementViewModel } from "#sch/definitions/models/SensorLastMeasurementViewModel";

export class SensorLastMeasurementsViewRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            WasteCollection.definitions.sensorLastMeasurementsView.name + "Repository",
            {
                outputSequelizeAttributes: SensorLastMeasurementViewModel.attributeModel,
                pgTableName: WasteCollection.definitions.sensorLastMeasurementsView.pgTableName,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(WasteCollection.definitions.sensorLastMeasurementsView.name + "Validator", {})
        );
    }

    public refreshView = async () => {
        const connection = PostgresConnector.getConnection();
        return connection.query(`REFRESH MATERIALIZED VIEW CONCURRENTLY "waste_collection".v_last_measurements;`, {
            type: Sequelize.QueryTypes.SELECT,
        });
    };
}
