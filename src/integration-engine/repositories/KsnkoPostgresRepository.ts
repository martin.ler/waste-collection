import { config, log, PostgresConnector } from "@golemio/core/dist/integration-engine";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { literal, Op } from "@golemio/core/dist/shared/sequelize";

export abstract class KsnkoPostgresRepository extends PostgresModel implements IModel {
    public updateActive = async (data: any[]) => {
        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();

        try {
            const promises = data.map(async (d) => {
                return this.sequelizeModel.upsert(d, { transaction: t, returning: config.NODE_ENV === "test" });
            });
            const rows = await Promise.all(promises);

            await this.sequelizeModel.update(
                { active: false },
                {
                    transaction: t,
                    where: {
                        active: true,
                        updated_at: {
                            [Op.lte]: literal(`NOW() - INTERVAL '5 minutes'`),
                        },
                    },
                }
            );

            await t.commit();
            return rows;
        } catch (err) {
            log.error(JSON.stringify({ message: err.message, errors: err.errors, fields: err.fields }));
            await t.rollback();
            throw new CustomError("Error while saving data", true, this.constructor.name, 4001, err);
        }
    };
}
