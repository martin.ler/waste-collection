import { KsnkoPostgresRepository } from "#ie/repositories/KsnkoPostgresRepository";
import { WasteCollection } from "#sch";
import { KsnkoStationContainerModel } from "#sch/definitions/models/KsnkoStationContainerModel";
import { log } from "@golemio/core/dist/integration-engine";
import { IModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class KsnkoContainersRepository extends KsnkoPostgresRepository implements IModel {
    constructor() {
        super(
            WasteCollection.definitions.ksnkoStationContainers.name + "Repository",
            {
                outputSequelizeAttributes: KsnkoStationContainerModel.attributeModel,
                pgTableName: WasteCollection.definitions.ksnkoStationContainers.pgTableName,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                WasteCollection.definitions.ksnkoStationContainers.name + "Validator",
                KsnkoStationContainerModel.jsonSchema
            )
        );
    }

    public getIdByTypeAndStation = async (
        trashType: string | undefined,
        stationNumber: string | undefined
    ): Promise<number[] | null> => {
        if (trashType && stationNumber) {
            const result = await this.sequelizeModel.findAll<KsnkoStationContainerModel>({
                where: { station_number: stationNumber, trash_type: trashType },
            });

            if (result) {
                return result.map((el) => el.id);
            }
        }

        log.warn(`Unable to find Ksnko Containers for station number ${stationNumber} and trash type: ${trashType}`);
        return null;
    };
}
