import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { WasteCollection } from "#sch";
import { SensorVendorDataModel } from "#sch/definitions/models/SensorVendorDataModel";

export class SensorVendorDataRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            WasteCollection.definitions.sensorVendorData.name + "Repository",
            {
                outputSequelizeAttributes: SensorVendorDataModel.attributeModel,
                pgTableName: WasteCollection.definitions.sensorVendorData.pgTableName,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                WasteCollection.definitions.sensorVendorData.name + "Validator",
                SensorVendorDataModel.jsonSchema
            )
        );
    }
}
