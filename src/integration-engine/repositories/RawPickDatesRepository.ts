import { WasteCollection } from "#sch";
import IRawPickDates from "#sch/definitions/models/interfaces/IRawPickDates";
import RawPickDates from "#sch/definitions/models/RawPickDatesModel";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class RawPickDatesRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            WasteCollection.definitions.rawPickDates.name + "Repository",
            {
                outputSequelizeAttributes: RawPickDates.attributeModel,
                pgTableName: WasteCollection.definitions.rawPickDates.pgTableName,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(WasteCollection.definitions.rawPickDates.name + "Validator", RawPickDates.jsonSchema)
        );
    }

    public saveBulk = async (data: IRawPickDates[]) => {
        if (await this.validate(data)) {
            const fieldsToUpdate = Object.getOwnPropertyNames(data[0])
                .filter((el) => el !== "subject_id")
                .map<keyof IRawPickDates>((el) => el as keyof IRawPickDates);
            fieldsToUpdate.push("updated_at" as any);
            await this.sequelizeModel.bulkCreate<RawPickDates>(data, { updateOnDuplicate: fieldsToUpdate });
        }
    };
}
