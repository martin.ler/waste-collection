import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { WasteCollection } from "#sch";
import { ISensorVendorData } from "#sch/definitions/SensorVendorData";
import { SensorHistoryModel } from "#sch/definitions/models/SensorHistoryModel";

export class SensorHistoryRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            WasteCollection.definitions.sensorHistory.name + "Repository",
            {
                outputSequelizeAttributes: SensorHistoryModel.attributeModel,
                pgTableName: WasteCollection.definitions.sensorHistory.pgTableName,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(WasteCollection.definitions.sensorHistory.name + "Validator", SensorHistoryModel.jsonSchema)
        );
    }

    public updateHistory = async (data: ISensorVendorData[]) => {
        const now = new Date().toISOString();
        const jsonStr = JSON.stringify(
            data.map((el) => ({
                vendor_id: el.vendor_id,
                sensor_id: el.sensor_id,
            }))
        )
            .replace(/'/g, "\\'")
            .replace(/\"/g, '\\"');

        await this.query(`
            SELECT ${WasteCollection.pgSchema}.import_sensor_history(E'${jsonStr}'::json, '${now}');
        `);
    };
}
