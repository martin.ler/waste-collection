import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { IModel } from "@golemio/core/dist/integration-engine/models";
import { WasteCollection } from "#sch";
import { KsnkoPostgresRepository } from "#ie/repositories/KsnkoPostgresRepository";
import { KsnkoStationItemModel } from "#sch/definitions/models/KsnkoStationItemModel";

export class KsnkoItemsRepository extends KsnkoPostgresRepository implements IModel {
    constructor() {
        super(
            WasteCollection.definitions.ksnkoStationItems.name + "Repository",
            {
                outputSequelizeAttributes: KsnkoStationItemModel.attributeModel,
                pgTableName: WasteCollection.definitions.ksnkoStationItems.pgTableName,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                WasteCollection.definitions.ksnkoStationItems.name + "Validator",
                KsnkoStationItemModel.jsonSchema
            )
        );
    }
}
