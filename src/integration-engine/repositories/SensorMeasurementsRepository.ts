import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { WasteCollection } from "#sch";
import { SensorMeasurementModel } from "#sch/definitions/models/SensorMeasurementModel";

export class SensorMeasurementsRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            WasteCollection.definitions.sensorMeasurements.name + "Repository",
            {
                outputSequelizeAttributes: SensorMeasurementModel.attributeModel,
                pgTableName: WasteCollection.definitions.sensorMeasurements.pgTableName,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                WasteCollection.definitions.sensorMeasurements.name + "Validator",
                SensorMeasurementModel.jsonSchema
            )
        );
    }
}
