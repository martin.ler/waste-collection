import IRfidPick from "#sch/datasources/rfid/IRfidPick";
import { Point } from "geojson";

export default interface IDbRfidPick extends IRfidPick {
    geometry: Point;
}
