import { WasteCollection } from "#sch";
import IRfidContainerVendorData from "#sch/datasources/rfid/IRfidContainerVendorData";
import { SensorHistoryModel } from "#sch/definitions/models/SensorHistoryModel";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class RfidSensorHistoryRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            WasteCollection.definitions.rfid.sensorHistory.name + "Repository",
            {
                outputSequelizeAttributes: SensorHistoryModel.attributeModel,
                pgTableName: WasteCollection.definitions.rfid.sensorHistory.pgTableName,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(WasteCollection.definitions.sensorHistory.name + "Validator", SensorHistoryModel.jsonSchema)
        );
    }

    public updateHistory = async (data: IRfidContainerVendorData[]) => {
        const now = new Date().toISOString();
        const jsonStr = JSON.stringify(
            data.map((el) => ({
                vendor_id: el.vendor_id,
                sensor_id: el.sensor_id,
            }))
        )
            .replace(/'/g, "\\'")
            .replace(/\"/g, '\\"');

        await this.query(`
            SELECT ${WasteCollection.pgSchema}.import_sensor_history_rfid(E'${jsonStr}'::json, '${now}');
        `);
    };
}
