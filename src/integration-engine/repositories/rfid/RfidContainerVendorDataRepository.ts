import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { WasteCollection } from "#sch";
import IRfidContainerVendorData from "#sch/datasources/rfid/IRfidContainerVendorData";
import { RfidContainerVendorDataModel } from "#sch/definitions/models/rfid/RfidContainerVendorDataModel";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

@injectable()
export class RfidContainerVendorDataRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            WasteCollection.definitions.rfid.containerVendorData.name + "Repository",
            {
                outputSequelizeAttributes: RfidContainerVendorDataModel.attributeModel,
                pgTableName: WasteCollection.definitions.rfid.containerVendorData.pgTableName,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(WasteCollection.definitions.rfid.name + "Validator", RfidContainerVendorDataModel.jsonSchema)
        );
    }

    public bulkUpdate = async (data: IRfidContainerVendorData[]) => {
        await this.validate(data);
        await this.sequelizeModel.bulkCreate<RfidContainerVendorDataModel>(data, {
            updateOnDuplicate: this.getUpdateAttributes(),
        });
    };

    private getUpdateAttributes = () => {
        return Object.keys(RfidContainerVendorDataModel.attributeModel)
            .filter((att) => !["vendor_id"].includes(att))
            .concat("updated_at") as Array<keyof IRfidContainerVendorData>;
    };
}
