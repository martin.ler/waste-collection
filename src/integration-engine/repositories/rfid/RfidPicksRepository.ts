import { WasteCollection } from "#sch";
import IRfidPick from "#sch/datasources/rfid/IRfidPick";
import { RfidPickModel } from "#sch/definitions/models/rfid/RfidPickModel";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import IDbRfidPick from "./interfaces/IDbRfidPicks";

@injectable()
export class RfidPicksRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            WasteCollection.definitions.rfid.picks.name + "Repository",
            {
                outputSequelizeAttributes: RfidPickModel.attributeModel,
                pgTableName: WasteCollection.definitions.rfid.picks.pgTableName,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(WasteCollection.definitions.rfid.name + "Validator", RfidPickModel.jsonSchema)
        );
    }

    public bulkUpdate = async (data: IDbRfidPick[]) => {
        await this.validate(data);
        await this.sequelizeModel.bulkCreate<RfidPickModel>(data, {
            updateOnDuplicate: this.getUpdateAttributes(),
        });
    };

    private getUpdateAttributes = () => {
        return Object.keys(RfidPickModel.attributeModel)
            .filter((att) => !["vendor_id", "pick_at"].includes(att))
            .concat("updated_at") as Array<keyof IRfidPick>;
    };
}
