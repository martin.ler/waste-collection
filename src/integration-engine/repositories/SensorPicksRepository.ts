import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { Op, Transaction } from "@golemio/core/dist/shared/sequelize";
import { log } from "@golemio/core/dist/integration-engine";
import { IMessageInterval } from "#ie/helpers/MessageIntervalHelper";
import { WasteCollection } from "#sch";
import { SensorPickModel } from "#sch/definitions/models/SensorPickModel";
import { ISensorPick } from "#sch/definitions/SensorPicks";

export class SensorPicksRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            WasteCollection.definitions.sensorPicks.name + "Repository",
            {
                outputSequelizeAttributes: SensorPickModel.attributeModel,
                pgTableName: WasteCollection.definitions.sensorPicks.pgTableName,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(WasteCollection.definitions.sensorPicks.name + "Validator", SensorPickModel.jsonSchema)
        );
    }

    public bulkUpdate = async (data: ISensorPick[], t: Transaction) => {
        await this.validate(data);
        await this.sequelizeModel.bulkCreate<SensorPickModel>(data, {
            updateOnDuplicate: SensorPickModel.updateAttributes,
            transaction: t,
        });
    };

    public deleteOldData = async (range: IMessageInterval, olderThan: string, vendor: string, t: Transaction) => {
        const deleted = await this.sequelizeModel.destroy({
            where: {
                vendor_id: { [Op.like]: `${vendor}%` },
                pick_at: { [Op.and]: [{ [Op.gte]: range.from }, { [Op.lte]: range.to }] },
                updated_at: { [Op.lt]: olderThan },
            },
            transaction: t,
        });

        log.warn(
            `${
                this.constructor.name
            }: ${deleted} record(s) deleted (${vendor}: ${range.from.toISOString()} - ${range.to.toISOString()}).`
        );
    };
}
