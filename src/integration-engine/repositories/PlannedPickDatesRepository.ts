import { WasteCollection } from "#sch";
import IPlannedPickDates from "#sch/definitions/models/interfaces/IPlannedPickDates";
import PlannedPickDatesModel from "#sch/definitions/models/PlannedPickDatesModel";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import { Op } from "@golemio/core/dist/shared/sequelize";

export class PlannedPickDatesRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            WasteCollection.definitions.plannedPickDates.name + "Repository",
            {
                outputSequelizeAttributes: PlannedPickDatesModel.attributeModel,
                pgTableName: WasteCollection.definitions.plannedPickDates.pgTableName,
                pgSchema: WasteCollection.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                WasteCollection.definitions.plannedPickDates.name + "Validator",
                PlannedPickDatesModel.jsonSchema
            )
        );
    }

    public cleanFuturePickDates = async () => {
        await this.sequelizeModel.destroy({
            where: {
                pick_date: {
                    [Op.gt]: DateTime.now().toISO(),
                },
            },
        });
    };

    public saveBulk = async (data: IPlannedPickDates[]) => {
        if (await this.validate(data)) {
            await this.sequelizeModel.bulkCreate<PlannedPickDatesModel>(data, { ignoreDuplicates: true });
        }
    };
}
