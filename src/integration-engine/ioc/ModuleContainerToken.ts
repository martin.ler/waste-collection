const ModuleContainerToken = {
    RfidContainerVendorDatasource: Symbol(),
    RfidPicksDatasource: Symbol(),
    RfidContainerVendorDataRepository: Symbol(),
    RfidPicksRepository: Symbol(),
    RfidSensorHistoryRepository: Symbol(),
    RfidDownloadContainerVendorDataTask: Symbol(),
    RfidDownloadPicksTask: Symbol(),
};

export { ModuleContainerToken };
