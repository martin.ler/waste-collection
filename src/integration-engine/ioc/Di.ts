import RfidContainerVendorDatasource from "#ie/datasources/rfid/RfidContainerVendorDatasource";
import RfidPicksDatasource from "#ie/datasources/rfid/RfidPicksDatasource";
import { RfidContainerVendorDataRepository } from "#ie/repositories/rfid/RfidContainerVendorDataRepository";
import { RfidPicksRepository } from "#ie/repositories/rfid/RfidPicksRepository";
import { RfidSensorHistoryRepository } from "#ie/repositories/rfid/RfidSensorHistoryRepository";
import RfidDownloadContainerVendorDataTask from "#ie/workers/tasks/RfidDownloadContainerVendorDataTask";
import RfidDownloadPicksTask from "#ie/workers/tasks/RfidDownloadPicksTask";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { delay, DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { ModuleContainerToken } from "./ModuleContainerToken";

//#region Initialization
const WasteCollectionContainer: DependencyContainer = IntegrationEngineContainer.createChildContainer();
//#endregion

//#region Datasource
WasteCollectionContainer.registerSingleton(
    ModuleContainerToken.RfidContainerVendorDatasource,
    RfidContainerVendorDatasource
).registerSingleton(ModuleContainerToken.RfidPicksDatasource, RfidPicksDatasource);
//#endregion

//#region Repositories
WasteCollectionContainer.registerSingleton<RfidContainerVendorDataRepository>(
    ModuleContainerToken.RfidContainerVendorDataRepository,
    RfidContainerVendorDataRepository
)
    .registerSingleton<RfidPicksRepository>(ModuleContainerToken.RfidPicksRepository, RfidPicksRepository)
    .registerSingleton<RfidSensorHistoryRepository>(
        ModuleContainerToken.RfidSensorHistoryRepository,
        RfidSensorHistoryRepository
    );
//#endregion

//#region Tasks
WasteCollectionContainer.registerSingleton<RfidDownloadContainerVendorDataTask>(
    ModuleContainerToken.RfidDownloadContainerVendorDataTask,
    RfidDownloadContainerVendorDataTask
).registerSingleton<RfidDownloadPicksTask>(ModuleContainerToken.RfidDownloadPicksTask, RfidDownloadPicksTask);
//#endregion

export { WasteCollectionContainer };
