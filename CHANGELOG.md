# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

### Changed

-  Update view: v_waste_collection_containers,v_waste_collection_containers_technical_info (https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/272)

## [1.1.3] - 2023-02-27

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.2] - 2023-02-22

### Changed

-   Update city-districts

## [1.1.1] - 2023-02-20

### Fixed

-   VIEW analytic.v_waste_collection_containers_technical_info

## [1.1.0] - 2023-02-13

### Added

-   Rfid integration ([waste-collection#](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/issues/4))

## [1.0.1] - 2023-02-08

### Added

-   Extend legacy api with pick_days & next_picks attributes ([p0149#262](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/262))

## [1.0.0] - 2023-02-07

### Fixed

-   legacy output api returns district as slug ([p0149#268](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/268))

## [0.1.4] - 2023-02-06

### Added

-   Add analytic view ([p0149#266](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/266))

## [0.1.3] - 2023-02-01

### Fixed

-   Sensor picks strategy ([waste-collection#3](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/issues/3))
-   Docs fixes

## [0.1.2] - 2023-01-30

### Fixed

-   Change analytic views ([p0149#264](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/264))

## [0.1.0] - 2023-01-23

### Fixed

-   Delete outdated pick data ([waste-collection#3](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/issues/3))

### Changed

-   Migrate to npm

## [0.0.8] - 2023-01-16

### Fixed

-   Fix stations API sensor output
-   Fix analytic views ([p0149#255](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/255))

## [0.0.7] - 2023-01-04

### Added

-   New sensor data source (Sensority) ([p0149#254](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/254))
-   Update analytic views for all waste-collection dashboards:
    -   ([p0149#253](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/253))
    -   ([p0149#255](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/255))
-   Add analytic view that recommends changing the frequency of waste collection ([p0149#250]](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/250))

### Fixed

-   Performance issues fixes
-   Ksnko has_sensor replaced with active sensor mapping

## [0.0.6] - 2022-11-29

### Added

-   Add analytic view ([p0149#249](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/249))

## [0.0.5] - 2022-11-14

### Added

Add analytic views.

## [0.0.4] - 2022-10-20

### Changed

-   Typescript upgrade to version 4.7.2
-   Pick dates are generated upto 6 months in future

### Fixed

-   Condition added to compute pick date only for future incl today.
-   trash type mapping table initialized before transformation

## [0.0.3] - 2022-09-21

### Added

-   Historic sensor data processing [p0149#232](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/232)

### Fixed

-   Generation of Planned pick dates for raw pick data without isoweeks

### Changed

-   Trash type lookup table added to database

### Removed

-   Unused dependencies ([general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

## [0.0.2] - 2022-09-06

### Changed

-   Optimalization of import sensor history([waste-collection#1](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/issues/1))

## [0.0.1] - 2022-08-24

### Added

-   PSAS integration ([p0149-chytry-svoz-odpadu##226](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/226))
-   Sensoneo integration ([p0149-chytry-svoz-odpadu#225](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/225))
-   KSNKO integration ([p0149-chytry-svoz-odpadu#224](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/224))
