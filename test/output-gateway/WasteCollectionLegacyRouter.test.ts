import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";
import { CustomError, HTTPErrorHandler, ICustomErrorObject } from "@golemio/core/dist/shared/golemio-errors";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { wasteCollectionLegacyRouter } from "#og/routers/WasteCollectionLegacyRouter";
import { legacyPicksResponse } from "../data/legacy-picks-response";
import { legacyPickDaysResponse } from "../data/legacy-pickdays-response";
import { legacyMeasurementsResponse } from "../data/legacy-measurements-response";
import { legacyStationsResponse } from "../data/legacy-stations-response";

chai.use(chaiAsPromised);

describe("WasteCollection Legacy Router", () => {
    const app = express();

    before(async () => {
        app.use("/wastecollectionlegacy", wasteCollectionLegacyRouter);
        app.use((err: Error | CustomError, req: Request, res: Response, next: NextFunction) => {
            const errObject: ICustomErrorObject = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond with json to GET /wastecollectionlegacy", (done) => {
        request(app)
            .get("/wastecollectionlegacy")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body.features).to.have.length(5);
                expect(response.body.features[4]).to.deep.eq(legacyStationsResponse.features[4]);
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond with json to GET /wastecollectionlegacy/measurements", (done) => {
        request(app)
            .get("/wastecollectionlegacy/measurements?containerId=9552&from=2022-11-29T16:57:42.000Z&to=2022-11-29T19:46:20.000Z")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .expect(200)
            .then((response) => {
                expect(response.body).to.deep.eq(legacyMeasurementsResponse.slice(1, 4));
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond with json to GET /wastecollectionlegacy/picks", (done) => {
        request(app)
            .get("/wastecollectionlegacy/picks?containerId=9552&from=2022-11-25T05:22:14.000Z&to=2022-11-28T07:25:57.000Z")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).to.deep.eq(legacyPicksResponse.slice(0, 2));
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond with json to GET /wastecollectionlegacy/pickdays", (done) => {
        request(app)
            .get("/wastecollectionlegacy/pickdays?ksnkoId=9553&sensoneoCode=9553")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).to.deep.eq([legacyPickDaysResponse[0]]);
                done();
            })
            .catch((err) => done(err));
    });
});
