export const legacyStationsResponse = {
    features: [
        {
            type: "Feature",
            geometry: {
                coordinates: [9.313658638379719, 52.379484438973755],
                type: "Point",
            },
            properties: {
                accessibility: {
                    description: "volně",
                    id: 1,
                },
                containers: [],
                district: "praha-22",
                id: 1,
                is_monitored: false,
                name: "Přátelství 356/61",
                station_number: "0022/ 001",
                updated_at: "2022-07-19T15:03:56.029Z",
            },
        },
        {
            type: "Feature",
            geometry: {
                coordinates: [14.593803015701983, 50.03649152836336],
                type: "Point",
            },
            properties: {
                accessibility: {
                    description: "volně",
                    id: 1,
                },
                containers: [
                    {
                        cleaning_frequency: {
                            duration: "P1W",
                            frequency: 1,
                            id: 61,
                        },
                        container_type: "3350 Atomium Reflex SV",
                        trash_type: {
                            description: "Čiré sklo",
                            id: 7,
                        },
                        ksnko_id: 101,
                        container_id: 101,
                    },
                    {
                        cleaning_frequency: {
                            duration: "P1W",
                            frequency: 1,
                            id: 11,
                        },
                        container_type: "240 normální HV",
                        trash_type: {
                            description: "Nápojové kartóny",
                            id: 4,
                        },
                        ksnko_id: 102,
                        container_id: 102,
                    },
                    {
                        cleaning_frequency: {
                            duration: "P3W",
                            frequency: 3,
                            id: 13,
                        },
                        container_type: "1100 normální HV",
                        trash_type: {
                            description: "Papír",
                            id: 5,
                        },
                        ksnko_id: 103,
                        container_id: 103,
                    },
                    {
                        cleaning_frequency: {
                            duration: "P1W",
                            frequency: 1,
                            id: 61,
                        },
                        container_type: "3350 Atomium Reflex SV",
                        trash_type: {
                            description: "Barevné sklo",
                            id: 1,
                        },
                        ksnko_id: 104,
                        container_id: 104,
                    },
                    {
                        cleaning_frequency: {
                            duration: "P3W",
                            frequency: 3,
                            id: 13,
                        },
                        container_type: "1100 normální HV",
                        trash_type: {
                            description: "Plast",
                            id: 6,
                        },
                        ksnko_id: 105,
                        container_id: 105,
                    },
                ],
                district: "praha-22",
                id: 19,
                is_monitored: false,
                name: "V Bytovkách 783",
                station_number: "0022/ 019",
                updated_at: "2022-07-19T15:03:56.029Z",
            },
        },
        {
            type: "Feature",
            geometry: {
                coordinates: [9.321902057986193, 52.37296489565013],
                type: "Point",
            },
            properties: {
                accessibility: {
                    description: "volně",
                    id: 1,
                },
                containers: [
                    {
                        cleaning_frequency: {
                            duration: "P1W",
                            frequency: 1,
                            id: 61,
                        },
                        container_type: "3350 Atomium Reflex SV",
                        trash_type: {
                            description: "Čiré sklo",
                            id: 7,
                        },
                        ksnko_id: 132,
                        container_id: 132,
                    },
                    {
                        cleaning_frequency: {
                            duration: "P1W",
                            frequency: 1,
                            id: 11,
                        },
                        container_type: "240 normální HV",
                        trash_type: {
                            description: "Nápojové kartóny",
                            id: 4,
                        },
                        ksnko_id: 133,
                        container_id: 133,
                    },
                    {
                        cleaning_frequency: {
                            duration: "P3W",
                            frequency: 3,
                            id: 13,
                        },
                        container_type: "1100 normální HV",
                        trash_type: {
                            description: "Papír",
                            id: 5,
                        },
                        ksnko_id: 134,
                        container_id: 134,
                    },
                    {
                        cleaning_frequency: {
                            duration: "P1W",
                            frequency: 1,
                            id: 61,
                        },
                        container_type: "3350 Atomium Reflex SV",
                        trash_type: {
                            description: "Barevné sklo",
                            id: 1,
                        },
                        ksnko_id: 135,
                        container_id: 135,
                    },
                    {
                        cleaning_frequency: {
                            duration: "P3W",
                            frequency: 3,
                            id: 13,
                        },
                        container_type: "1100 normální HV",
                        trash_type: {
                            description: "Plast",
                            id: 6,
                        },
                        ksnko_id: 136,
                        container_id: 136,
                    },
                    {
                        cleaning_frequency: {
                            duration: "P1W",
                            frequency: 1,
                            id: 61,
                        },
                        container_type: "1100 mini H SV",
                        trash_type: {
                            description: "Kovy",
                            id: 3,
                        },
                        ksnko_id: 21128,
                        container_id: 21128,
                    },
                    {
                        cleaning_frequency: {
                            duration: "P1W",
                            frequency: 1,
                            id: 61,
                        },
                        container_type: "3350 Atomium Reflex SV",
                        trash_type: {
                            description: "Čiré sklo",
                            id: 7,
                        },
                        ksnko_id: 30414,
                        container_id: 30414,
                    },
                    {
                        cleaning_frequency: {
                            duration: "P1W",
                            frequency: 1,
                            id: 41,
                        },
                        container_type: "240 normální HV",
                        trash_type: {
                            description: "Jedlé tuky a oleje",
                            id: 8,
                        },
                        ksnko_id: 30415,
                        container_id: 30415,
                    },
                    {
                        cleaning_frequency: {
                            duration: "P1W",
                            frequency: 1,
                            id: 61,
                        },
                        container_type: "3350 Atomium Reflex SV",
                        trash_type: {
                            description: "Čiré sklo",
                            id: 7,
                        },
                        ksnko_id: 30416,
                        container_id: 30416,
                    },
                ],
                district: "praha-22",
                id: 25,
                is_monitored: false,
                name: "Pstružná 821/2",
                station_number: "0022/ 002",
                updated_at: "2022-07-19T15:03:56.029Z",
            },
        },
        {
            type: "Feature",
            geometry: {
                coordinates: [14.435715562630405, 50.100918470008914],
                type: "Point",
            },
            properties: {
                accessibility: {
                    description: "volně",
                    id: 1,
                },
                containers: [
                    {
                        cleaning_frequency: {
                            duration: "P3W",
                            frequency: 3,
                            id: 13,
                        },
                        container_type: "3000 Podzemní SV",
                        trash_type: {
                            description: "Papír",
                            id: 5,
                        },
                        last_measurement: {
                            measured_at_utc: "2022-11-29T21:46:24.000Z",
                            percent_calculated: 100,
                            prediction_utc: null,
                        },
                        last_pick: {
                            pick_at_utc: "2022-11-28T07:25:57.000Z",
                        },
                        ksnko_id: 9552,
                        container_id: 9552,
                        sensor_code: 9552,
                        sensor_supplier: "Sensoneo",
                        sensor_id: "Sensoneo_C00413",
                    },
                    {
                        cleaning_frequency: {
                            duration: "P1W",
                            frequency: 1,
                            id: 11,
                        },
                        container_type: "3000 Podzemní SV",
                        trash_type: {
                            description: "Barevné sklo",
                            id: 1,
                        },
                        last_measurement: {
                            measured_at_utc: "2022-11-29T22:30:21.000Z",
                            percent_calculated: 72,
                            prediction_utc: "2022-11-30T04:04:04.000Z",
                        },
                        last_pick: {
                            pick_at_utc: "2022-11-26T22:19:04.000Z",
                        },
                        ksnko_id: 9553,
                        container_id: 9553,
                        sensor_code: 9553,
                        sensor_supplier: "Sensoneo",
                        sensor_id: "Sensoneo_C00412",
                    },
                    {
                        cleaning_frequency: {
                            duration: "P3W",
                            frequency: 3,
                            id: 13,
                        },
                        container_type: "3000 Podzemní SV",
                        trash_type: {
                            description: "Plast",
                            id: 6,
                        },
                        last_measurement: {
                            measured_at_utc: "2022-11-29T22:33:01.000Z",
                            percent_calculated: 100,
                            prediction_utc: null,
                        },
                        last_pick: {
                            pick_at_utc: "2022-11-28T08:27:05.000Z",
                        },
                        ksnko_id: 9554,
                        container_id: 9554,
                        sensor_code: 9554,
                        sensor_supplier: "Sensoneo",
                        sensor_id: "Sensoneo_C00414",
                    },
                ],
                district: "praha-7",
                id: 2074,
                is_monitored: true,
                name: "Řezáčovo náměstí 1256/3",
                station_number: "0007/ 022",
                updated_at: "2022-07-19T15:03:56.029Z",
            },
        },
        {
            type: "Feature",
            geometry: {
                coordinates: [14.600276664024898, 50.03140865143183],
                type: "Point",
            },
            properties: {
                accessibility: {
                    description: "volně",
                    id: 1,
                },
                containers: [],
                district: "praha-22",
                id: 9999,
                is_monitored: false,
                name: "test",
                station_number: "9999/ 025",
                updated_at: "2022-07-19T15:03:56.029Z",
            },
        },
    ],
    type: "FeatureCollection",
};
