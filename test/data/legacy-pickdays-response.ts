export const legacyPickDaysResponse = [
    {
        ksnko_id: 9553,
        sensoneo_code: "9553",
        generated_dates: ["2022-08-09"],
    },
    {
        ksnko_id: 99999998,
        sensoneo_code: "99999998",
        generated_dates: ["2022-01-01"],
    },
];
