import { TrashTypeMappingRepository } from "#ie/repositories/TrashTypeMappingRepository";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);

describe("TrashTypeMapper", () => {
    let trashTypeMapper: TrashTypeMappingRepository;
    before(() => {
        PostgresConnector.connect();
        trashTypeMapper = TrashTypeMappingRepository.getInstance();
    });
    it("test mapping", async () => {
        const result = await trashTypeMapper.findByPsasCode("P");
        expect(result).to.deep.equal({
            code_ksnko: "p",
            code_psas: "P",
            name: "Papír",
            legacy_id: 5,
        });
        expect(trashTypeMapper.findByPsasCode("p")).to.eventually.equal(undefined);
    });
});
