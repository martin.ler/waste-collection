import PsasParserHelper from "#ie/helpers/PsasParserHelper";
import { expect } from "chai";
import sinon from "sinon";
import { SinonFakeTimers, SinonSandbox } from "sinon";

describe("PsasParserHelper", () => {
    let sandbox: SinonSandbox;
    let clock: SinonFakeTimers;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: new Date(2022, 6, 1, 7),
            shouldAdvanceTime: true,
        });
    });

    afterEach(() => {
        clock.restore();
        sandbox.restore();
    });

    it("parseYearIsoWeeks ~ 2022-Po,4,8,12,16,20,24,28,32,36,40,44,48,52", () => {
        expect(PsasParserHelper.parseYearIsoWeeks(`2022-Po,4,8,12,16,20,24,28,32,36,40,44,48,52`)).to.deep.equal({
            year: 2022,
            days: "Po",
            weekNumbers: "4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52",
        });
    });

    it("parseYearIsoWeeks ~ undefined", () => {
        expect(PsasParserHelper.parseYearIsoWeeks(undefined)).to.deep.equal({
            year: undefined,
            days: undefined,
            weekNumbers: undefined,
        });
    });

    it("parseYearIsoWeeks ~ null", () => {
        expect(PsasParserHelper.parseYearIsoWeeks(null)).to.deep.equal({
            year: undefined,
            days: undefined,
            weekNumbers: undefined,
        });
    });

    it("parseYearIsoWeeks ~ empty string", () => {
        expect(PsasParserHelper.parseYearIsoWeeks("")).to.deep.equal({
            year: undefined,
            days: undefined,
            weekNumbers: undefined,
        });
    });

    it("parseYearIsoWeeks ~ 2022-Po", () => {
        expect(PsasParserHelper.parseYearIsoWeeks(`2022-Po`)).to.deep.equal({
            year: 2022,
            days: "Po",
            weekNumbers: "",
        });
    });

    it("parseYearIsoWeeks ~ 2022-4,8,12,16,20,24,28,32,36,40,44,48,52", () => {
        expect(PsasParserHelper.parseYearIsoWeeks(`2022-4,8,12,16,20,24,28,32,36,40,44,48,52`)).to.deep.equal({
            year: 2022,
            days: "",
            weekNumbers: "4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52",
        });
    });

    it("parseYearIsoWeeks ~ 2022-Po 4,8 12,16 20,24,28 32,36;40,44;48,52", () => {
        expect(PsasParserHelper.parseYearIsoWeeks(`2022-Po 4,8 12,16 20,24,28 32,36;40,44;48,52`)).to.deep.equal({
            year: 2022,
            days: "Po",
            weekNumbers: "4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52",
        });
    });
    it("parseYearIsoWeeks ~ nonsense", () => {
        expect(PsasParserHelper.parseYearIsoWeeks(`nonsense`)).to.deep.equal({
            year: undefined,
            days: undefined,
            weekNumbers: undefined,
        });
    });

    it("generateDateFromIsoWeek should return date based on week number an day", () => {
        expect(PsasParserHelper.generateDateFromIsoWeek("1", "Po", 2022).toISODate()).to.equal("2022-01-03");
        expect(PsasParserHelper.generateDateFromIsoWeek("2", "Út", 2022).toISODate()).to.equal("2022-01-11");
        expect(PsasParserHelper.generateDateFromIsoWeek("3", "St", 2022).toISODate()).to.equal("2022-01-19");
        expect(PsasParserHelper.generateDateFromIsoWeek("4", "Čt", 2022).toISODate()).to.equal("2022-01-27");
        expect(PsasParserHelper.generateDateFromIsoWeek("5", "Pá", 2022).toISODate()).to.equal("2022-02-04");
        expect(PsasParserHelper.generateDateFromIsoWeek("6", "So", 2022).toISODate()).to.equal("2022-02-12");
        expect(PsasParserHelper.generateDateFromIsoWeek("7", "Ne", 2022).toISODate()).to.equal("2022-02-20");
    });

    it("generatePickDates should return multiple results without any isoweeks input", () => {
        const generatedPickDates = Array.from(
            PsasParserHelper.generatePickDates([], ["Po", "Út", "St", "Čt", "Pá", "So", "Ne"], undefined)
        );
        expect(generatedPickDates.length).to.greaterThan(0);
        expect(generatedPickDates[generatedPickDates.length - 1].toISO()).to.equal("2023-01-01T00:00:00.000+01:00");
    });

    it("generatePickDates should return multiple results without any isoweeks input #2", () => {
        const generatedPickDates = Array.from(
            PsasParserHelper.generatePickDates([], ["Po", "Út", "St", "Čt", "Pá", "So", "Ne"], "2022-07-31T00:00:00")
        );
        expect(generatedPickDates.length).to.greaterThan(0);
        expect(generatedPickDates[generatedPickDates.length - 1].toISO()).to.equal("2022-07-31T00:00:00.000+02:00");
    });
});
