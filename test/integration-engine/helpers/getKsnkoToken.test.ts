import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import axios from "@golemio/core/dist/shared/axios";
import { config } from "@golemio/core/dist/integration-engine";
import { getKsnkoToken } from "#ie/helpers/getKsnkoToken";

chai.use(chaiAsPromised);

describe("getKsnkoToken", () => {
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(config, "datasources").value({
            KSNKOApi: {
                url: "<ksnkoApiUrl>",
            },
        });
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should get token from KSNKO API", async () => {
        sandbox.stub(axios, "post").resolves({ data: { token: "test-token" } });
        expect(await getKsnkoToken()).to.equal("test-token");
    });

    it("should throw error if token cannot be obtained", async () => {
        sandbox.stub(axios, "post").rejects("Not found");
        await expect(getKsnkoToken()).to.be.rejectedWith("Can not obtain KSNKO auth token: Not found");
    });
});
