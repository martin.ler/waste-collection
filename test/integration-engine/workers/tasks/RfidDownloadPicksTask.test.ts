import { WasteCollectionContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { RfidPicksRepository } from "#ie/repositories/rfid/RfidPicksRepository";
import RfidDownloadPicksTask from "#ie/workers/tasks/RfidDownloadPicksTask";
import IRfidPick from "#sch/datasources/rfid/IRfidPick";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("RfidDownloadPicksTask", () => {
    let sandbox: SinonSandbox;
    let inputData: IRfidPick[];
    let dataSource: any;
    before(() => {
        sandbox = sinon.createSandbox();
        inputData = JSON.parse(fs.readFileSync(__dirname + "/../../../data/raw-rfid-picks.json").toString("utf8")).picks;
        dataSource = {
            getData: () => Promise.resolve(inputData),
        };

        WasteCollectionContainer.registerInstance(ModuleContainerToken.RfidPicksDatasource, dataSource);
    });

    after(() => {
        WasteCollectionContainer.clearInstances();
    });

    it("Integration test of task", async () => {
        const getDataStub = sinon.stub(dataSource, "getData").resolves([]);
        const saveToDbStub = sinon
            .stub(WasteCollectionContainer.resolve<RfidPicksRepository>(ModuleContainerToken.RfidPicksRepository), "bulkUpdate")
            .callsFake((data) => {
                return Promise.resolve();
            });
        const task = WasteCollectionContainer.resolve<RfidDownloadPicksTask>(ModuleContainerToken.RfidDownloadPicksTask);
        const promise = task.validateAndExecute({ content: Buffer.from(JSON.stringify("{}")) } as any);
        await expect(promise).to.be.fulfilled;
        expect(getDataStub.callCount).equal(1);
        expect(saveToDbStub.callCount).equal(1);
    });
});
