import { WasteCollectionContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { RfidContainerVendorDataRepository } from "#ie/repositories/rfid/RfidContainerVendorDataRepository";
import { RfidSensorHistoryRepository } from "#ie/repositories/rfid/RfidSensorHistoryRepository";
import RfidDownloadContainerVendorDataTask from "#ie/workers/tasks/RfidDownloadContainerVendorDataTask";
import IRfidPick from "#sch/datasources/rfid/IRfidPick";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("RfidDownloadContainerVendorDataTask", () => {
    let sandbox: SinonSandbox;
    let inputData: IRfidPick[];
    let dataSource: any;
    before(() => {
        sandbox = sinon.createSandbox();
        inputData = JSON.parse(
            fs.readFileSync(__dirname + "/../../../data/raw-rfid-containervendordata.json").toString("utf8")
        ).container_vendor_data;
        dataSource = {
            getData: () => Promise.resolve(inputData),
        };

        WasteCollectionContainer.registerInstance(ModuleContainerToken.RfidContainerVendorDatasource, dataSource);
    });

    after(() => {
        WasteCollectionContainer.clearInstances();
    });

    it("Integration test of task", async () => {
        const getDataStub = sinon.stub(dataSource, "getData").resolves(inputData);
        const bulkUpdateStub = sinon
            .stub(
                WasteCollectionContainer.resolve<RfidContainerVendorDataRepository>(
                    ModuleContainerToken.RfidContainerVendorDataRepository
                ),
                "bulkUpdate"
            )
            .callsFake((data) => {
                return Promise.resolve();
            });
        const updateHistoryStub = sinon
            .stub(
                WasteCollectionContainer.resolve<RfidSensorHistoryRepository>(ModuleContainerToken.RfidSensorHistoryRepository),
                "updateHistory"
            )
            .callsFake((data) => {
                return Promise.resolve();
            });
        const task = WasteCollectionContainer.resolve<RfidDownloadContainerVendorDataTask>(
            ModuleContainerToken.RfidDownloadContainerVendorDataTask
        );
        const promise = task.validateAndExecute({ content: Buffer.from(JSON.stringify("{}")) } as any);
        await expect(promise).to.be.fulfilled;
        expect(getDataStub.calledOnce).to.be.true;
        expect(bulkUpdateStub.calledOnce).to.be.true;
        expect(updateHistoryStub.calledOnce).to.be.true;
    });
});
