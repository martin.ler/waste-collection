import { PsasTransformation } from "#ie/transformations/PsasTransformation";
import IRawPickDatesInput from "#sch/datasources/psas/IRawPickDatesInput";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { rawPickDatesTransformed } from "test/data/raw-pickdates-transformation";

chai.use(chaiAsPromised);

describe("PsasStationsTransformation", () => {
    let transformation: PsasTransformation;
    let sourceData: {
        data: IRawPickDatesInput[];
    };

    before(() => {
        transformation = new PsasTransformation();
        sourceData = JSON.parse(fs.readFileSync(__dirname + "/../../data/raw-pickdates-datasource.json").toString("utf8"));
    });

    it("should have name", () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("RawPickDatesDataSourceTransformation");
    });

    it("should properly transform data", async () => {
        const data = await transformation.transform(sourceData.data);
        expect(data[0]).to.deep.equal(rawPickDatesTransformed);
    });
});
