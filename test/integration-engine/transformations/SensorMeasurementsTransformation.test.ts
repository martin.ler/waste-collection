import fs from "fs";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { ISensorMeasurementsInput } from "#sch/datasources/sensor/SensorMeasurementsJsonSchema";
import { SensorMeasurementsTransformation } from "#ie/transformations/SensorMeasurementsTransformation";
import { sensoneoMeasurementsTransformedDataFixture } from "../../data/sensoneo-sensor-measurements-transformation";

chai.use(chaiAsPromised);

describe("SensorMeasurementsTransformation", () => {
    let transformation: SensorMeasurementsTransformation;
    let sourceData: { measurements: ISensorMeasurementsInput[] };

    before(() => {
        transformation = new SensorMeasurementsTransformation("Sensoneo");
        sourceData = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/sensoneo-sensor-measurements-datasource.json").toString("utf8")
        );
    });

    it("should have name", () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("SensoneoSensorMeasurementsDataSourceTransformation");
    });

    it("should properly transform data", async () => {
        const data = await transformation.transform(sourceData.measurements);
        expect(data).to.deep.equal(sensoneoMeasurementsTransformedDataFixture);
    });
});
