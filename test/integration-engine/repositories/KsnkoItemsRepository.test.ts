import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { KsnkoItemsRepository } from "#ie/repositories/KsnkoItemsRepository";

chai.use(chaiAsPromised);

describe("KsnkoItemsRepository", () => {
    let model: KsnkoItemsRepository;

    before(() => {
        PostgresConnector.connect();
        model = new KsnkoItemsRepository();
    });

    it("should have name", async () => {
        expect(model.name).not.to.be.undefined;
        expect(model.name).is.equal("WasteCollectionKsnkoItemsRepository");
    });

    it("should have updateActive method", async () => {
        expect(model.updateActive).not.to.be.undefined;
    });
});
