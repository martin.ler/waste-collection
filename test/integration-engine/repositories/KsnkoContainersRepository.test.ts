import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { KsnkoContainersRepository } from "#ie/repositories/KsnkoContainersRepository";

chai.use(chaiAsPromised);

describe("KsnkoContainersRepository", () => {
    let model: KsnkoContainersRepository;

    before(() => {
        PostgresConnector.connect();
        model = new KsnkoContainersRepository();
    });

    it("should have name", async () => {
        expect(model.name).not.to.be.undefined;
        expect(model.name).is.equal("WasteCollectionKsnkoContainersRepository");
    });

    it("should have updateActive method", async () => {
        expect(model.updateActive).not.to.be.undefined;
    });

    it("should get container id by trash type and station number", async () => {
        const resultId = await model.getIdByTypeAndStation("sb", "0007/ 022");
        expect(resultId?.length).to.equal(1);
        expect(resultId![0]).to.equal(9553);
    });
});
