import { RawPickDatesRepository } from "#ie/repositories/RawPickDatesRepository";
import IRawPickDates from "#sch/definitions/models/interfaces/IRawPickDates";
import RawPickDates from "#sch/definitions/models/RawPickDatesModel";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);

describe("RawPickDatesRepository", () => {
    let repo: RawPickDatesRepository;
    const mockupData = [
        {
            date: "2022-08-09T12:42:17.291Z",
            subject_id: 999998,
            count: 0.0,
            container_type: "abc",
            container_volume: 0,
            frequency: "1",
            street_name: "Dělnická 2",
            trash_type: "p",
            trash_type_code: "P",
            trash_type_name: "Papír",
            orientation_number: 123,
            address_char: "abcd",
            conscription_number: 123.0,
            valid_from: "2022-08-09T12:42:17.291Z",
            valid_to: "2022-08-09T12:42:17.291Z",
            station_number: "456789",
            year_days_isoweeks: "2022-Po,4,8,12,16,20,24,28,32,36,40,44,48,52",
            year: 2022,
            days: "Po",
            isoweeks: "4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52",
            company: "Pražské služby",
        } as IRawPickDates,
    ];
    before(() => {
        PostgresConnector.connect();
        repo = new RawPickDatesRepository();
    });

    it("should have name", async () => {
        expect(repo.name).not.to.be.undefined;
        expect(repo.name).is.equal("WasteCollectionRawPickDatesRepository");
    });

    it("should load example item", async () => {
        const result = await repo["sequelizeModel"].findOne<RawPickDates>({
            where: { subject_id: 9553 },
        });
        expect(result?.street_name).to.equal("Dělnická");
    });

    it("should validate item", async () => {
        expect(await repo.validate(mockupData)).to.equal(true);
    });

    it("should save item", async () => {
        await repo.saveBulk(mockupData);
        const result = await repo["sequelizeModel"].findOne<RawPickDates>({
            where: { subject_id: 999998 },
        });
        expect(result?.street_name).to.equal("Dělnická 2");
    });
});
