import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { KsnkoPostgresRepository } from "#ie/repositories/KsnkoPostgresRepository";
import { KsnkoStationsRepository } from "#ie/repositories/KsnkoStationsRepository";
import { IKsnkoStation } from "#sch/definitions/KsnkoStations";

chai.use(chaiAsPromised);

describe("KsnkoStationsRepository", () => {
    let model: KsnkoPostgresRepository;

    before(() => {
        PostgresConnector.connect();
        model = new KsnkoStationsRepository();
    });

    it("should have name", async () => {
        expect(model.name).not.to.be.undefined;
        expect(model.name).is.equal("WasteCollectionKsnkoStationsRepository");
    });

    it("should have updateActive method", async () => {
        expect(model.updateActive).not.to.be.undefined;
    });

    it("should update records and mark old ones inactive", async () => {
        const stations = [
            {
                id: 1,
                number: "0022/ 001",
                name: "Přátelství 356/61",
                access: "volně",
                location: "outdoor",
                has_sensor: false,
                changed: "2021-09-30T06:22:27.000Z",
                city_district_name: "Praha 22",
                coordinate_lat: -731175.14,
                coordinate_lon: -1050805.71,
                geom: {
                    type: "Point",
                    coordinates: [9.313658638379719, 52.379484438973755],
                },
                active: true,
            },
            {
                id: 25,
                number: "0022/ 002",
                name: "Pstružná 821/2",
                access: "volně",
                location: "outdoor",
                has_sensor: false,
                changed: "2021-08-03T16:23:55.000Z",
                city_district_name: "Praha 22",
                coordinate_lat: -731999.974327,
                coordinate_lon: -1050404.54492,
                geom: {
                    type: "Point",
                    coordinates: [9.321902057986193, 52.37296489565013],
                },
                active: true,
            },
        ];

        await model.updateActive(stations);
        const records = await model.find({ raw: true });

        expect(records.length).to.equal(5);
        const activeRecordIds = records.filter((el: IKsnkoStation) => el.active).map((el: IKsnkoStation) => el.id);
        expect(activeRecordIds.length).to.equal(2);
        expect(activeRecordIds).to.have.members([1, 25]);
    });
});
