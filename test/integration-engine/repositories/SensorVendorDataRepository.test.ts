import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { SensorVendorDataRepository } from "#ie/repositories/SensorVendorDataRepository";

chai.use(chaiAsPromised);

describe("SensorVendorDataRepository", () => {
    let model: SensorVendorDataRepository;

    before(() => {
        PostgresConnector.connect();
        model = new SensorVendorDataRepository();
    });

    it("should have name", async () => {
        expect(model.name).not.to.be.undefined;
        expect(model.name).is.equal("WasteCollectionSensorVendorDataRepository");
    });
});
