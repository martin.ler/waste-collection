import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { SensorMeasurementsRepository } from "#ie/repositories/SensorMeasurementsRepository";

chai.use(chaiAsPromised);

describe("SensorMeasurementsRepository", () => {
    let model: SensorMeasurementsRepository;

    before(() => {
        PostgresConnector.connect();
        model = new SensorMeasurementsRepository();
    });

    it("should have name", async () => {
        expect(model.name).not.to.be.undefined;
        expect(model.name).is.equal("WasteCollectionSensorMeasurementsRepository");
    });
});
