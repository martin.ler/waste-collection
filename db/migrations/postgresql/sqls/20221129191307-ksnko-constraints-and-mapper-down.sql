ALTER TABLE ksnko_items DROP CONSTRAINT ksnko_items_station_number_fkey;
ALTER TABLE ksnko_items ADD CONSTRAINT ksnko_items_station_number_fkey FOREIGN KEY ("station_number")
    REFERENCES ksnko_stations (number) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

ALTER TABLE ksnko_containers DROP CONSTRAINT ksnko_containers_station_number_fkey;
ALTER TABLE ksnko_containers ADD CONSTRAINT ksnko_containers_station_number_fkey FOREIGN KEY ("station_number")
    REFERENCES ksnko_stations (number) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

ALTER TABLE trash_type_mapping DROP CONSTRAINT trash_type_mapping_legacy_id_key;
ALTER TABLE trash_type_mapping DROP COLUMN legacy_id;

TRUNCATE TABLE trash_type_mapping;

INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name") VALUES('p', 'P', 'Papír');
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name") VALUES('u', 'U', 'Plast');
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name") VALUES('sb', 'S', 'Barevné Sklo');
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name") VALUES('sc', 'C', 'Čiré sklo');
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name") VALUES('nk', 'K', 'Nápojové kartóny');
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name") VALUES('ko', 'E', 'Kovy');

