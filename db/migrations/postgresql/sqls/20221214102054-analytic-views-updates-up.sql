-- Zmena PK
alter table waste_collection.measurements DROP CONSTRAINT measurements_pkey;
ALTER TABLE waste_collection.measurements ADD CONSTRAINT measurements_pkey PRIMARY KEY (measured_at,vendor_id);

-- Technické info o senzorech
drop view analytic.v_waste_collection_containers_technical_info;

CREATE OR REPLACE VIEW analytic.v_waste_collection_containers_technical_info
AS WITH main AS (
         SELECT DISTINCT m.ksnko_container_id,
            m.vendor_id,
            k.station_number,
            ks.city_district_name,
            ks.name,
            rpd.trash_type_name,
            cvd.network,
            cvd.anti_noise_depth,
            cvd.bin_depth,
            cvd.vendor,
            cvd.sensor_version,
            m.measured_at,
            m.measured_at::date AS measured_at_day,
            m.percent_smoothed::double precision / 100::double precision AS percent_smoothed,
            m.battery_status,
            p.pick_date::date AS pick_day
           FROM waste_collection.measurements m
             LEFT JOIN waste_collection.planned_pick_dates p ON m.ksnko_container_id = p.ksnko_container_id AND m.measured_at::date = p.pick_date::date
             JOIN waste_collection.container_vendor_data cvd ON m.vendor_id::text = cvd.vendor_id::text
             JOIN waste_collection.ksnko_containers k ON cvd.ksnko_container_id = k.id
             LEFT JOIN waste_collection.ksnko_stations ks ON k.station_number::text = ks.number::text
             JOIN waste_collection.raw_pick_dates rpd ON k.station_number::text = rpd.station_number::text AND k.trash_type::text = rpd.trash_type_code::text
          WHERE cvd.active IS TRUE AND m.measured_at >= (now() - '3 mons'::interval)
          ORDER BY m.ksnko_container_id, m.vendor_id, k.station_number, m.measured_at
        ), min_percent AS (
         SELECT DISTINCT foo.ksnko_container_id,
            foo.vendor_id,
            foo.station_number,
            last_value(foo.measured_at) OVER (PARTITION BY foo.ksnko_container_id, foo.vendor_id, foo.station_number) AS last_measured_at,
            foo.min_percent_smoothed
           FROM ( SELECT main.ksnko_container_id,
                    main.vendor_id,
                    main.station_number,
                    main.measured_at,
                    main.percent_smoothed,
                    min(main.percent_smoothed) OVER (PARTITION BY main.ksnko_container_id, main.vendor_id, main.station_number) AS min_percent_smoothed
                   FROM main
                  ORDER BY main.ksnko_container_id, main.vendor_id, main.station_number, main.measured_at) foo
          WHERE foo.min_percent_smoothed = foo.percent_smoothed
        ), last_battery_status AS (
         SELECT DISTINCT main.ksnko_container_id,
            main.vendor_id,
            main.station_number,
            main.name,
            main.city_district_name,
            main.trash_type_name,
            main.sensor_version,
            main.network,
            1::double precision - main.anti_noise_depth::double precision / main.bin_depth::double precision AS anti_noise_depth,
            min(main.battery_status) OVER (PARTITION BY main.ksnko_container_id, main.vendor_id, main.station_number) AS last_battery_status
           FROM main
          WHERE main.measured_at >= (now() - '14 days'::interval day)
        ), msg AS (
         SELECT daily_msg.ksnko_container_id,
            daily_msg.vendor_id,
            daily_msg.station_number,
            sum(daily_msg.daily_msg::double precision / daily_msg.daily_msg_expected::double precision) / daily_msg.count_days::double precision AS msg_transfer_in_percent
           FROM ( SELECT main.ksnko_container_id,
                    main.vendor_id,
                    main.station_number,
                    main.measured_at_day,
                        CASE
                            WHEN main.sensor_version::text = 'v2'::text AND main.vendor::text = 'Sensoneo'::text THEN 4
                            ELSE 7
                        END AS daily_msg_expected,
                    30 AS count_days,
                        CASE
                            WHEN main.sensor_version::text = 'v2'::text AND main.vendor::text = 'Sensoneo'::text THEN
                            CASE
                                WHEN count(main.measured_at) > 4 THEN 4::bigint
                                ELSE count(main.measured_at)
                            END
                            ELSE
                            CASE
                                WHEN count(main.measured_at) > 7 THEN 7::bigint
                                ELSE count(main.measured_at)
                            END
                        END AS daily_msg
                   FROM main
                  WHERE main.measured_at >= (CURRENT_DATE::timestamp with time zone - '30 days'::interval) AND main.measured_at < CURRENT_DATE::timestamp with time zone
                  GROUP BY main.ksnko_container_id, main.vendor_id, main.station_number, main.vendor, main.sensor_version, main.measured_at_day, (
                        CASE
                            WHEN main.sensor_version::text = 'v2'::text AND main.vendor::text = 'Sensoneo'::text THEN 4
                            ELSE 7
                        END)) daily_msg
          GROUP BY daily_msg.ksnko_container_id, daily_msg.vendor_id, daily_msg.station_number, daily_msg.count_days
        )
 SELECT lbs.ksnko_container_id,
    lbs.vendor_id,
    lbs.station_number,
    lbs.name,
    lbs.city_district_name,
    lbs.trash_type_name,
    lbs.sensor_version,
    lbs.network,
    lbs.anti_noise_depth,
    mp.min_percent_smoothed,
    mp.last_measured_at,
    lbs.last_battery_status,
    msg.msg_transfer_in_percent
   FROM last_battery_status lbs
     LEFT JOIN min_percent mp ON mp.ksnko_container_id = lbs.ksnko_container_id
     LEFT JOIN msg ON lbs.ksnko_container_id = msg.ksnko_container_id;

-- Kontejnery s chybejicim merenim
drop view analytic.v_waste_collection_24_hod_missing;

CREATE OR REPLACE VIEW analytic.v_waste_collection_24_hr_missing
AS SELECT ks.id AS ksnko_container_id,
    max(m.measured_at) AS measured_at
   FROM waste_collection.measurements m
     JOIN waste_collection.container_vendor_data cc ON cc.vendor_id::text = m.vendor_id::text
     JOIN waste_collection.ksnko_containers ks ON m.ksnko_container_id = ks.id
  GROUP BY ks.id
 HAVING max(m.measured_at) < (now() - '1 day'::interval) OR max(m.measured_at) IS NULL
  ORDER BY (max(m.measured_at));

-- detekovane svozy s prirazenim k realnemu svozovemu planu (via python job)
drop view analytic.v_waste_collection_assigned_picks;

CREATE OR REPLACE VIEW analytic.v_waste_collection_assigned_picks
AS SELECT cp.ksnko_container_id AS container_code,
    cc.container_volume AS total_volume,
        CASE
            WHEN cc.trash_type::text = 'sb'::text THEN 411
            WHEN cc.trash_type::text = 'ko'::text THEN 120
            WHEN cc.trash_type::text = 'nk'::text THEN 55
            WHEN cc.trash_type::text = 'p'::text THEN 76
            WHEN cc.trash_type::text = 'u'::text THEN 39
            WHEN cc.trash_type::text = 'sc'::text THEN 411
            ELSE NULL::integer
        END AS koeficient,
    cp.pick_at,
    cp.pick_at::date AS pick_at_date,
    date_part('hour'::text, cp.pick_at) AS hour,
    cp.percent_before,
    cp.percent_now,
    cp.event_driven,
    wpd.category,
        CASE
            WHEN wpd.category::text = 'exact'::text THEN 'Přesný'::text
            WHEN wpd.category::text = 'tolerated_before'::text THEN 'Předjetý'::text
            WHEN wpd.category::text = 'tolerated_after'::text THEN 'Dodatečný'::text
            ELSE 'Nepřiřazený'::text
        END AS category_cz
   FROM waste_collection.picks cp
     JOIN waste_collection.ksnko_containers cc ON cp.ksnko_container_id = cc.id
     JOIN waste_collection.container_vendor_data cvd ON cc.id = cvd.ksnko_container_id
     LEFT JOIN python.waste_collection_assigned_pick_dates wpd ON cp.ksnko_container_id = wpd.container_code::integer AND cp.pick_at = wpd.pick_at
  WHERE cvd.active IS TRUE;

-- kontejnery
drop view analytic.v_waste_collection_containers;

CREATE OR REPLACE VIEW analytic.v_waste_collection_containers
AS SELECT v.vendor_id,
    v.id,
    v.code,
    v.volume_ratio,
    v.trash_type,
    v.container_volume,
    v.container_brand,
    v.container_dump,
    v.cleaning_frequency_code,
    concat(v.cleaning_frequency_code, ' – ', v.days) AS cleaning_frequency_displayed,
    v.station_id,
    v.station_number,
    v.active,
    v.create_batch_id,
    v.created_at,
    v.created_by,
    v.update_batch_id,
    v.updated_at,
    v.updated_by,
    v.city_district_name,
    split_part(v.city_district_name::text, ' '::text, 2) AS city_district_number,
    v.coordinate_lat,
    v.coordinate_lon,
    v.trash_type_name,
    v.days,
    v.street_name,
    v.frequency,
    v.name,
    v.network,
    v.bin_brand,
    v.frequency_text,
    v.row_number
   FROM ( SELECT cvd.vendor_id,
            kc.id,
            kc.code,
            kc.volume_ratio,
            kc.trash_type,
            kc.container_volume,
            kc.container_brand,
            kc.container_dump,
            kc.cleaning_frequency_code,
            kc.station_id,
            kc.station_number,
            cvd.active,
            kc.create_batch_id,
            kc.created_at,
            kc.created_by,
            kc.update_batch_id,
            kc.updated_at,
            kc.updated_by,
            ks.city_district_name,
            st_x(ks.geom) AS coordinate_lon,
            st_y(ks.geom) AS coordinate_lat,
            ttm.name AS trash_type_name,
            rpd.days,
            rpd.street_name,
            rpd.frequency,
            ks.name,
            cvd.network,
            cvd.bin_brand,
                CASE
                    WHEN rpd.frequency::text = '017'::text THEN '7x týdně'::text
                    WHEN rpd.frequency::text = '016'::text THEN '6x týdně'::text
                    WHEN rpd.frequency::text = '015'::text THEN '5x týdně'::text
                    WHEN rpd.frequency::text = '014'::text THEN '4x týdně'::text
                    WHEN rpd.frequency::text = '013'::text THEN '3x týdně'::text
                    WHEN rpd.frequency::text = '012'::text THEN '2x týdně'::text
                    WHEN rpd.frequency::text = '011'::text THEN '1x týdně'::text
                    WHEN rpd.frequency::text = '021'::text THEN '1x za 2 týdny'::text
                    WHEN rpd.frequency::text = '031'::text THEN '1x za 3 týdny'::text
                    WHEN rpd.frequency::text = '041'::text THEN '1x za 4 týdny'::text
                    WHEN rpd.frequency::text = '051'::text THEN '1x za 5 týdnů'::text
                    WHEN rpd.frequency::text = '061'::text THEN '1x za 6 týdnů'::text
                    ELSE NULL::text
                END AS frequency_text,
            row_number() OVER (PARTITION BY kc.id) AS row_number
           FROM waste_collection.container_vendor_data cvd
             JOIN waste_collection.ksnko_containers kc ON cvd.ksnko_container_id = kc.id
             LEFT JOIN waste_collection.trash_type_mapping ttm ON kc.trash_type::text = ttm.code_ksnko::text
             LEFT JOIN waste_collection.ksnko_stations ks ON kc.station_number::text = ks.number::text
             JOIN waste_collection.raw_pick_dates rpd ON kc.station_number::text = rpd.station_number::text AND kc.trash_type::text = rpd.trash_type_code::text
          WHERE cvd.active IS TRUE) v
  WHERE v.row_number = 1;

-- měření
drop view analytic.v_waste_collection_measurements;

CREATE OR REPLACE VIEW analytic.v_waste_collection_measurements
AS SELECT m.ksnko_container_id,
    k.station_number,
    m.percent_smoothed,
    m.percent_raw,
    m.measured_at,
    m.measured_at::date AS measured_at_date,
    split_part(m.measured_at::character varying(50)::text, ' '::text, 2) AS measured_at_time,
    COALESCE(m.percent_smoothed::numeric, 0::numeric) / 100::numeric AS percent_smoothed_calculated_decimal
   FROM waste_collection.measurements m
     JOIN waste_collection.container_vendor_data cvd ON m.vendor_id::text = cvd.vendor_id::text
     JOIN waste_collection.ksnko_containers k ON cvd.ksnko_container_id = k.id;

-- svozy plán ps
drop view analytic.v_waste_collection_pick_dates_ps;

CREATE OR REPLACE VIEW analytic.v_waste_collection_pick_dates_ps
AS SELECT vv.ksnko_container_id,
    vv.measured_at,
    vv.measured_at::date AS measured_at_date,
    vv.percent_smoothed,
        CASE
            WHEN vv.row_number = 2 AND vv.pick_date IS NOT NULL THEN 'Svoz PS'::text
            ELSE NULL::text
        END AS picks,
        CASE
            WHEN vv.row_number = 2 AND vv.pick_date IS NOT NULL THEN vv.pick_date
            ELSE NULL::timestamp with time zone
        END AS pick_date_svoz
   FROM ( SELECT v.ksnko_container_id,
            v.measured_at,
            v.percent_smoothed,
            v.pick_date,
            row_number() OVER (PARTITION BY v.ksnko_container_id, (v.pick_date::date) ORDER BY v.measured_at) AS row_number
           FROM ( SELECT COALESCE(m.ksnko_container_id, p.ksnko_container_id) AS ksnko_container_id,
                    COALESCE(m.measured_at, p.pick_date) AS measured_at,
                    COALESCE(m.percent_smoothed, 0) AS percent_smoothed,
                    p.pick_date
                   FROM waste_collection.measurements m
                     FULL JOIN waste_collection.planned_pick_dates p ON m.ksnko_container_id = p.ksnko_container_id AND m.measured_at::date = p.pick_date::date
                  WHERE m.measured_at <= (( SELECT max(measurements.measured_at) AS max
                           FROM waste_collection.measurements)) AND m.measured_at <= now()) v) vv
  ORDER BY vv.measured_at DESC;

-- detekované svozy
drop view analytic.v_waste_collection_pick_dates_sensoneo;

CREATE OR REPLACE VIEW analytic.v_waste_collection_pick_dates_sensoneo
AS SELECT vv.ksnko_container_id,
    vv.measured_at,
    vv.measured_at::date AS measured_at_date,
    vv.percent_smoothed,
    vv.pick_at,
        CASE
            WHEN vv.row_number = 1 AND vv.pick_at IS NOT NULL THEN 'Svoz sensoneo'::text
            ELSE NULL::text
        END AS picks,
        CASE
            WHEN vv.row_number = 1 AND vv.pick_at IS NOT NULL THEN vv.percent_before
            ELSE NULL::integer
        END AS percent_before,
        CASE
            WHEN vv.row_number = 1 AND vv.pick_at IS NOT NULL THEN vv.percent_now
            ELSE NULL::integer
        END AS percent_now,
        CASE
            WHEN vv.row_number = 1 AND vv.pick_at IS NOT NULL THEN vv.event_driven
            ELSE NULL::boolean
        END AS event_driven,
        CASE
            WHEN vv.row_number = 1 AND vv.pick_at IS NOT NULL THEN vv.percent_before
            ELSE NULL::integer
        END -
        CASE
            WHEN vv.row_number = 1 AND vv.pick_at IS NOT NULL THEN vv.percent_now
            ELSE NULL::integer
        END AS size
   FROM ( SELECT m.vendor_id,
            m.ksnko_container_id,
            m.measured_at,
            m.percent_smoothed,
            p.pick_at,
            p.event_driven,
            p.percent_before,
            p.percent_now,
            row_number() OVER (PARTITION BY m.ksnko_container_id, (p.pick_at::date) ORDER BY p.pick_at) AS row_number
           FROM waste_collection.measurements m
             LEFT JOIN waste_collection.picks p ON m.vendor_id::text = p.vendor_id::text AND m.measured_at = p.pick_at) vv;

-- plné kontejnery
CREATE OR REPLACE VIEW analytic.v_waste_colection_containers_full
AS WITH base AS (
         SELECT measurements.ksnko_container_id AS container_id,
            measurements.measured_at,
            measurements.percent_smoothed
           FROM waste_collection.measurements
        ), status_tbl AS (
         SELECT base.container_id,
            base.measured_at,
            base.percent_smoothed,
                CASE
                    WHEN base.percent_smoothed > 95 THEN
                    CASE
                        WHEN lag(base.percent_smoothed, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at) < 96 THEN 'begin'::text
                        ELSE 'body'::text
                    END
                    WHEN lag(base.percent_smoothed, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at) > 95 THEN 'end'::text
                    ELSE 'ok'::text
                END AS status
           FROM base
          ORDER BY base.container_id, base.measured_at
        ), start_end AS (
         SELECT status_tbl.container_id,
            status_tbl.measured_at,
            status_tbl.percent_smoothed,
            status_tbl.status,
                CASE
                    WHEN status_tbl.status = 'begin'::text THEN status_tbl.measured_at
                    ELSE NULL::timestamp without time zone::timestamp with time zone
                END AS start_time,
                CASE
                    WHEN status_tbl.status = 'begin'::text THEN COALESCE(lead(status_tbl.measured_at, 1) OVER (PARTITION BY status_tbl.container_id ORDER BY status_tbl.measured_at), date_trunc('day'::text, status_tbl.measured_at) + '1 day'::interval)
                    ELSE NULL::timestamp with time zone
                END AS end_time
           FROM status_tbl
          WHERE status_tbl.status = ANY (ARRAY['begin'::text, 'end'::text])
          ORDER BY status_tbl.container_id, status_tbl.measured_at
        )
 SELECT start_end.container_id,
    date_trunc('day'::text, start_end.start_time) AS date,
    start_end.start_time,
    start_end.end_time
   FROM start_end
  WHERE start_end.status = 'begin'::text;

-- plné kontejnery
CREATE OR REPLACE VIEW analytic.v_waste_colection_containers_full_days
AS WITH base AS (
         SELECT m.ksnko_container_id AS container_id,
            m.measured_at,
            m.percent_smoothed AS percent_calculated
           FROM waste_collection.measurements m
        ), status_tbl AS (
         SELECT base.container_id,
            base.measured_at,
            base.percent_calculated,
                CASE
                    WHEN base.percent_calculated > 95 THEN
                    CASE
                        WHEN lag(base.percent_calculated, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at) < 96 AND (base.measured_at::date - lead(base.measured_at::date, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at)) <> 0 THEN 'begin_till_midnight'::text
                        WHEN (base.measured_at::date - lag(base.measured_at::date, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at)) <> 0 AND lag(base.percent_calculated, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at) > 95 THEN 'begin_from_midnight'::text
                        WHEN lag(base.percent_calculated, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at) < 96 THEN 'begin'::text
                        WHEN (base.measured_at::date - lead(base.measured_at::date, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at)) <> 0 THEN 'end_at_midnight'::text
                        ELSE 'body'::text
                    END
                    WHEN lag(base.percent_calculated, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at) > 95 THEN
                    CASE
                        WHEN (base.measured_at::date - lag(base.measured_at::date, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at)) <> 0 THEN 'end_from_midnight'::text
                        ELSE 'end'::text
                    END
                    ELSE 'ok'::text
                END AS status
           FROM base
          ORDER BY base.container_id, base.measured_at
        ), start_end AS (
         SELECT status_tbl.container_id,
            status_tbl.measured_at,
            status_tbl.percent_calculated,
            status_tbl.status,
                CASE
                    WHEN status_tbl.status = 'end_from_midnight'::text THEN date_trunc('day'::text, status_tbl.measured_at)
                    WHEN status_tbl.status = 'begin'::text THEN status_tbl.measured_at
                    WHEN status_tbl.status = 'begin_till_midnight'::text THEN status_tbl.measured_at
                    WHEN status_tbl.status = 'begin_from_midnight'::text THEN date_trunc('day'::text, status_tbl.measured_at)
                    ELSE NULL::timestamp without time zone::timestamp with time zone
                END AS start_time,
                CASE
                    WHEN status_tbl.status ~~ 'begin%'::text AND (status_tbl.measured_at::date - lead(status_tbl.measured_at::date, 1) OVER (PARTITION BY status_tbl.container_id ORDER BY status_tbl.measured_at)) <> 0 THEN date_trunc('day'::text, status_tbl.measured_at) + '1 day'::interval
                    WHEN status_tbl.status ~~ 'begin%'::text AND lead(status_tbl.status, 1) OVER (PARTITION BY status_tbl.container_id ORDER BY status_tbl.measured_at) = 'end_at_midnight'::text THEN date_trunc('day'::text, status_tbl.measured_at) + '1 day'::interval
                    WHEN status_tbl.status ~~ 'begin%'::text AND lead(status_tbl.status, 1) OVER (PARTITION BY status_tbl.container_id ORDER BY status_tbl.measured_at) = 'end'::text THEN lead(status_tbl.measured_at, 1) OVER (PARTITION BY status_tbl.container_id ORDER BY status_tbl.measured_at)
                    WHEN status_tbl.status = 'end_from_midnight'::text THEN status_tbl.measured_at
                    ELSE NULL::timestamp without time zone::timestamp with time zone
                END AS end_time
           FROM status_tbl
          WHERE status_tbl.status = ANY (ARRAY['begin_till_midnight'::text, 'begin_from_midnight'::text, 'begin'::text, 'end_at_midnight'::text, 'end_from_midnight'::text, 'end'::text])
          ORDER BY status_tbl.container_id, status_tbl.measured_at
        )
 SELECT start_end.container_id,
    date_trunc('day'::text, start_end.start_time) AS date,
    start_end.start_time,
    start_end.end_time
   FROM start_end
  WHERE start_end.status = 'end_from_midnight'::text OR start_end.status ~~ 'begin%'::text;
