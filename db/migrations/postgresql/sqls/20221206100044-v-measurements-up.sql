CREATE MATERIALIZED VIEW IF NOT EXISTS v_last_measurements AS
SELECT m.* FROM measurements m
INNER JOIN (
	SELECT DISTINCT ksnko_container_id, MAX(measured_at) as measured_at
	FROM measurements
	GROUP BY ksnko_container_id
) cm
ON m.ksnko_container_id = cm.ksnko_container_id AND m.measured_at = cm.measured_at;

CREATE UNIQUE INDEX IF NOT EXISTS v_last_measurements_ksnko_id_idx ON v_last_measurements USING btree (ksnko_container_id);
