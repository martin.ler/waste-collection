CREATE OR REPLACE VIEW v_next_planned_picks AS
SELECT ksnko_container_id, MIN(pick_date) as next_planned_pick_at
FROM planned_pick_dates
WHERE pick_date >= timezone('Europe/Prague', CURRENT_DATE + INTERVAL '1d' )
GROUP BY 1
ORDER BY 1;

CREATE INDEX IF NOT EXISTS planned_pick_dates_date_idx ON planned_pick_dates ("pick_date");
CREATE INDEX IF NOT EXISTS raw_pick_dates_type_idx ON raw_pick_dates ("station_number", "trash_type");
