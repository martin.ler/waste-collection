CREATE TABLE IF NOT EXISTS container_vendor_data (
    vendor_id varchar(255) NOT NULL,
    ksnko_container_id int,
    sensor_id varchar(255) NOT NULL,
    vendor varchar(255),
    prediction timestamptz,
    installed_at timestamptz,
    installed_by varchar(255),
    network varchar(255),
    bin_depth int,
    has_anti_noise boolean,
    anti_noise_depth int,
    algorithm varchar(255),
    schedule varchar(255),
    sensor_version varchar(255),
    is_sensitive_to_pickups boolean,
    pickup_sensor_type varchar(255),
    decrease_threshold int,
    pick_min_fill_level int,
    active boolean,
    bin_brand varchar(255),
    -- audit fields,
    create_batch_id int8 NULL,
    created_at timestamptz NULL,
    created_by varchar(150) NULL,
    update_batch_id int8 NULL,
    updated_at timestamptz NULL,
    updated_by varchar(150) NULL,

    CONSTRAINT container_vendor_data_vendor_id_key UNIQUE (vendor_id),
    CONSTRAINT container_vendor_data_pkey PRIMARY KEY (vendor_id)
);

CREATE TABLE IF NOT EXISTS sensor_history (
    vendor_id varchar(255) NOT NULL,
    sensor_id varchar(255),
    recorded_at timestamptz NOT NULL,
    -- audit fields,
    create_batch_id int8 NULL,
    created_at timestamptz NULL,
    created_by varchar(150) NULL,
    update_batch_id int8 NULL,
    updated_at timestamptz NULL,
    updated_by varchar(150) NULL,

    CONSTRAINT sensor_history_pkey PRIMARY KEY (vendor_id, recorded_at),

    CONSTRAINT sensor_history_vendor_id_fkey FOREIGN KEY ("vendor_id")
        REFERENCES container_vendor_data (vendor_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS measurements (
    vendor_id varchar(255) NOT NULL,
    ksnko_container_id int NOT NULL,
    percent_smoothed int,
    percent_raw int,
    is_blocked boolean,
    temperature int,
    battery_status float,
    measured_at timestamptz,
    saved_at timestamptz,
    vendor_updated_at timestamptz,
    prediction_bin_full timestamptz,
    firealarm boolean,
    -- audit fields,
    create_batch_id int8 NULL,
    created_at timestamptz NULL,
    created_by varchar(150) NULL,
    update_batch_id int8 NULL,
    updated_at timestamptz NULL,
    updated_by varchar(150) NULL,

    CONSTRAINT measurements_pkey PRIMARY KEY (vendor_id, measured_at),

    CONSTRAINT measurements_vendor_id_fkey FOREIGN KEY ("vendor_id")
        REFERENCES container_vendor_data (vendor_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS picks (
    vendor_id varchar(255) NOT NULL,
    ksnko_container_id int NOT NULL,
    pick_at timestamptz,
    percent_before int,
    percent_now int,
    event_driven boolean,
    -- audit fields,
    create_batch_id int8 NULL,
    created_at timestamptz NULL,
    created_by varchar(150) NULL,
    update_batch_id int8 NULL,
    updated_at timestamptz NULL,
    updated_by varchar(150) NULL,

    CONSTRAINT picks_pkey PRIMARY KEY (vendor_id, pick_at),

    CONSTRAINT picks_vendor_id_fkey FOREIGN KEY ("vendor_id")
        REFERENCES container_vendor_data (vendor_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

