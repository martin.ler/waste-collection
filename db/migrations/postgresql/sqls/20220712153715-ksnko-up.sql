CREATE TABLE IF NOT EXISTS ksnko_stations (
    id int NOT NULL,
    number varchar(50) NOT NULL,
    name varchar(255),
    access varchar(50),
    location varchar(50),
    has_sensor boolean,
    changed timestamptz,
    city_district_name varchar(50),
    coordinate_lat decimal,
    coordinate_lon decimal,
    geom geometry,
    active boolean NOT NULL DEFAULT true,
    -- audit fields,
    create_batch_id int8 NULL,
    created_at timestamptz NULL,
    created_by varchar(150) NULL,
    update_batch_id int8 NULL,
    updated_at timestamptz NULL,
    updated_by varchar(150) NULL,

    CONSTRAINT ksnko_stations_id_key UNIQUE (id),
    CONSTRAINT ksnko_stations_number_key UNIQUE (number),
    CONSTRAINT ksnko_stations_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS ksnko_items (
     id int NOT NULL,
     count float,
     trash_type varchar(50),
     container_volume int,
     container_brand varchar(150),
     container_dump varchar(50),
     cleaning_frequency_code varchar(50),
     station_id int NOT NULL,
     station_number varchar(50) NOT NULL,
     active bool NOT NULL DEFAULT true,
    -- audit fields,
     create_batch_id int8 NULL,
     created_at timestamptz NULL,
     created_by varchar(150) NULL,
     update_batch_id int8 NULL,
     updated_at timestamptz NULL,
     updated_by varchar(150) NULL,

     CONSTRAINT ksnko_items_id_key UNIQUE (id),
     CONSTRAINT ksnko_items_pkey PRIMARY KEY (id),
     CONSTRAINT ksnko_items_station_id_fkey FOREIGN KEY ("station_id")
         REFERENCES ksnko_stations (id) MATCH SIMPLE
         ON UPDATE NO ACTION
         ON DELETE NO ACTION,
     CONSTRAINT ksnko_items_station_number_fkey FOREIGN KEY ("station_number")
         REFERENCES ksnko_stations (number) MATCH SIMPLE
         ON UPDATE NO ACTION
         ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS ksnko_containers (
    id int NOT NULL,
    code varchar(50) NOT NULL,
    volume_ratio float,
    trash_type varchar(50),
    container_volume int,
    container_brand varchar(150),
    container_dump varchar(50),
    cleaning_frequency_code varchar(50),
    station_id int,
    station_number varchar(50),
    active boolean NOT NULL DEFAULT true,
    -- audit fields,
    create_batch_id int8 NULL,
    created_at timestamptz NULL,
    created_by varchar(150) NULL,
    update_batch_id int8 NULL,
    updated_at timestamptz NULL,
    updated_by varchar(150) NULL,

    CONSTRAINT ksnko_containers_id_key UNIQUE (id),
    CONSTRAINT ksnko_containers_pkey PRIMARY KEY (id),
    CONSTRAINT ksnko_containers_station_id_fkey FOREIGN KEY ("station_id")
        REFERENCES ksnko_stations (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT ksnko_containers_station_number_fkey FOREIGN KEY ("station_number")
        REFERENCES ksnko_stations (number) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
