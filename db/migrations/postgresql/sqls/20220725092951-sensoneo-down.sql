DROP TABLE IF EXISTS picks;
DROP TABLE IF EXISTS measurements;
DROP TABLE IF EXISTS sensor_history;
DROP TABLE IF EXISTS container_vendor_data;
