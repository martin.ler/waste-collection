DROP FUNCTION IF EXISTS import_sensor_history_rfid;

DROP TABLE picks_rfid;

DROP TABLE sensor_history_rfid;

DROP TABLE container_vendor_data_rfid;
