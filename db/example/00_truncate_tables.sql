TRUNCATE
    "trash_type_mapping",
    "planned_pick_dates",
    "raw_pick_dates",
    "picks",
    "measurements",
    "sensor_history",
    "container_vendor_data",
    "ksnko_items",
    "ksnko_containers",
    "ksnko_stations";
