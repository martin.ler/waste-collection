INSERT INTO planned_pick_dates
(ksnko_container_id, pick_date, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by)
VALUES(9553, '2022-08-09T08:00:00.000Z', NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO raw_pick_dates
("date", subject_id, count, container_type, container_volume, frequency, street_name, trash_type, trash_type_code, trash_type_name, orientation_number, address_char, conscription_number, valid_from, valid_to, station_number, year_days_isoweeks, "year", days, isoweeks, company, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by)
VALUES('2022-08-09 12:42:17.291', 9553, 0.0, 'abc', 0, '1', 'Dělnická', 'p', 'P', 'Papír', 123, 'abcd', 123.0, '2022-08-09 12:42:17.291', '2022-08-09 12:42:17.291', '456789', '2022-Po,4,8,12,16,20,24,28,32,36,40,44,48,52', 2022, 'Po', '4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52', 'Pražské služby', NULL, NULL, NULL, NULL, NULL, NULL);
