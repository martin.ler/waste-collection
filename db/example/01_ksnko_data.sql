INSERT INTO ksnko_stations (id, number, name, access, location, has_sensor, changed, city_district_name, city_district_slug, coordinate_lat, coordinate_lon, geom, active, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES
(19, '0022/ 019', 'V Bytovkách 783', 'volně', 'outdoor', false, '2019-11-01 13:52:41+00', 'Praha 22', 'praha-22', -731335.500031, -1050300.17852, '0101000020E61000009269E9F206302D406A7320C1AB044940', true, NULL, '2022-07-20 14:54:48.309+00', NULL, NULL, '2022-07-19 15:03:56.029+00', NULL),
(25, '0022/ 025', 'Nové náměstí 1257/9', 'volně', 'outdoor', false, '2022-07-14 10:20:27+00', 'Praha 22', 'praha-22', -730951.671895, -1050922.53939, '0101000020E6100000ED13817657332D40085BDD3205044940', false, NULL, '2022-07-20 14:54:48.309+00', NULL, NULL, '2022-07-19 15:03:56.029+00', NULL),
(9999, '9999/ 025', 'test', 'volně', 'outdoor', false, '2022-07-14 10:20:27+00', 'Praha 22', 'praha-22', -730951.671895, -1050922.53939, '0101000020E6100000ED13817657332D40085BDD3205044940', false, NULL, '2022-07-20 14:54:48.309+00', NULL, NULL, '2022-07-19 15:03:56.029+00', NULL),
(2074, '0007/ 022', 'Řezáčovo náměstí 1256/3', 'volně', 'underground', true, '2022-03-17 12:12:52+01', 'Praha 7', 'praha-7', -741580.769887, -1041671.49585, '0101000020E61000002FB6371C16DF2C4015207CE5EA0C4940', true, NULL, '2022-07-20 14:54:48.309+00', NULL, NULL, '2022-07-19 15:03:56.029+00', NULL);

INSERT INTO ksnko_containers (id, code, volume_ratio, trash_type, container_volume, container_brand, container_dump, cleaning_frequency_code, station_id, station_number, active, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES
(101, 'S0022019TSCV3350SV101', 0.3, 'sc', 3350, 'Atomium Reflex', 'SV', '61', 19, '0022/ 019', true, NULL, NULL, NULL, NULL, '2022-07-19 15:03:56.029+00', NULL),
(102, 'S0022019TNKV240HV102', 1, 'nk', 240, 'normální', 'HV', '11', 19, '0022/ 019', true, NULL, NULL, NULL, NULL, '2022-07-19 15:03:56.029+00', NULL),
(103, 'S0022019TPV1100HV103', 1, 'p', 1100, 'normální', 'HV', '13', 19, '0022/ 019', true, NULL, NULL, NULL, NULL, '2022-07-19 15:03:56.029+00', NULL),
(104, 'S0022019TSBV3350SV104', 0.7, 'sb', 3350, 'Atomium Reflex', 'SV', '61', 19, '0022/ 019', true, NULL, NULL, NULL, NULL, '2022-07-19 15:03:56.029+00', NULL),
(105, 'S0022019TUV1100HV105', 1, 'u', 1100, 'normální', 'HV', '13', 19, '0022/ 019', true, NULL, NULL, NULL, NULL, '2022-07-19 15:03:56.029+00', NULL),
(132, 'S0022025TSCV3350SV132', 0.3, 'sc', 3350, 'Atomium Reflex', 'SV', '61', 25, '0022/ 025', true, NULL, NULL, NULL, NULL, '2022-07-19 15:03:56.029+00', NULL),
(133, 'S0022025TNKV240HV133', 1, 'nk', 240, 'normální', 'HV', '11', 25, '0022/ 025', true, NULL, NULL, NULL, NULL, '2022-07-19 15:03:56.029+00', NULL),
(134, 'S0022025TPV1100HV134', 1, 'p', 1100, 'normální', 'HV', '13', 25, '0022/ 025', true, NULL, NULL, NULL, NULL, '2022-07-19 15:03:56.029+00', NULL),
(135, 'S0022025TSBV3350SV135', 0.7, 'sb', 3350, 'Atomium Reflex', 'SV', '61', 25, '0022/ 025', true, NULL, NULL, NULL, NULL, '2022-07-19 15:03:56.029+00', NULL),
(136, 'S0022025TUV1100HV136', 1, 'u', 1100, 'normální', 'HV', '13', 25, '0022/ 025', true, NULL, NULL, NULL, NULL, '2022-07-19 15:03:56.029+00', NULL),
(21128, 'S0022025TKOV1100SV21128', 1, 'ko', 1100, 'mini H', 'SV', '61', 25, '0022/ 025', true, NULL, NULL, NULL, NULL, '2022-07-19 15:03:56.029+00', NULL),
(30414, 'S0022025TSCV3350SV30414', 1.19209e-08, 'sc', 3350, 'Atomium Reflex', 'SV', '61', 25, '0022/ 025', false, NULL, NULL, NULL, NULL, '2022-07-19 15:03:56.029+00', NULL),
(30415, 'S0022025TJTV240HV30415', 1, 'jt', 240, 'normální', 'HV', '41', 25, '0022/ 025', true, NULL, NULL, NULL, NULL, '2022-07-19 15:03:56.029+00', NULL),
(30416, 'S0022025TSCV3350SV30416', 2.38419e-08, 'sc', 3350, 'Atomium Reflex', 'SV', '61', 25, '0022/ 025', false, NULL, NULL, NULL, NULL, '2022-07-19 15:03:56.029+00', NULL),
(9552, 'S0007022TPV3000SV9552', 1, 'p', 3000, 'Podzemní', 'SV', '13', 2074, '0007/ 022', true, NULL, NULL, NULL, NULL, '2022-07-19 15:03:56.029+00', NULL),
(9553, 'S0007022TSBV3000SV9553', 1, 'sb', 3000, 'Podzemní', 'SV', '11', 2074, '0007/ 022', true, NULL, NULL, NULL, NULL, '2022-07-19 15:03:56.029+00', NULL),
(9554, 'S0007022TUV3000SV9554', 1, 'u', 3000, 'Podzemní', 'SV', '13', 2074, '0007/ 022', true, NULL, NULL, NULL, NULL, '2022-07-19 15:03:56.029+00', NULL),
(99999998, 'test2', 0.0, 'p', 0, NULL, NULL, NULL, NULL, NULL, true, NULL, NULL, NULL, NULL, '2022-07-19 15:03:56.029+00', NULL);

INSERT INTO container_vendor_data (vendor_id, ksnko_container_id, sensor_id, vendor, prediction, installed_at, installed_by, network, bin_depth, has_anti_noise, anti_noise_depth, algorithm, schedule, sensor_version, is_sensitive_to_pickups, pickup_sensor_type, decrease_threshold, pick_min_fill_level, active, bin_brand, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES
('Sensoneo_C00412', 9553, '206524', 'Sensoneo', '2022-12-01 02:06:12+01', '2018-12-05 01:00:00+01', '-', 'SIGFOX', 88, false, NULL, 'Standard', '-', 'v2', true, 'data', 20, 50, true, 'Schäfer/Europa-OR/GB', NULL, '2022-08-24 14:05:09.071+02', NULL, NULL, '2022-11-29 14:05:02.648+01', NULL),
('Sensoneo_C00413', 9552, 'C7AFA0', 'Sensoneo', NULL, '2018-12-05 01:00:00+01', '-', 'SIGFOX', 180, false, NULL, 'Standard', '-', 'v3lite', true, 'data + accelerometer', 20, 55, true, 'Schäfer/Europa-OR/GB', NULL, '2022-08-24 14:05:09.071+02', NULL, NULL, '2022-11-29 14:05:02.648+01', NULL),
('Sensoneo_C00414', 9554, '20676B', 'Sensoneo', NULL, '2018-12-05 01:00:00+01', '-', 'SIGFOX', 180, false, NULL, 'Standard', '-', 'v2', true, 'data', 20, 55, true, 'Schäfer/Europa-OR/GB', NULL, '2022-08-24 14:05:09.071+02', NULL, NULL, '2022-11-29 14:05:02.648+01', NULL);

INSERT INTO measurements (vendor_id, ksnko_container_id, percent_smoothed, percent_raw, is_blocked, temperature, battery_status, measured_at, saved_at, vendor_updated_at, prediction_bin_full, firealarm, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES
('Sensoneo_C00412', 9553, 54, 54, false, 7, 3.5, '2022-11-29 01:26:55+01', '2022-11-29 01:26:55+01', '2022-11-29 01:26:55+01', '2022-12-01 08:15:15+01', false, NULL, '2022-11-29 02:05:21.897+01', NULL, NULL, '2022-11-29 05:36:50.596+01', NULL),
('Sensoneo_C00412', 9553, 53, 53, false, 7, 3.5, '2022-11-29 05:27:33+01', '2022-11-29 05:27:33+01', '2022-11-29 05:27:33+01', '2022-12-01 08:15:15+01', false, NULL, '2022-11-29 05:36:50.557+01', NULL, NULL, '2022-11-29 07:05:54.46+01', NULL),
('Sensoneo_C00412', 9553, 56, 56, false, 7, 3.5, '2022-11-29 09:28:12+01', '2022-11-29 09:28:12+01', '2022-11-29 09:28:12+01', '2022-12-01 02:06:12+01', false, NULL, '2022-11-29 10:05:46.212+01', NULL, NULL, '2022-11-29 11:05:39.708+01', NULL),
('Sensoneo_C00412', 9553, 69, 69, false, 7, 3.5, '2022-11-29 21:30:01+01', '2022-11-29 21:30:01+01', '2022-11-29 21:30:01+01', '2022-11-30 09:36:21+01', false, NULL, '2022-11-29 22:05:28.973+01', NULL, NULL, '2022-11-29 23:05:24.55+01', NULL),
('Sensoneo_C00412', 9553, 72, 72, false, 7, 3.5, '2022-11-29 23:30:21+01', '2022-11-29 23:30:21+01', '2022-11-29 23:30:21+01', '2022-11-30 05:04:04+01', false, NULL, '2022-11-30 00:05:35.063+01', NULL, NULL, '2022-11-30 00:05:35.063+01', NULL),
('Sensoneo_C00413', 9552, 59, 59, false, 3, 3.58, '2022-11-29 00:46:33+01', '2022-11-29 00:46:33+01', '2022-11-29 00:46:33+01', '2022-11-30 00:26:58+01', false, NULL, '2022-11-29 01:05:36.694+01', NULL, NULL, '2022-11-29 05:36:50.597+01', NULL),
('Sensoneo_C00413', 9552, 53, 53, false, 3, 3.59, '2022-11-29 02:46:28+01', '2022-11-29 02:46:28+01', '2022-11-29 02:46:28+01', '2022-11-30 00:26:58+01', false, NULL, '2022-11-29 03:05:35.53+01', NULL, NULL, '2022-11-29 05:36:50.562+01', NULL),
('Sensoneo_C00413', 9552, 53, 53, false, 2, 3.59, '2022-11-29 04:46:29+01', '2022-11-29 04:46:29+01', '2022-11-29 04:46:29+01', '2022-11-30 00:26:58+01', false, NULL, '2022-11-29 05:05:38.407+01', NULL, NULL, '2022-11-29 06:05:27.255+01', NULL),
('Sensoneo_C00413', 9552, 50, 50, false, 3, 3.59, '2022-11-29 06:46:31+01', '2022-11-29 06:46:31+01', '2022-11-29 06:46:31+01', '2022-11-30 04:37:16+01', false, NULL, '2022-11-29 07:05:54.457+01', NULL, NULL, '2022-11-29 08:05:35.728+01', NULL),
('Sensoneo_C00413', 9552, 96, 96, false, 4, 3.59, '2022-11-29 08:46:32+01', '2022-11-29 08:46:32+01', '2022-11-29 08:46:32+01', NULL, false, NULL, '2022-11-29 09:05:43.796+01', NULL, NULL, '2022-11-29 10:05:46.214+01', NULL),
('Sensoneo_C00413', 9552, 96, 96, false, 4, 3.59, '2022-11-29 10:46:36+01', '2022-11-29 10:46:36+01', '2022-11-29 10:46:36+01', NULL, false, NULL, '2022-11-29 11:05:39.705+01', NULL, NULL, '2022-11-29 12:05:25.905+01', NULL),
('Sensoneo_C00413', 9552, 86, 86, false, 4, 3.59, '2022-11-29 14:46:16+01', '2022-11-29 14:46:16+01', '2022-11-29 14:46:16+01', NULL, false, NULL, '2022-11-29 15:05:52.505+01', NULL, NULL, '2022-11-29 16:05:27.059+01', NULL),
('Sensoneo_C00413', 9552, 100, 100, false, 5, 3.59, '2022-11-29 16:46:17+01', '2022-11-29 16:46:17+01', '2022-11-29 16:46:17+01', NULL, false, NULL, '2022-11-29 17:05:53.637+01', NULL, NULL, '2022-11-29 18:05:56.848+01', NULL),
('Sensoneo_C00413', 9552, 100, 100, false, 4, 3.57, '2022-11-29 17:57:42+01', '2022-11-29 17:57:42+01', '2022-11-29 17:57:42+01', NULL, false, NULL, '2022-11-29 18:05:56.843+01', NULL, NULL, '2022-11-29 19:05:27.871+01', NULL),
('Sensoneo_C00413', 9552, 100, 100, false, 4, 3.57, '2022-11-29 18:00:51+01', '2022-11-29 18:00:51+01', '2022-11-29 18:00:51+01', NULL, false, NULL, '2022-11-29 18:05:56.842+01', NULL, NULL, '2022-11-29 19:05:27.871+01', NULL),
('Sensoneo_C00413', 9552, 93, 93, false, 5, 3.58, '2022-11-29 20:46:20+01', '2022-11-29 20:46:20+01', '2022-11-29 20:46:20+01', NULL, false, NULL, '2022-11-29 21:05:22.095+01', NULL, NULL, '2022-11-29 22:05:28.995+01', NULL),
('Sensoneo_C00413', 9552, 100, 100, false, 4, 3.58, '2022-11-29 22:46:24+01', '2022-11-29 22:46:24+01', '2022-11-29 22:46:24+01', NULL, false, NULL, '2022-11-29 23:05:24.549+01', NULL, NULL, '2022-11-30 00:05:35.064+01', NULL),
('Sensoneo_C00414', 9554, 73, 79, false, 3, 3.6, '2022-11-29 01:29:33+01', '2022-11-29 01:29:33+01', '2022-11-29 01:29:33+01', '2022-11-29 06:09:23+01', false, NULL, '2022-11-29 02:05:21.897+01', NULL, NULL, '2022-11-29 05:36:50.596+01', NULL),
('Sensoneo_C00414', 9554, 74, 74, false, 3, 3.6, '2022-11-29 05:30:11+01', '2022-11-29 05:30:11+01', '2022-11-29 05:30:11+01', '2022-11-29 06:09:23+01', false, NULL, '2022-11-29 05:36:50.557+01', NULL, NULL, '2022-11-29 07:05:54.46+01', NULL),
('Sensoneo_C00414', 9554, 91, 91, false, 4, 3.6, '2022-11-29 13:31:31+01', '2022-11-29 13:31:31+01', '2022-11-29 13:31:31+01', NULL, false, NULL, '2022-11-29 14:05:33.821+01', NULL, NULL, '2022-11-29 15:05:52.507+01', NULL),
('Sensoneo_C00414', 9554, 99, 99, false, 3, 3.6, '2022-11-29 21:32:40+01', '2022-11-29 21:32:40+01', '2022-11-29 21:32:40+01', NULL, false, NULL, '2022-11-29 22:05:28.973+01', NULL, NULL, '2022-11-29 23:05:24.55+01', NULL),
('Sensoneo_C00414', 9554, 100, 100, false, 2, 3.6, '2022-11-29 23:33:01+01', '2022-11-29 23:33:01+01', '2022-11-29 23:33:01+01', NULL, false, NULL, '2022-11-30 00:05:35.063+01', NULL, NULL, '2022-11-30 00:05:35.063+01', NULL);

INSERT INTO picks (vendor_id, ksnko_container_id, pick_at, percent_before, percent_now, event_driven, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES
('Sensoneo_C00412', 9553, '2022-11-24 09:09:26+01', 63, 9, false, NULL, '2022-11-25 01:05:36.155+01', NULL, NULL, '2022-11-29 05:35:06.911+01', NULL),
('Sensoneo_C00412', 9553, '2022-11-25 23:15:20+01', 30, 0, false, NULL, '2022-11-26 01:05:37.715+01', NULL, NULL, '2022-11-26 01:05:37.715+01', NULL),
('Sensoneo_C00412', 9553, '2022-11-26 23:19:04+01', 34, 0, false, NULL, '2022-11-27 02:05:27.747+01', NULL, NULL, '2022-11-29 05:35:07.788+01', NULL),
('Sensoneo_C00413', 9552, '2022-11-23 06:30:48+01', 100, 13, false, NULL, '2022-11-24 01:05:36.723+01', NULL, NULL, '2022-11-29 05:35:06.912+01', NULL),
('Sensoneo_C00413', 9552, '2022-11-25 06:22:14+01', 100, 11, false, NULL, '2022-11-26 01:05:37.716+01', NULL, NULL, '2022-11-29 05:35:06.91+01', NULL),
('Sensoneo_C00413', 9552, '2022-11-28 08:25:57+01', 100, 13, false, NULL, '2022-11-29 01:05:36.815+01', NULL, NULL, '2022-11-29 05:35:07.788+01', NULL),
('Sensoneo_C00414', 9554, '2022-11-23 09:08:19+01', 97, 11, false, NULL, '2022-11-24 01:05:36.723+01', NULL, NULL, '2022-11-29 05:35:06.912+01', NULL),
('Sensoneo_C00414', 9554, '2022-11-25 09:15:50+01', 100, 32, false, NULL, '2022-11-26 01:05:37.716+01', NULL, NULL, '2022-11-29 05:35:06.91+01', NULL),
('Sensoneo_C00414', 9554, '2022-11-27 23:25:33+01', 100, 0, false, NULL, '2022-11-28 01:05:35.214+01', NULL, NULL, '2022-11-29 05:35:07.788+01', NULL),
('Sensoneo_C00414', 9554, '2022-11-28 09:27:05+01', 64, 26, false, NULL, '2022-11-29 01:05:36.815+01', NULL, NULL, '2022-11-29 05:35:07.788+01', NULL);
