INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id) VALUES('sb', 'S', 'Barevné sklo', 1);
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id) VALUES('e', '-', 'Elektrozařízení', 2);
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id) VALUES('ko', 'E', 'Kovy', 3);
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id) VALUES('nk', 'K', 'Nápojové kartóny', 4);
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id) VALUES('p', 'P', 'Papír', 5);
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id) VALUES('u', 'U', 'Plast', 6);
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id) VALUES('sc', 'C', 'Čiré sklo', 7);
INSERT INTO trash_type_mapping (code_ksnko, code_psas, "name", legacy_id) VALUES('jt', '-', 'Jedlé tuky a oleje', 8);
