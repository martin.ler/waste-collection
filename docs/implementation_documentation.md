# Implementační dokumentace modulu _WASTE COLLECTION_

## Záměr

Modul slouží k ukládání a poskytování informací o svozu tříděného odpadu.

## Vstupní data

### Data aktivně stahujeme

Základem integrace je KSNKO (Komplexní systém nakládání s komunálním odpadem) - zdroj pravdy o polohách, typech odpadu a zdroj ID.

#### _KSNKO_

-   zdroj dat
    -   url: [https://ksnko.praha.eu/api/stations?full=1&detail=1](https://ksnko.praha.eu/api/stations?full=1&detail=1)
    -   paramtery dotazu: Bearer token získaný z `POST https://ksnko.praha.eu/api/login` s parametry
        ```
        user: config.datasources.KSNKOApi.user,
        password: config.datasources.KSNKOApi.password,
        ```
-   formát dat
    -   protokol: `HTTPProtocolStrategyStreamed`
    -   datový typ: json data stream
    -   [validační schéma: ksnkoStationsJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/schema-definitions/index.ts)
    -   [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/test/integration-engine/data/ksnko-stations-datasource.json)
-   frekvence stahování
    -   cron definice:
        -   `cron.dataplatform.wastecollection.refreshKsnkoData` (`0 25 7 * * *`)
-   název rabbitmq fronty
    -   `dataplatform.wastecollection.refreshKsnkoData`

#### _Sensor data_

-   zdroj dat
    -   url: [https://sensoneotools.azurewebsites.net/api/container_vendor_data?code={code}](https://sensoneotools.azurewebsites.net/api/container_vendor_data?code={code})
        ```
        code: config.datasources.SensoneoApi.code,
        ```
    -   url: [https://sensemanag.sensority.eu/external/golemio-container-vendor-data](https://sensemanag.sensority.eu/external/golemio-container-vendor-data)
        ```
        Bearer: config.datasources.sensorityApi.token,
        ```
-   formát dat
    -   protokol: `HTTPProtocolStrategyStreamed`
    -   datový typ: json data stream
    -   [validační schéma: SensorVendorDataJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/schema-definitions/index.ts)
    -   [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/test/integration-engine/data/sensoneo-sensor-vendor-data-datasource.json)
-   frekvence stahování
    -   cron definice:
        -   `cron.dataplatform.wastecollection.refreshSensorVendorData` (`0 5 * * * *`)
-   závislé fronty
    -   `dataplatform.wastecollection.refreshSensorMeasurements`
    -   `dataplatform.wastecollection.refreshSensorPicks`
-   název rabbitmq fronty

    -   `dataplatform.wastecollection.refreshSensorVendorData`

-   zdroj dat
    -   url: [https://sensoneotools.azurewebsites.net/api/measurements?code={code}](https://sensoneotools.azurewebsites.net/api/measurements?code={code})
        ```
        code: config.datasources.SensoneoApi.code,
        ```
    -   url: [https://sensemanag.sensority.eu/external/golemio-container-measurements](https://sensemanag.sensority.eu/external/golemio-container-measurements)
        ```
        Bearer: config.datasources.sensorityApi.token,
        ```
-   formát dat
    -   protokol: `HTTPProtocolStrategyStreamed`
    -   datový typ: json data stream
    -   [validační schéma: SensorMeasurementsJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/schema-definitions/index.ts)
    -   [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/test/integration-engine/data/sensoneo-sensor-measurements-datasource.json)
-   frekvence stahování
    -   spouští se z `refreshSensorVendorData`
-   název rabbitmq fronty

    -   `dataplatform.wastecollection.refreshSensorMeasurements`

-   zdroj dat
    -   url: [https://sensoneotools.azurewebsites.net/api/picks?code={code}](https://sensoneotools.azurewebsites.net/api/picks?code={code})
        ```
        code: config.datasources.SensoneoApi.code,
        ```
    -   url: [https://sensemanag.sensority.eu/external/golemio-container-picks](https://sensemanag.sensority.eu/external/golemio-container-picks)
        ```
        Bearer: config.datasources.sensorityApi.token,
        ```
-   formát dat
    -   protokol: `HTTPProtocolStrategyStreamed`
    -   datový typ: json data stream
    -   [validační schéma: SensorPicksJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/schema-definitions/index.ts)
    -   [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/test/integration-engine/data/sensoneo-sensor-measurements-datasource.json)
-   frekvence stahování
    -   spouští se z `refreshSensorVendorData`
-   název rabbitmq fronty
    -   `dataplatform.wastecollection.refreshSensorPicks`

#### _PSAS_

-   zdroj dat
    -   url: [https://rapi.psas.cz:7777/api/View/oict_nadoby](https://rapi.psas.cz:7777/api/View/oict_nadoby)
    -   paramtery dotazu: Bearer token získaný z `POST https://rapi.psas.cz:7777/api/User/Login` s parametry
        ```
        user: config.datasources.PSAS.user,
        password: config.datasources.PSAS.password,
        ```
-   formát dat
    -   protokol: `HTTPProtocolStrategyStreamed`
    -   datový typ: json data stream
    -   [validační schéma](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/schema-definitions/datasources/psas/InputRawPickDatesSchema.ts)
    -   [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/test/integration-engine/data/raw-pickdates-datasource.json)
-   frekvence stahování
    -   cron definice:
        -   `cron.dataplatform.wastecollection.refreshPsasData` (`0 40 7 * * *`)
-   název rabbitmq fronty
    -   `dataplatform.wastecollection.refreshPsasData`

#### _RFID_

-   zdroj dat
    -   url: [https://sensoneotools.azurewebsites.net/api/RFID-container_vendor_data](https://sensoneotools.azurewebsites.net/api/RFID-container_vendor_data)
    -   paramtery dotazu: 
            POST
            parametr `code` obsahující secret
            prázdný objekt v body `{}`
-   formát dat
    -   protokol: `HTTPProtocolStrategyStreamed`
    -   datový typ: json data stream
    -   [validační schéma](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/schema-definitions/datasources/rfid/RfidContainerVendorDataSchema.ts)
    -   [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/test/integration-engine/data/raw-rfid-containervendordata.json)
-   frekvence stahování
    -   cron definice:
        -   `cron.dataplatform.wastecollection.rfidDownloadContainerVendorData` (`0 5 * * * *`)
-   název rabbitmq fronty
    -   `dataplatform.wastecollection.rfidDownloadContainerVendorData`

-   zdroj dat
    -   url: [https://sensoneotools.azurewebsites.net/api/RFID-picks](https://sensoneotools.azurewebsites.net/api/RFID-picks)
    -   paramtery dotazu: 
            - metoda POST
            - parametr `code` obsahující secret
            - body obsahuje časové ohraničení `start_at` a `end`
```json
{
    "vendor_id" : [],
    "ksnko_container_id" : [],
    "measured_at" : {
        "start" : "2023-01-30T09:30:15",
        "end" : "2023-02-01T09:30:15"
    }
}
```
-   formát dat
    -   protokol: `HTTPProtocolStrategyStreamed`
    -   datový typ: json data stream
    -   [validační schéma](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/schema-definitions/datasources/rfid/RfidPickSchema.ts)
    -   [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/test/integration-engine/data/raw-rfid-picks.json)
-   frekvence stahování
    -   cron definice:
        -   `cron.dataplatform.wastecollection.rfidDownloadPicks` (`0 7 * * * *`)
-   název rabbitmq fronty
    -   `dataplatform.wastecollection.rfidDownloadPicks`

## Zpracování dat / transformace

### _KsnkoWorker_

Při ukládání data upsertujeme a pro starší data nastavujeme atribut active na false.

#### _refreshKsnkoData_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.wastecollection.refreshKsnkoData`
    -   bez parametrů
-   datové zdroje
    -   KsnkoDataSourceFactory (http),
-   transformace:

    -   [KsnkoTransformation](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/integration-engine/KsnkoStationsTransformation.ts) - mapování pro
        -   `ksnko_stations` - označují stanoviště kde stojí vícero kontejnerů s různými druhy odpadu. sloupec `city_district_slug` doplněn pomocí dotazu přes `geom` na modul `city-districts`
        -   `ksnko_containers` - označuje jednotlivé nádoby.
        -   `ksnko_items` - označuje agregátně jednotlivé typy odpadu na jednotlivých stanovištích.

-   data modely
    -   KsnkoStationsRepository -> (schéma waste-collection) `ksnko_stations`
    -   KsnkoContainerRepository -> (schéma waste-collection) `ksnko_containers`
    -   KsnkoItemsRepository -> (schéma waste-collection) `ksnko_items`

### _SensorWorker_

Při ukládání data upsertujeme.

#### _refreshSensorVendorData_

-   vstupní rabbitmq fronta:
    -   název: `dataplatform.wastecollection.refreshSensorVendorData`
    -   bez parametrů
-   datové zdroje:
    -   SensorDataSourceFactory (http),
-   transformace:
    -   [SensorVendorDataTransformation](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/integration-engine/SensorVendorDataTransformation.ts) - mapování pro
        -   `container_vendor_data` - označují všechny instalované nádoby.
-   data modely:
    -   SensorVendorDataRepository -> (schéma waste-collection) `container_vendor_data`
    -   SensorHistoryRepository -> (schéma waste-collection) `sensor_history`

#### _refreshSensorMeasurements_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.wastecollection.refreshSensorMeasurements`
    -   parametry: `{ "from": [timestamp], "to": [timestamp] }`
-   datové zdroje
    -   SensorDataSourceFactory (http),
-   transformace:
    -   [SensorMeasurementsTransformation](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/integration-engine/SensorMeasurementsTransformation.ts) - mapování pro
        -   `measurements` - označují všechna měření.
-   data modely
    -   SensorMeasurementsRepository -> (schéma waste-collection) `measurements`

#### _refreshSensorPicks_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.wastecollection.refreshSensorPicks`
    -   parametry: `{ "from": [timestamp], "to": [timestamp] }`
-   datové zdroje
    -   SensorDataSourceFactory (http),
-   transformace:
    -   [SensorPicksTransformation](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/integration-engine/SensorPicksTransformation.ts) - mapování pro
        -   `picks` - označují informace o svozech.
-   data modely
    -   SensorPicksRepository -> (schéma waste-collection) `picks`

#### _updateSensorsForMonth_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.wastecollection.updateSensorsForMonth`
    -   bez parametrů - generuje týdenní intervaly pro stáhnutí sensorických měření od `now - month` do `now`.
-   závislé fronty
    -   `dataplatform.wastecollection.refreshSensorMeasurements`
    -   `dataplatform.wastecollection.refreshSensorPicks`
-   frekvence stahování
    -   cron definice:
        -   `cron.dataplatform.wastecollection.updateSensorsForMonth` (`0 35 5 * * *`)

#### _updateSensorsForPreviousTwoMonths_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.wastecollection.updateSensorsForMonth`
    -   bez parametrů - generuje týdenní intervaly pro stáhnutí sensorických měření od `now - 3 months` do `now - month`.
-   závislé fronty
    -   `dataplatform.wastecollection.refreshSensorMeasurements`
    -   `dataplatform.wastecollection.refreshSensorPicks`
-   frekvence stahování
    -   cron definice:
        -   `cron.dataplatform.wastecollection.updateSensorsForPreviousTwoMonths` (`0 45 5 1,8,15,22 * *`)

#### _updateSensorsFromTo_

_pro stahování/zpětnou opravu sensorických dat v určitém časovém rozpětí_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.wastecollection.updateSensorsFromTo`
    -   parametry: `{ "from": [timestamp], "to": [timestamp] }` - generuje týdenní intervaly pro stáhnutí sensorických měření
-   závislé fronty
    -   `dataplatform.wastecollection.refreshSensorMeasurements`
    -   `dataplatform.wastecollection.refreshSensorPicks`

### _WasteCollectionWorker_

Při ukládání data `raw_pick_dates` upsertujeme.
Při ukládání data `planned_pick_dates`, která mají `pick_date` v budoucnosti smažeme a následně dohrajeme nová.

#### task _CollectPickDatesTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.wastecollection.collectPickDatesFromPsas`
    -   bez parametrů
-   datové zdroje
    -   PsasDataSource (http),
-   transformace:
    -   [PsasTransformation](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/integration-engine/transformations/PsasTransformation.ts) -
-   mapování pro
    -   `raw_pick_dates` - obsahuje raw informace o planovaných svozech. `trash_type_code`, `trash_type_name` jsou doplněny dotazem do lookup tabulky `trash_type_mapping`
-   generovaná data
    -   `planned_pick_dates`:
        `pick_date`: - na základě dat z `raw_pick_dates` generujeme naplánované svozy - `isoweeks` definuje týdny v roce, kdy se svoz uskuteční, pokud je null tak se bere v potaz každý týden - `days` definuje dny v týdnu, kdy se svoz uskuteční - rok je definován současností a propertka `year` se ignoruje - `valid_to` pokud je definováno tak negenerovat datumy svozu po tomto datu - z proměných je sestaven iso string např. "2022-W01-1" a knihovna luxon ho přeloží na datum.
        `ksnko_container_id` - se získává z tabulky `ksnko_containers` dotazem na `trash_type` = `raw_pick_dates`.`trash_type_code` a `station_number` = `raw_pick_dates`.`station_number` - pokud se najde více výsledků je potřeba pro každý výsledek vygenerovat samostatný řádek
        [generatePickDates](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection/-/blob/development/src/integration-engine/helpers/PsasParserHelper.ts)
-   data modely
    -   RawPickDatesRepository -> (schéma waste-collection) `raw_pick_dates`
    -   PlannedPickDatesRepository -> (schéma waste-collection) `planned_pick_dates`

#### task _RfidDownloadContainerVendorDataTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.wastecollection.rfidDownloadContainerVendorData`
    -   bez parametrů
    -   datové zdroje
    -   RFID (http),
-   transformace:
    -   data ukládána v příchozí podobě
    -   obdobně jako u _refreshSensorVendorData_ ukládáme sensor history pomocí sql funkce `import_sensor_history_rfid` do které vstupuje aktuální seznam příslušností vendor_id - sensor_id
-   data modely
    -   RfidContainerVendorDataRepository -> (schéma waste-collection) `container_vendor_data_rfid`
    -   RfidSensorHistory -> (schéma waste-collection) `sensor_history_rfid`

#### task _RfidDownloadPicksTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.wastecollection.rfidDownloadPicks`
    -   bez parametrů
    -   datové zdroje
    -   RFID (http),
-   transformace:
    -   data ukládána v příchozí podobě
-   data modely
    -   RfidContainerVendorDataRepository -> (schéma waste-collection) `picks_rfid`

## Uložení dat

Lookup tabulka `trash_type_mapping` je naplněna migračním skriptem a její obsah si repository cachuje in memory v `TrashTypeMappingRepository`.

KSNKO: Pokud se objeví nová station/container/item je potřeba to zapracovat do existující databáze, ale pokud nepřijdou updatovaná data o dané položce, tak si ji držme, jen ji označme jako neaktivní (atribut `active: true`).

Psas: - `raw_pick_dates`: Vkládání se provádí UPSERTem přes atribut `subject_id`. Pokud přestane nádoba přestane na api chodit, záznam zůstane, jenom bude mít staré datum. - `planned_pick_dates`: Na základě atributů `days` a `isoweeks` z `raw_pick_dates` tabulky, se vygenerují budoucí datumy očekávaných svozů (prosté dva sloupce `ksnko_container_id` a date `pick_date`). Atribut `year` bude ignorován. Při updatu je třeba aktualizovat pouze datumy v budoucnosti. Minulé datumy je potřeba považovat za ukládanou historii.

### Obecné

-   typ databáze
    -   PSQL
-   datábázové schéma
    -   ![waste-collection er diagram](assets/wastecollection_erd.png)
